# CODE GUIDELINES #
 
 - Button: btn_
 - TextView: tv_
 - ListView: lv_

Meaningful variable names
Line Comments
JavaDoc
Remove unused imports/variables
No code duplications
Brackets in next line
Intend: two spaces
Language English (Except displayed text?)
Spaces between all operands (+-/\*%=) (e.g.: "Hallo" + "Hallo")

# COMMIT GUIDELINES #

Example Commit Description:
---------------------------------------------------	
	UPDATE:	Cleaned Activity Main.java | ADDED: Created new Activity for displaying timestamps | REMOVED: Deleted Activity New.java due to replacement | BUG-FIX: Allocation of Value in Activity XYZ.java
---------------------------------------------------

Important:
 - No umcompilable code
 - Specifying necessity of comments (if comment is code)
 - Communicate what you are working on (Classes)
 - End of Day + Part of work finished
 
 