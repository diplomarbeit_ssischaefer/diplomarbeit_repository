package com.ssi.zeiterfassungsapp.layouts;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Matthias Pöschl
 * @since 2019/07/23
 * Class that handles the eveything that is used for the filterDialog
 */
public class FilterDialog
{
    private WorktimelistAdapter adapter;
    private Date filterFrom;
    private Date filterTo;
    private Dialog filterDialog;
    private String filteredProjectKey;
    private Webservice webservice;
    private boolean synced;

    public FilterDialog()
    {
        filteredProjectKey = "All Projects";
        filterFrom = new Date();
        filterFrom.setTime(0);
        filterTo = new Date();
        filterTo.setTime(0);

        adapter = WorktimelistAdapter.getInstance();
    }

    public View showFilterDialog(Context context)
    {
        webservice = Webservice.getInstance(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View filterDialogView = inflater.inflate(R.layout.filter_worktime_dialog, null);

        builder.setView(filterDialogView);

        filterDialog = builder.create();

        Toolbar tbDialog = filterDialogView.findViewById(R.id.tb_top);
        tbDialog.setTitle("Filter Worktime");

        WindowManager.LayoutParams params = filterDialog.getWindow().getAttributes();
        params.y = 128;
        filterDialog.getWindow().setAttributes(params);
        filterDialog.getWindow().setGravity(Gravity.TOP);

        filterDialog.show();

        return filterDialogView;
    }

    public void initializeFilterDialog(final Context context, final Context applicationContext, final int position, View filterDialogView)
    {
        final EditText etStarttime = filterDialogView.findViewById(R.id.et_startDate);
        final EditText etEndtime = filterDialogView.findViewById(R.id.et_endDate);
        final CheckBox filterSynced = filterDialogView.findViewById(R.id.cb_synced);

        if(filterFrom.getTime() != 0  && filterTo.getTime() != 0)
        {
            Calendar calendar = Calendar.getInstance();

            calendar.setTime(filterFrom);

            etStarttime.setText(String.format("%02d.%02d.%d"
                    , calendar.get(Calendar.DAY_OF_MONTH)
                    , calendar.get(Calendar.MONTH) + 1
                    , calendar.get(Calendar.YEAR)));

            calendar.setTime(filterTo);

            etEndtime.setText(String.format("%02d.%02d.%d"
                    , calendar.get(Calendar.DAY_OF_MONTH)
                    , calendar.get(Calendar.MONTH) + 1
                    , calendar.get(Calendar.YEAR)));
        }

        final MaterialButton mbDropdown = filterDialogView.findViewById(R.id.btn_dropdown);

        if(!webservice.loadUsername().equals("Guest"))
        {
            mbDropdown.getBackground().setAlpha(255);

            mbDropdown.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    showPopupMenu(context, view, mbDropdown);
                }
            });
        }
        else
        {
            mbDropdown.getBackground().setAlpha(69);
        }

        final Calendar c = Calendar.getInstance();

        final DatePickerDialog dpdStarttime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etStarttime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                filterFrom = calendar.getTime();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        final DatePickerDialog dpdEndtime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etEndtime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                filterTo = calendar.getTime();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        MaterialButton mbFilter = filterDialogView.findViewById(R.id.mb_filter);

        mbFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                synced = filterSynced.isChecked();

                filterFrom.setHours(0);
                filterFrom.setMinutes(0);
                filterFrom.setSeconds(0);
                filterTo.setHours(23);
                filterTo.setMinutes(59);
                filterTo.setSeconds(59);

                if(filterTo.getTime() == 0 && filterFrom.getTime() != 0)
                {
                    Toast.makeText(applicationContext,"End date has to be set", Toast.LENGTH_LONG).show();
                }
                else if(filterFrom.getTime() > filterTo.getTime())
                {
                    Toast.makeText(applicationContext,"Start date must be set earlier than end date", Toast.LENGTH_LONG).show();
                }
                else
                {
                    filteredProjectKey = mbDropdown.getText().toString();

                    if ((etStarttime.getText().toString().equals("") && !etEndtime.getText().toString().equals("")) || (!etStarttime.getText().toString().equals("") && etEndtime.getText().toString().equals("")))
                    {
                        Toast.makeText(applicationContext, "Filtering by date requires start and end date", Toast.LENGTH_LONG).show();
                    }

                    if (etStarttime.getText().toString().equals("") && etEndtime.getText().toString().equals(""))
                    {
                        if(adapter.getUnfilteredWorktimeList() != null && adapter.getUnfilteredWorktimeList().size() > 0)
                        {
                            adapter.filter(0, Long.MAX_VALUE, filteredProjectKey,synced);
                        }
                        else
                        {
                            Toast.makeText(applicationContext, "The list is empty", Toast.LENGTH_LONG).show();
                        }
                    }
                    if (!etStarttime.getText().toString().equals("") && !etEndtime.getText().toString().equals(""))
                    {

                        if(adapter.getUnfilteredWorktimeList() != null || adapter.getUnfilteredWorktimeList().size() > 0)
                        {
                            adapter.filter(filterFrom.getTime(), filterTo.getTime(), filteredProjectKey, synced);
                        }
                        else
                        {
                            Toast.makeText(applicationContext, "The list is empty", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                filterDialog.dismiss();
            }
        });

        MaterialButton mbReset = filterDialogView.findViewById(R.id.mb_reset);

        mbReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                filterTo.setTime(0);
                filterFrom.setTime(0);
                filteredProjectKey = "All Projects";

                if(adapter.getUnfilteredWorktimeList() != null && adapter.getUnfilteredWorktimeList().size() > 0)
                {
                    adapter.resetFilter();
                }
                else
                {
                    Toast.makeText(applicationContext, "The list is empty", Toast.LENGTH_LONG).show();
                }

                filterDialog.dismiss();
            }
        });

        etStarttime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdStarttime.show();
            }
        });

        etEndtime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdEndtime.show();
            }
        });
    }

    /**
     * Method to initialize, handle and show the popup menu for the projects and issues
     * @param context This is the context from the WorktimeActivity, it used to handle everything
     *                that happens here
     * @param view    This is the view from the WorktimeActivity, it used so that the popup menu
     *                knows where it should be displayed
     * @param mbDropdown Button for the dropdown menu
     */

    public void showPopupMenu(Context context,View view,final MaterialButton mbDropdown)
    {
        final PopupMenu popup = new PopupMenu(context, view);
        final List<SubMenu> submenuList = new LinkedList<>();
        // Inflate the menu from xml
        popup.inflate(R.menu.project_issue_menu);
        // Setup menu item selection

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                List<Issue> issueList = ListingProvider.getInstance().getIssuesOfProject(item.getTitle().toString());
                SubMenu currentSubMenu = null;

                if(!item.hasSubMenu())
                {
                    mbDropdown.setText(item.getTitle());
                }
                else
                {
                    for(SubMenu subMenu : submenuList)
                    {
                        if(subMenu.getItem().getTitle().equals(item.getTitle()))
                        {
                            subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            currentSubMenu = subMenu;
                        }
                    }
                    for (Issue issue : issueList)
                    {
                        currentSubMenu.add(issue.getKey());
                    }
                }

                return true;
            }
        });

        List<Project> projectList = ListingProvider.getInstance().getProjects();

        for (Project p : projectList)
        {
            SubMenu helpMenu = popup.getMenu().addSubMenu(p.getProjectKey());
            helpMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            submenuList.add(helpMenu);
        }

        popup.show();
    }
}
