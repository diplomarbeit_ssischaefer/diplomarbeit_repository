package com.ssi.zeiterfassungsapp.layouts;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.StatisticsTabAdapter;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Matthias Pöschl & Bernhard Heiß
 * @since ???
 **/

public class StatisticActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncDelegate
{
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private Webservice webservice;
    private WorktimelistAdapter adapter;
    private ListingProvider listingProvider;


    private LinkedList<Worktime> workTime = new LinkedList<>();
    private LinkedList<Worktime> unsyncedWorktimes;

    private StatisticsTabAdapter myPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);
        adapter = WorktimelistAdapter.getInstance();
        listingProvider = ListingProvider.getInstance();
        webservice.listAllWorktimes();

        if (webservice.loadUsername().equals("Guest"))
        {
            webservice.loadWorktimesFromDatabase(0);
        } else
        {
            webservice.loadWorktimesFromDatabase(1); 
        }

        setContentView(R.layout.statistics_main);

        ViewPager viewPager = findViewById(R.id.viewPager);
        myPagerAdapter = new StatisticsTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Statistics");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if (!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        } else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                startActivity(new Intent(StatisticActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, StartStopActivity.class));
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            startActivity(new Intent(StatisticActivity.this, StartStopActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        } else if (id == R.id.nav_worktime)
        {
            startActivity(new Intent(StatisticActivity.this, WorktimeActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        } else if (id == R.id.nav_projects)
        {
            startActivity(new Intent(StatisticActivity.this, ProjectActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        } else if (id == R.id.nav_statistics)
        {
        } else if (id == R.id.nav_export)
        {
            startActivity(new Intent(StatisticActivity.this, ExportActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        } else if (id == R.id.nav_settings)
        {
            startActivity(new Intent(StatisticActivity.this, SettingsActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void loginComplete(boolean success)
    {
    }

    @Override
    public void validationComplete(boolean success)
    {
    }

    @Override
    public void issueLoadingComplete(boolean success)
    {
    }

    @Override
    public void worktimeLoadingComplete(boolean success)
    {
        ViewPager viewPager = findViewById(R.id.viewPager);
        myPagerAdapter = new StatisticsTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void authenticationJiraComplete(boolean success)
    {
    }

    @Override
    public void deauthenticationJiraComplete(boolean success)
    {
    }

    @Override
    public void notificationSent(boolean success)
    {
    }


    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage)
    {

        if (usage == 0)
        {
            adapter.setUnfilteredWorktimeList(worktimes);
            webservice.listAllWorktimes();
            System.out.println(listingProvider.getWorktimes());
            listingProvider.addLocalWorktimes(worktimes);

            ViewPager viewPager = findViewById(R.id.viewPager);
            myPagerAdapter = new StatisticsTabAdapter(getSupportFragmentManager());
            viewPager.setAdapter(myPagerAdapter);
            TabLayout tabLayout = findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
        }


        if (usage == 1 && worktimes.size() > 0)
        {
            if (!webservice.loadUsername().equals("Guest"))
            {
                unsyncedWorktimes = new LinkedList<>(worktimes);
                List<Date> modifiedDays = new LinkedList<>();

                for (Worktime worktime : worktimes)
                {
                    if (!modifiedDays.contains(worktime.getStartDate()))
                    {
                        modifiedDays.add(worktime.getStartDate());
                    }
                }

                Collections.sort(modifiedDays);

                webservice.listWorktimesOfDay(((LinkedList<Date>) modifiedDays).getFirst(), ((LinkedList<Date>) modifiedDays).getLast());
            } else
            {
                adapter.setUnfilteredWorktimeList(worktimes);
            }
        }

    }

    @Override
    public void deleteWorktimesComplete(boolean success)
    {
    }

    @Override
    public void stopWorktimeComplete(boolean success)
    {
    }

    @Override
    public void worktimeDayLoadingComplete(boolean success)
    {
    }

    @Override
    public void postingWorktimeCompleted(boolean success)
    {
    }
}
