package com.ssi.zeiterfassungsapp.layouts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.util.NetworkUtil;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.List;

/**
 * This class is used for the login of an user, as soon as the application loads
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class LoginActivity extends AppCompatActivity implements AsyncDelegate
{
    private TextInputEditText etUsername;
    private TextInputEditText etPassword;
    private Webservice webservice;
    private MaterialCheckBox cbRememberMe;
    private boolean rememberMe = false;
    private String username = "";
    private static final String TAG = "LoginActivity";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_menu);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        MaterialButton btnLogin = findViewById(R.id.btn_login);
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        TextView tvGuest = findViewById(R.id.tv_loginGuest);
        cbRememberMe = findViewById(R.id.cb_rememberMe);

        SpannableString content = new SpannableString("Sign in as guest");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvGuest.setText(content);
        tvGuest.setClickable(true);

        loadSharedPreferences();

        etUsername.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_NEXT)
                {
                    etUsername.clearFocus();
                    etPassword.requestFocus();

                    return true;
                }

                return false;
            }
        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_GO)
                {
                    InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                    imm.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
                    startLogin();

                    return true;
                }

                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                username = etUsername.getText().toString().trim();

                startLogin();
            }
        });

        tvGuest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startLoginAsGuest();
            }
        });

        etUsername.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable)
            {
                if(etUsername.getText().toString().equals(""))
                {
                    etUsername.setError("Username should not be empty");
                }
            }
        });

        etPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable)
            {
                if(etPassword.getText().toString().equals(""))
                {
                    etPassword.setError("Password should not be empty");
                }
            }
        });

        cbRememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    etUsername.setText(etUsername.getText().toString());
                }

                cbRememberMe.setChecked(isChecked);
                rememberMe = isChecked;
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }

    /**
     * This method loads the options the user entered in the previous session of the application.
     */
    public void loadSharedPreferences()
    {
        rememberMe = webservice.loadRememberMe();
        username = webservice.loadUsername().equals("Guest") ? "" : webservice.loadUsername();

        cbRememberMe.setChecked(rememberMe);

        if(rememberMe)
        {
            etUsername.setText(username);
        }
    }

    /**
     * This method checks if the user entered the user name and password correctly and
     * loads the next screen if the credentials are correct, otherwise it will throw an error in
     * the corresponding text field.
     */
    public void startLogin()
    {
        if(etUsername.getText().toString().trim().equals("") && etPassword.getText().toString().trim().equals(""))
        {
            etPassword.setError("Password should not be empty");
            etUsername.setError("Username should not be empty");
        }
        else if(!etUsername.getText().toString().trim().equals(""))
        {
            if(etUsername.getText().toString().trim().matches("^.+\\..+@ssi-schaefer.com$"))
            {
                if(!etPassword.getText().toString().trim().equals(""))
                {
                    if(!webservice.loadUsername().equals(etUsername.getText().toString().trim()))
                    {
                        webservice.storeProjectColor("");
                    }

                    if(NetworkUtil.isConnected(getApplicationContext()))
                    {
                        webservice.login(etUsername.getText().toString().trim(), etPassword.getText().toString().trim(), rememberMe);
                    }else
                    {
                        Toast.makeText(getApplicationContext(), "Login unavailable - Use Guest", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    etPassword.setError("Password should not be empty");
                }
            }
            else
            {
                etUsername.setError("The email address you entered is not valid");
            }
        }
        else
        {
            etUsername.setError("Username should not be empty");
        }
    }

    /**
     * This method lets a user use the application as a guest with limited functions.
     */
    public void startLoginAsGuest()
    {
        webservice.storeUsername("Guest");
        startActivity(new Intent(LoginActivity.this, StartStopActivity.class).putExtra("callingActivity", TAG));
        webservice.setGuest(true);

        Toast.makeText(this, "Signed in as guest", Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginComplete(boolean success)
    {
        //TODO: Code which is responsible for actions after Login completed
        if(success)
        {
            startActivity(new Intent(LoginActivity.this, StartStopActivity.class).putExtra("callingActivity", TAG));
            webservice.setGuest(false);

            Toast.makeText(this, "Hello " + etUsername.getText().toString().trim(), Toast.LENGTH_LONG).show();
        }

        Log.i(TAG, "Login successful: " + success);
    }

    @Override
    public void validationComplete(boolean success)
    {
        //TODO: Code which is responsible for actions after TokenValidation completed
        //1. CheckToken and then in validationComplete() loadToken()
        Log.i(TAG, "Token valid: " + success);
    }

    @Override
    public void issueLoadingComplete(boolean success)
    {
        //TODO: Code which is responsible for actions after Loading Issues completed
        //1. CheckToken and then in validationComplete() loadToken()
        //all Lists of Issues and Projects available in ListingProvider
        Log.i(TAG, "Loading Issues: " + success);
    }

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success)
    {
        if(success)
        {
            ListingProvider.getInstance().clearProjectsAndIssues();
            webservice.loadIssues();
        }

        Log.i(TAG,"authenticated: "+success);
    }

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}
}
