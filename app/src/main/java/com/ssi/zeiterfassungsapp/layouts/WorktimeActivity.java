package com.ssi.zeiterfassungsapp.layouts;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This is the class for the worktime_list.xml. It is used for managing the activity
 * and the WorktimelistAdapter. It also has an OnClickListener so you can click on each worktimeList
 * item and change their start/endtime, project id, etc...
 * It also includes the methods for adding and filtering Worktime items
 * @author Matthias Pöschl
 * @since  2019/07/09
 */
public class WorktimeActivity extends AppCompatActivity implements WorktimelistAdapter.ItemClickListener, NavigationView.OnNavigationItemSelectedListener, AsyncDelegate
{
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private WorktimelistAdapter adapter;
    private ArrayList<Worktime> workTime = new ArrayList<>();
    private int position;
    private Webservice webservice;
    private LinkedList<Worktime> unsyncedWorktimes;
    private boolean syncFinished = true;
    private static WorktimeActivity instance;
    private AddDialog addDialog;
    private EditDialog editDialog;

    public static WorktimeActivity getInstance()
    {
        if(instance == null)
        {
            instance = new WorktimeActivity();
        }
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.worktime_list);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Worktime");

        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if(!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        }
        else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
            FloatingActionButton fabSync = findViewById(R.id.fab_sync);
            fabSync.setVisibility(View.GONE);
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                adapter.reset();
                startActivity(new Intent(WorktimeActivity.this, LoginActivity.class));
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_worktimeList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = WorktimelistAdapter.getInstance();
        adapter.filter(0,0,"All Projects", false);

        loadData();

        adapter.resetFilter();

        adapter.setInflater(LayoutInflater.from(this));
        adapter.setClickListener(this);
        adapter.notifyDataSetChanged();

        Toolbar tb = findViewById(R.id.toolbar);
        tb.bringToFront();

        recyclerView.setAdapter(adapter);

        final SwipeRefreshLayout refreshLayout = findViewById(R.id.srl_worktimes);

        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        refreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorPrimaryDark));

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        (new Handler()).postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if(isSyncFinished() && !webservice.loadUsername().equals("Guest"))
                                {
                                    Log.e("Refresh", "Working");
                                    loadData();
                                }else
                                {
                                    Log.e("Refresh", "Not Working");
                                }
                                refreshLayout.setRefreshing(false);
                            }
                        }, 2000);
                    }
                }
        );
    }

    /**
     * Function to load test data in to the list (will be deleted later)
     */
    public void loadData()
    {
        webservice.loadWorktimesFromDatabase(0);
    }

    public boolean isSyncFinished()
    {
        boolean finished = false;

        if(editDialog == null || (editDialog != null && !editDialog.isOK())) //When dialog not opened or dialog canceled
        {
            if(addDialog == null)  //And add dialog should also be null otherwise it would manipulate syncFinished before actually finished by addDialog
            {
                syncFinished = true;
                Log.e("Refresh", "Working");
                finished = true;
            }
        }

        if(editDialog != null && editDialog.isOK() && syncFinished)
        {
            Log.e("Refresh", "Working");
            finished = true;
        }

        if(addDialog == null || (addDialog != null && !addDialog.isOK())) //When dialog not opened or dialog canceled
        {
            if(editDialog == null)
            {
                syncFinished = true;
                Log.e("Refresh", "Working");
                finished = true;
            }
        }

        if(addDialog != null && addDialog.isOK() && syncFinished) //When dialog confirmed and sync finished
        {
            Log.e("Refresh", "Working");
            finished = true;
        }

        return finished;
    }

    /**
     * Method to create and show the AddDialog
     * @param view Has to be an parameter in all onClick methods (Has no function here)
     */
    public void showAddDialog(View view)
    {
        Log.e("Refresh", "Unavailable");
        syncFinished = false;
        addDialog = new AddDialog(getApplicationContext());
        View addDialogView = addDialog.showAddDialog(this);

        addDialog.initializeAddDialog(this, getApplicationContext(), position, addDialogView);
    }

    public void showFilterDialog(View view)
    {
        FilterDialog filterDialog = new FilterDialog();
        View filterDialogView = filterDialog.showFilterDialog(this);

        filterDialog.initializeFilterDialog(this, getApplicationContext(), position, filterDialogView);
    }

    public void onSync(View view)
    {
        syncWorktimes();
    }

    public void syncWorktimes()
    {
        webservice.loadWorktimesFromDatabase(1);

        FloatingActionButton fabSync = findViewById(R.id.fab_sync);
        float deg = fabSync.getRotation() + 180F;

        fabSync.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
    }

    /**
     * This function handles the clicks for each list item.
     * It is overwritten from the class "WorktimelistAdapter".
     * @param view Has to be an paramerter in all onClick methods (Has no function here)
     * @param itemPosition The position of the clicked item in the list
     */
    @Override
    public void onItemClick(View view, int itemPosition)
    {
        position = itemPosition;

        showEditDialog();
    }

    /**
     * This method creates and shows the edit dialog
     */
    public void showEditDialog()
    {
        Log.e("Refresh", "Unavailable");
        syncFinished = false;
        editDialog = new EditDialog(getApplicationContext());
        View editDialogView = editDialog.showEditDialog(this);

        editDialog.initializeEditDialog(this,getApplicationContext(),position,editDialogView);
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, StartStopActivity.class));
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            startActivity(new Intent(WorktimeActivity.this, StartStopActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_worktime)
        {
        }
        else if (id == R.id.nav_projects)
        {
            startActivity(new Intent(WorktimeActivity.this, ProjectActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_statistics)
        {
            startActivity(new Intent(WorktimeActivity.this, StatisticActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_export)
        {
            startActivity(new Intent(WorktimeActivity.this, ExportActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_settings)
        {
            startActivity(new Intent(WorktimeActivity.this, SettingsActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success)
    {
        if(success)
        {
            List<Worktime> syncedList = ListingProvider.getInstance().getWorktimes();
            List<Worktime> unsyncedList = adapter.getUnfilteredWorktimeList();

            for(Worktime w : syncedList)
            {
                if(!workTime.contains(w))
                {
                    workTime.add(w);
                }
                w.setSynced(true);
                unsyncedList.add(w);
            }

            adapter.setUnfilteredWorktimeList(unsyncedList);
            workTime.clear();
            workTime.addAll(unsyncedList);
            adapter.resetFilter();
            adapter.notifyDataSetChanged();
        }

        if(!success)
        {
            Log.e("WorktimeActivity", "WorktimeLoading failed");
        }
    }

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage)
    {
        //syncFinished = true;
        if(usage == 0 || usage == 3)
        {
            workTime.clear();
            for (Worktime w : worktimes)
            {
                if(!workTime.contains(w))
                {
                    workTime.add(w);
                }
            }

            adapter.setUnfilteredWorktimeList(worktimes);

            if(!webservice.loadUsername().equals("Guest"))
            {
                webservice.listAllWorktimes();
            }

            if(usage == 3) //Usage 3 is only if worktimes added manually
            {
                Log.e("ADD","Manually Adding new Worktime - Should SYNC now");
                syncWorktimes();
                //syncFinished = true; //Now it should be possible to refresh
            }
            adapter.resetFilter();
            adapter.notifyDataSetChanged();
        }

        if(usage == 1 && worktimes.size() > 0)
        {
            if(!webservice.loadUsername().equals("Guest"))
            {
                boolean isValid = true;

                for(Worktime w : worktimes)
                {
                    if(w.getIssueKey().equals("No project selected"))
                    {
                        isValid = false;
                    }
                }

                if(isValid)
                {
                    unsyncedWorktimes = new LinkedList<>(worktimes);
                    List<Date> modifiedDays = new LinkedList<>();

                    for(Worktime worktime : worktimes)
                    {
                        if(!modifiedDays.contains(worktime.getStartDate()))
                        {
                            modifiedDays.add(worktime.getStartDate());
                        }
                    }

                    Collections.sort(modifiedDays);

                    webservice.listWorktimesOfDay(((LinkedList<Date>) modifiedDays).getFirst(), ((LinkedList<Date>) modifiedDays).getLast());
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Some worktimes are not assigned to issues yet", Toast.LENGTH_LONG).show();
                }
            }
            //syncFinished = true;
        }
    }

    @Override
    public void stopWorktimeComplete(boolean success) {
        //adapter.notifyDataSetChanged();
        //adapter.setUnfilteredWorktimeList(workTime);
        //adapter.notifyDataSetChanged();
    }

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success)
    {
        if(success)
        {
            List<Worktime> syncedWorktimes = ListingProvider.getInstance().getWorktimesDateSpecific();

            for (Worktime w : syncedWorktimes)
            {
                if(!unsyncedWorktimes.contains(w))
                {
                    unsyncedWorktimes.add(w);
                }
            }

            webservice.syncDatabaseWithWebservice(unsyncedWorktimes);
            workTime.addAll(unsyncedWorktimes);
            adapter.setUnfilteredWorktimeList(workTime);
            webservice.listAllWorktimes();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void postingWorktimeCompleted(boolean success) {
        syncFinished = true;
        Log.e("Refresh", "Available");
        //addDialog = null;
        //editDialog = null;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        /**
         * Checks if: worktime activity gains focus, the activity which possess activity before worktime is another activity or the listener was assigned to another activity
         */
        if(hasFocus && (!webservice.getmContext().equals(getApplicationContext()) || !webservice.getListener().equals(this))) //Only if dialog opened and changed listener
        {
            webservice = Webservice.getInstance(getApplicationContext());
            webservice.setListener(this);
        }

    }
}