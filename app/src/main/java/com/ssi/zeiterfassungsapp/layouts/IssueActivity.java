package com.ssi.zeiterfassungsapp.layouts;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.IssueListAdapter;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to display all issues of a selected project of the current user.
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class IssueActivity extends AppCompatActivity implements AsyncDelegate
{
    protected Toolbar toolbar;
    private Webservice webservice;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager llm;
    private ArrayList<Issue> issueList = new ArrayList<>();
    private String projectKey;
    private int projectColor;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issue_list);

        projectKey = getIntent().getStringExtra("PROJECTKEY");
        projectColor = getIntent().getIntExtra("PROJECTCOLOR", 0);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(projectKey);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onBackPressed();
            }
        });

        llm = new LinearLayoutManager(this);
        adapter = new IssueListAdapter(getApplicationContext());

        recyclerView = (RecyclerView) findViewById(R.id.rv_issues);

        issueList = ListingProvider.getInstance().getIssuesOfProject(projectKey);
        addIssueList(issueList);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(llm);
    }

    /**
     * This method adds a list of issues of the corresponding project to the adapter
     * to make them visible for the user.
     *
     * @param issueList - List of issues to add
     */
    public void addIssueList(ArrayList<Issue> issueList)
    {
        for (Issue issue: issueList)
        {
            ((IssueListAdapter) adapter).addIssue(issue);
            ((IssueListAdapter) adapter).setColor(projectColor);
        }
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(IssueActivity.this, ProjectActivity.class));
        overridePendingTransition(R.layout.animation_from_left, R.layout.animation_to_right);
        finish();
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}

}
