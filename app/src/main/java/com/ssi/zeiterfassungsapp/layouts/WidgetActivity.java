package com.ssi.zeiterfassungsapp.layouts;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.Date;

/**
 * Implementation of App Widget functionality.
 *
 * @author Bernhard Heiß
 * @since ???
 */
public class WidgetActivity extends AppWidgetProvider
{
    protected Toolbar toolbar;
    private static Webservice webservice;
    private static Worktime worktime;
    private static Long startDate;
    private static SharedPreferences sharedPrefs;
    private static final String BUTTON_CLICK = "com.ssi.zeiterfassungsapp.Button_Click";
    private PendingIntent pendingIntent;
    private static AlarmManager alarmManager;
    private static Intent alarmIntent;
    private final String ALARM_SERVICE = "com.ssi.zeiterfassungsapp.ALARM_SERVICE";

    /**
     * @param context          Context of the Widget
     * @param appWidgetManager
     * @param appWidgetIds
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds)
        {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
            views.setOnClickPendingIntent(R.id.btn_Timer,
                    getPendingSelfIntent(context, BUTTON_CLICK));

            sharedPrefs = context.getSharedPreferences("com.ssi.zeiterfassungsapp", Context.MODE_PRIVATE);
            startDate = sharedPrefs.getLong("startDateInMillis", 0);

            if (pendingIntent == null)
            {
                performConfig(context);
                pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            }

            //Start Timer
            if (startDate != 0)
            {
                views.setImageViewResource(R.id.btn_Timer, R.drawable.ic_stop_24px);
                alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), 60000, pendingIntent);
            }
            //Stop timer
            else if (startDate == 0)
            {
                views.setImageViewResource(R.id.btn_Timer, android.R.drawable.ic_media_play);
                alarmManager.cancel(pendingIntent);
            }
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    /**
     * @param context Context of the Widget
     */
    public void performConfig(Context context)
    {
        webservice = Webservice.getInstance(context.getApplicationContext());
        sharedPrefs = context.getSharedPreferences("com.ssi.zeiterfassungsapp", Context.MODE_PRIVATE);

        //Add BroadcastReceiver to alarmManager
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(context, CustomBroadcastReceiver.class);
        alarmIntent.setAction(ALARM_SERVICE);

        //Create Filters for the BroadcastReceiver to listen to
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(alarmIntent.getAction());
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        android.content.BroadcastReceiver mReceiver = new CustomBroadcastReceiver();
        context.getApplicationContext().registerReceiver(mReceiver, filter);
    }

    /**
     * onEnable is called when the widget is created.
     * In here a new Timer instacne will be created.
     *
     * @param context Context of the Widget
     */
    @Override
    public void onEnabled(Context context)
    {
        // Enter relevant functionality for when the first widget is created
    }


    /**
     * Function when the last widget is disabled.
     *
     * @param context
     */
    @Override
    public void onDisabled(Context context)
    {
        //This sets the WidgetID to -1. This is needed in order for only one Widget instance to exist.

        sharedPrefs = context.getSharedPreferences("com.ssi.zeiterfassungsapp", Context.MODE_PRIVATE);
        sharedPrefs.edit().putInt("WidgetID", -1).apply();
    }

    /**
     * A Receive funtion when the Button is clicked, either starts or stops the time tracking.
     *
     * @param context Context of the Widget
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        super.onReceive(context, intent);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
        webservice = Webservice.getInstance(context.getApplicationContext());
        /**
         * Start or stop timing with AppWidget 'btn_Timer' click
         */
        if (BUTTON_CLICK.equals(intent.getAction()))
        {
            sharedPrefs = context.getSharedPreferences("com.ssi.zeiterfassungsapp", Context.MODE_PRIVATE);
            startDate = sharedPrefs.getLong("startDateInMillis", 0);

            /*
             * If a startDate already exists, the tracking will be stopped
             * and the StartStopActivity will be started
             */
            if (startDate != 0 && isOverOneMinute(context))
            {
                //countDown.stopTimer();
                Intent launchAcitivity = new Intent(context, StartStopActivity.class);
                launchAcitivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putInt("widget", 1);
                launchAcitivity.putExtras(bundle);
                context.startActivity(launchAcitivity);
            } else if (startDate == 0)  //If no timing exists. Meaning the timing will be started
            {
                startWork();
                Toast.makeText(context, "Time tracking started.", Toast.LENGTH_LONG).show();
            }
        }

        int[] ids = AppWidgetManager.getInstance(context).getAppWidgetIds(
                new ComponentName(context, WidgetActivity.class));

        this.onUpdate(context, AppWidgetManager.getInstance(context), ids);
    }

    /**
     * If the Time is stopped this functions checks if the timetracking is over the minimum of one minute
     *
     * @param context Context of the Widget
     * @return boolean if the time is over one minute
     */
    private boolean isOverOneMinute(Context context)
    {
        Worktime worktime_tmp = new Worktime(sharedPrefs.getString("unfinishedWork", "")); //Converts String to Worktime Object
        worktime_tmp.setEndDate(new Date(System.currentTimeMillis()));
        long difference = worktime_tmp.getEndDate().getTime() - worktime_tmp.getStartDate().getTime();

        if ((difference / 60000L) > 0)
        {
            return true;
        } else
        {
            Toast.makeText(context, "Start and end time must be one minute apart", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void startWork()
    {
        worktime = new Worktime();
        Date date = new Date();
        date.setTime(System.currentTimeMillis());
        sharedPrefs.edit().putLong("startDateInMillis", date.getTime()).apply();
        worktime.setIssueKey("No project selected");

        worktime.setStartDate(date);
        worktime.setEndDateString("");

        sharedPrefs.edit().putString("unfinishedWork", worktime.toString()).apply(); //read Object of Worktime
        webservice.saveWorktimeInDatabase(worktime, 3); //Saves the running Worktime
        startDate = sharedPrefs.getLong("startDateInMillis", 0);
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action)
    {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
}