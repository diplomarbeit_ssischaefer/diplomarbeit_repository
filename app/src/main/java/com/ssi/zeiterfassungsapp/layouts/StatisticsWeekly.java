package com.ssi.zeiterfassungsapp.layouts;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author Matthias Pöschl & Bernhard Heiß
 * @since 2019/07/19
 * A simple {@link Fragment} subclass.
 */
public class StatisticsWeekly extends Fragment
{
    private ArrayList<Worktime> workTime = new ArrayList<>();
    private ListingProvider listingProvider;
    private LinkedList<Float> hoursofDays = new LinkedList<>();
    private HashMap<String, Float> hoursPerProject = new HashMap<>();
    private boolean synced = false;

    public StatisticsWeekly()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics_weekly, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Toolbar tbBarChart = view.findViewById(R.id.tb_BarChart);
        Toolbar tbPieChart = view.findViewById(R.id.tb_pieChart);

        tbBarChart.setTitle("Daily Working Hours");
        tbPieChart.setTitle("Hours by Project");

        listingProvider = ListingProvider.getInstance();
        Webservice webservice = Webservice.getInstance();
        synced = !webservice.loadUsername().equals("Guest");

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));                                             //Hardcode timezone
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        long startofWeek = cal.getTimeInMillis();
        Calendar cal2 = (Calendar) cal.clone();
        cal2.add(Calendar.WEEK_OF_YEAR, 1);
        long endofWeek = cal2.getTimeInMillis();

        workTime = listingProvider.filterWorktimes("All Projects", startofWeek, endofWeek, synced);


        hoursofDays(cal);
        hoursPerProject();

        initializeBarChart(view);
        initializePieChart(view);
    }

    /**
     * Calculates the working hours of each day, beginning at the starting of current week '00:00'
     * Ending the following week at '00:00'
     *
     * @param currDay the start of the current week with time '00:00'
     */
    public void hoursofDays(Calendar currDay)
    {
        currDay.set(Calendar.HOUR_OF_DAY, 0);
        currDay.set(Calendar.MINUTE, 0);

        Calendar nextDay = (Calendar) currDay.clone();
        nextDay.add(Calendar.DAY_OF_YEAR, 1);

        for(int i = 1; i <= 7; i++)   //iterate though all 7 week days
        {
            float sumofDay = 0;
            ArrayList<Worktime> singleDay = listingProvider.filterWorktimes("All Projects", currDay.getTimeInMillis(), nextDay.getTimeInMillis(), synced);

            for (Worktime obj : singleDay)
            {
                float diff = obj.getEndDate().getTime() - obj.getStartDate().getTime();
                sumofDay += diff;
            }

            hoursofDays.add(sumofDay);  //add single day to list
            nextDay.add(Calendar.DAY_OF_YEAR, 1);
            currDay.add(Calendar.DAY_OF_YEAR, 1);
        }
    }

    /**
     * Calculates each hours worked on a Project and stores them on the Collection 'hoursPerProject'
     */
    public void hoursPerProject()
    {
        ArrayList<Project> projectsList = new ArrayList<>();
        for (Worktime worktime : workTime)
        {
            String projectkey = worktime.getIssueKey().split("-")[0];
            projectsList.add(new Project(projectkey, projectkey));    //Zweimal project weils egal ist
        }

        //if no project is selected. and also for testing purposes
        //not needed if only the synced Worktimes need to be displayed
        //projectsList.add(new Project("No project selected", "No project selected"));

        for (Project project : projectsList)
        {
            ArrayList<Worktime> projWorkTimeList = listingProvider.getWorktimesOfProject(project.getProjectKey().toUpperCase(), workTime.toArray(new Worktime[workTime.size()]));

            float sumofProject = 0;
            for (Worktime obj : projWorkTimeList)
            {
                float diff_TMP = Math.abs(obj.getEndDate().getTime() - obj.getStartDate().getTime());
                float diff = ((diff_TMP % 86400000) / 3600000);
                sumofProject += diff;
            }
            hoursPerProject.put(project.getProjectKey(), sumofProject);
        }
    }


    /**
     * Initializes the Bar Char representing one day of the week(Monday to Sunday)
     * Each day has a maximum working hour of 12 hours.
     * The Individual hours for each day are stored in the Collection 'hoursofDays'
     *
     * @param view the view on which 'barChart' is located and will be drawn on.
     */
    public void initializeBarChart(View view)
    {
        BarChart barChart = view.findViewById(R.id.barChart);
        List<BarEntry> entries = new ArrayList<>();

        int xAxis = 1;
        for (float yAxis : hoursofDays)
        {
            entries.add(new BarEntry(xAxis++, yAxis / (1000 * 60 * 60)));
        }

        BarDataSet dataSet = new BarDataSet(entries, ""); // add entries to dataset

        dataSet.setColor(Color.GRAY);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueFormatter(new ChartFormatter());

        BarData barData = new BarData(dataSet);

        barChart.setTouchEnabled(false);
        barChart.getXAxis().setAxisMaximum(entries.size() + 1);
        barChart.getXAxis().setAxisMinimum(0);
        barChart.getAxisLeft().setAxisMaximum(12);
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisRight().setEnabled(false);
        barChart.getAxisLeft().setGranularity(1);
        barChart.getXAxis().setGranularity(1);
        barChart.getAxisLeft().setLabelCount(12);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setValueFormatter(new XAxisFormatter());
        barChart.getXAxis().setTextSize(8);
        barChart.getLegend().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.setData(barData);
        barChart.invalidate(); // refresh
    }

    /**
     * Initializes the Pie Chart
     * The Individual hours for each Project are stored in the Collection 'hoursPerProject'
     *
     * @param view the view on which 'pieChart'  is located and will be drawn on.
     */
    public void initializePieChart(View view)
    {
        PieChart pieChart = view.findViewById(R.id.pieChart);
        List<PieEntry> entries = new ArrayList<>();

        for (Map.Entry<String, Float> entry : hoursPerProject.entrySet())
        {
            entries.add(new PieEntry(entry.getValue(), entry.getKey()));
        }

        int[] colorArray = new int[]{Color.RED, Color.LTGRAY, Color.GREEN, Color.CYAN, Color.CYAN, Color.MAGENTA};
        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setColors(colorArray);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueFormatter(new ChartFormatter());
        dataSet.setValueTextSize(10);

        PieData pieData = new PieData(dataSet);

        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawEntryLabels(true);      //set to true for testing. perhaphs it should be true(?)
        pieChart.setCenterTextSize(12);
        pieChart.setCenterTextRadiusPercent(50);
        pieChart.setHoleRadius(45);
        pieChart.setTransparentCircleRadius(52);
        pieChart.setTransparentCircleAlpha(50);
        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private class XAxisFormatter implements IAxisValueFormatter
    {
        @Override
        public String getFormattedValue(float value, AxisBase axis)
        {
            switch ((int) value)
            {
                case 0:
                    return "";
                case 1:
                    return "Monday";
                case 2:
                    return "Tuesday";
                case 3:
                    return "Wednesday";
                case 4:
                    return "Thursday";
                case 5:
                    return "Friday";
                case 6:
                    return "Saturday";
                case 7:
                    return "Sunday";
                case 8:
                    return "";
                default:
                    return null;
            }
        }
    }

    private class ChartFormatter implements IValueFormatter
    {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler)
        {
            int hours = (int) value;
            int minutes = (int) ((value % 1) * 60);
            return String.format("%02dh %02dm", hours, minutes);

        }
    }
}
