package com.ssi.zeiterfassungsapp.layouts;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.util.NetworkReceiver;
import com.ssi.zeiterfassungsapp.util.NetworkUtil;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.util.List;

public class LoadingActivity extends AppCompatActivity implements AsyncDelegate
{
    private static final String TAG = "LoadingActivity";
    private NetworkReceiver receiver;
    private Webservice webservice;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        receiver = new NetworkReceiver();
        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        setContentView(R.layout.loading_screen);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        if(!webservice.loadAutoLogin())
        {
            webservice.logout();
            ListingProvider.getInstance().clearProjectsAndIssues();
        }
        else
        {
            webservice.loadIssues();
        }

        if(NetworkUtil.isConnected(getApplicationContext()))
        {
            webservice.checkToken();
        } else
        {
            startActivity(new Intent(this, LoginActivity.class).putExtra("callingActivity", TAG));
        }
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success)
    {
        if(success)
        {
            startActivity(new Intent(this, StartStopActivity.class).putExtra("callingActivity", TAG));
        }
        else
        {
            startActivity(new Intent(this, LoginActivity.class).putExtra("callingActivity", TAG));
        }

        finish();
    }

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}

    /**This methods will be called after Activity gets destroyed in order to unregister the Network receiver
     *
     */
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
