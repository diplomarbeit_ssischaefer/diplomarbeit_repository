package com.ssi.zeiterfassungsapp.layouts;

import android.app.Activity;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.ssi.zeiterfassungsapp.R;

/**
 * Implementation of Widget Configuration
 *
 * @author Bernhard Heiß
 * @since ?
 */
public class WidgetConfigurationActivity extends Activity
{
    int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Context context = WidgetConfigurationActivity.this;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            appWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.widget);
        appWidgetManager.updateAppWidget(appWidgetId, views);
        ComponentName name = new ComponentName(context.getApplicationContext(), WidgetActivity.class);
        int[] appWidgetIDs = appWidgetManager.getAppWidgetIds(name);

        //If more than one Widget exits
        if (appWidgetIDs.length == 1)
        {
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            setResult(RESULT_OK, resultValue);
        } else
        {
            AppWidgetHost host = new AppWidgetHost(context.getApplicationContext(), appWidgetIDs[1]);
            host.deleteAppWidgetId(appWidgetIDs[1]);

            Toast.makeText(context, "Only One Widget Instance is allowed", Toast.LENGTH_SHORT).show();
        }

        finish();
    }
}
