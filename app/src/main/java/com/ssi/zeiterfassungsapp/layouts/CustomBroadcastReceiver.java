package com.ssi.zeiterfassungsapp.layouts;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

import com.ssi.zeiterfassungsapp.R;

/**
 * Updates the Widget timer when receiving Broadcast from WidgetActivity
 *
 * @author Bernhard Heiß
 * @since
 */
public class CustomBroadcastReceiver extends android.content.BroadcastReceiver
{
    /**
     * Updates the tv_timer of the widget with the time of the time tracking.
     *
     * @param context the context of the running Widget
     * @param intent  Intent which the CustomBroadcastReceiver is set to receive. It is set to receive
     *                Broadcast from intent.ACTION_SCREEN_ON, Intent.ACTION_SCREEN_OFF,
     *                Intent.ACTION_USER_PRESENT, and ALARM_SERVICE.
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i("CustomBroadcastReceiver", intent.getAction());

        /*if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF))
        {

        }
        else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
        {

        }*/

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
        views.setTextViewText(R.id.tv_timer, getCurrentTime(context));

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        ComponentName theWidget = new ComponentName(context, WidgetActivity.class);

        manager.updateAppWidget(theWidget, views);
    }

    /**
     * Returns the Time between the start of the time tracking and now in the format "HH:mm".
     *
     * @param context the context of the running Widget
     * @return
     */
    public String getCurrentTime(Context context)
    {
        SharedPreferences sharedPrefs = context.getSharedPreferences("com.ssi.zeiterfassungsapp", Context.MODE_PRIVATE);
        Long startDate = sharedPrefs.getLong("startDateInMillis", 0);

        Long duration = System.currentTimeMillis() - startDate;
        int seconds = (int) (duration / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        minutes = minutes % 60;

        if (startDate == 0L)
        {
            return "00:00";
        } else
        {
            return String.format("%02d:%02d", hours, minutes);
        }
    }

}
