package com.ssi.zeiterfassungsapp.layouts;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Matthias Pöschl
 * @since 2019/07/23
 * Class that handles everything that is used for the addDialog
 */
public class AddDialog
{
    private WorktimelistAdapter adapter;
    private Date changedFrom;
    private Date changedTo;
    private String changedProjectKey;
    private String changedDescription;
    private Webservice webservice;
    private Dialog addDialog;
    private boolean OK;

    public AddDialog(Context applicationContext)
    {
        OK = false;
        adapter = WorktimelistAdapter.getInstance();
        webservice = Webservice.getInstance(applicationContext);
    }

    public View showAddDialog(Context context)
    {
        changedFrom = new Date();
        changedTo = new Date();
        changedTo.setMinutes(changedFrom.getMinutes()+1);
        changedProjectKey = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View addDialogView = inflater.inflate(R.layout.add_worktime_dialog, null);

        builder.setView(addDialogView);

        addDialog = builder.create();

        Toolbar tbDialog = addDialogView.findViewById(R.id.tb_top);
        tbDialog.setTitle("Add Worktime");

        WindowManager.LayoutParams params = addDialog.getWindow().getAttributes();
        params.y = 128;
        addDialog.getWindow().setAttributes(params);
        addDialog.getWindow().setGravity(Gravity.TOP);

        addDialog.show();

        return addDialogView;
    }

    public void initializeAddDialog(final Context context, final Context applicationContext, final int position, final View addDialogView)
    {
        final EditText etStarttime = addDialogView.findViewById(R.id.et_startDate);
        final EditText etEndtime = addDialogView.findViewById(R.id.et_endDate);
        final MaterialButton mbDropdown = addDialogView.findViewById(R.id.btn_dropdown);

        if(!webservice.loadUsername().equals("Guest"))
        {
            mbDropdown.getBackground().setAlpha(255);

            mbDropdown.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    showPopupMenu(context, view, mbDropdown);
                }
            });
        }
        else
        {
            mbDropdown.getBackground().setAlpha(69);
            mbDropdown.setText("No project selected");
        }
        
        MaterialButton mbConfirm = addDialogView.findViewById(R.id.mb_confirm);

        mbConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(changedFrom.getTime() > changedTo.getTime())
                {
                    Toast.makeText(applicationContext,"The starttime can not be set later than the endtime!", Toast.LENGTH_LONG).show();
                }
                else if((TimeUnit.MILLISECONDS.toHours(changedTo.getTime() - changedFrom.getTime())) > 12)
                {
                    Toast.makeText(applicationContext, "It is not allowed to work more than 12 hours!", Toast.LENGTH_LONG).show();
                }
                else if(mbDropdown.getText().equals("Select a project") && !webservice.loadUsername().equals("Guest"))
                {
                    Toast.makeText(applicationContext, "Please select a project first!", Toast.LENGTH_LONG).show();
                }
                else
                {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                    EditText etDescription = addDialogView.findViewById(R.id.et_description);
                    changedProjectKey = mbDropdown.getText().toString();
                    changedDescription = etDescription.getText().toString().equals("") ? "Default Bookingtext: Please update Bookingtext" : etDescription.getText().toString();

                    Worktime worktime = new Worktime(changedProjectKey, formatter.format(changedFrom), formatter.format(changedTo), changedDescription);

                    OK = true;

                    addDialog.dismiss();

                    webservice.saveWorktimeInDatabase(worktime, 3);


                }
            }
        });

        final MaterialButton mbCancel = addDialogView.findViewById(R.id.mb_cancel);

        mbCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OK = false;
                addDialog.dismiss();
            }
        });

        final Calendar c = Calendar.getInstance();
        final TimePickerDialog tpdStarttime = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener()
        {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes)
            {
                etStarttime.setText(String.format("%s, %d:%02d", etStarttime.getText().toString(), hourOfDay, minutes));
                changedFrom.setHours(hourOfDay);
                changedFrom.setMinutes(minutes);
            }
        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),true);

        final DatePickerDialog dpdStarttime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etStarttime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                changedFrom = calendar.getTime();
                tpdStarttime.show();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        final TimePickerDialog tpdEndtime = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener()
        {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes)
            {
                etEndtime.setText(String.format("%s, %d:%02d", etEndtime.getText().toString(), hourOfDay, minutes));
                changedTo.setHours(hourOfDay);
                changedTo.setMinutes(minutes);
            }
        }, (int)System.currentTimeMillis()/3600000,(int)System.currentTimeMillis()/60000,true);

        final DatePickerDialog dpdEndtime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etEndtime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                changedTo = calendar.getTime();
                tpdEndtime.show();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        etStarttime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdStarttime.show();
            }
        });

        etEndtime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdEndtime.show();
            }
        });

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(changedFrom);
        etStarttime.setText(String.format("%02d.%02d.%d, %d:%02d"
                , calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.MONTH) + 1
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)));
        calendar.setTime(changedTo);
        etEndtime.setText(String.format("%02d.%02d.%d, %d:%02d"
                , calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.MONTH) + 1
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)));
    }

    public void showPopupMenu(Context context,View view,final MaterialButton mbDropdown)
    {
        final PopupMenu popup = new PopupMenu(context, view);
        final List<SubMenu> submenuList = new LinkedList<>();

        // Inflate the menu from xml
        popup.inflate(R.menu.project_issue_menu);

        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                List<Issue> issueList = ListingProvider.getInstance().getIssuesOfProject(item.getTitle().toString());
                SubMenu currentSubMenu = null;

                if(!item.hasSubMenu())
                {
                    mbDropdown.setText(item.getTitle());
                }
                else
                {
                    for(SubMenu subMenu : submenuList)
                    {
                        if(subMenu.getItem().getTitle().equals(item.getTitle()))
                        {
                            subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            currentSubMenu = subMenu;
                        }
                    }
                    for (Issue issue : issueList)
                    {
                        currentSubMenu.add(issue.getKey());
                    }
                }

                return true;
            }
        });

        List<Project> projectList = ListingProvider.getInstance().getProjects();

        for (Project p : projectList)
        {
            SubMenu helpMenu = popup.getMenu().addSubMenu(p.getProjectKey());
            helpMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            submenuList.add(helpMenu);
        }

        popup.show();
    }

    public boolean isOK()
    {
        return OK;
    }
}
