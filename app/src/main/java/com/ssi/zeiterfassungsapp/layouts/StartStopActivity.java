package com.ssi.zeiterfassungsapp.layouts;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Implementation of App Widget functionality.
 * @author Heiß & Pöschl & Schwarzkogler
 * @since 2019/07/08
 */

public class StartStopActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncDelegate
{
    private static final String TAG = "StartStopActivity";
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private int seconds;
    private int minutes;
    private int hours;
    private TextView tvStartTime;
    private TextView tvTimer;
    private MaterialButton btnTimer;
    private Webservice webservice;
    private Timer workTimer;
    private Timer notificationTimer;
    private SharedPreferences sharedPrefs;
    private Worktime worktime;
    private WorktimelistAdapter worktimelistAdapter;
    private boolean runningWork = false;
    private MaterialButton mtbDropdown;
    private LinkedList<Worktime> unsyncedWorktimes;
    private static int isStartedByWidget;

    @Override
    protected void onResume()
    {
        super.onResume();
        setContentView(R.layout.startstop_menu);
        worktimelistAdapter = WorktimelistAdapter.getInstance();

        worktime = new Worktime();
        hours = 0;
        minutes = 0;
        seconds = 0;

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);
        unsyncedWorktimes = new LinkedList<>();

        sharedPrefs = getSharedPreferences("com.ssi.zeiterfassungsapp", MODE_PRIVATE);

        toolbar = findViewById(R.id.toolbar);
        tvTimer = findViewById(R.id.tv_timer);
        tvStartTime = findViewById(R.id.tv_startTime);
        mtbDropdown = findViewById(R.id.btn_dropdown);

        if(!webservice.loadUsername().equals("Guest") && !webservice.loadStandardIssue().isEmpty() && !webservice.loadStandardIssue().equals("Select issue"))
        {
            mtbDropdown.setText(webservice.loadStandardIssue());
        }
        else
        {
            mtbDropdown.setText("Select a project");
        }

        if(webservice.loadUsername().equals("Guest"))
        {
            mtbDropdown.setClickable(false);
            mtbDropdown.getBackground().setAlpha(69);
        }
        else
        {
            mtbDropdown.setClickable(true);
            mtbDropdown.getBackground().setAlpha(255);

            mtbDropdown.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    showPopupMenu(view);
                }
            });
        }

        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if(!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        }
        else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                startActivity(new Intent(StartStopActivity.this, LoginActivity.class));
                finish();
            }
        });

        btnTimer = findViewById(R.id.btn_Timer);

        //Sets an OnClickListener on the button which will start the time when being clicked.
        btnTimer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(btnTimer.getText().equals("Start Timer"))
                {
                    if((!mtbDropdown.getText().toString().equals("Select a project") && !webservice.loadUsername().equals("Guest")) || webservice.loadUsername().equals("Guest"))
                    {
                        startWork();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Issue not selected", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    stopWork();
                }
            }
        });

        long startDate = sharedPrefs.getLong("startDateInMillis", 0);

        if(startDate != 0)
        {
            loadRunningWork();
        }


        /**
         * If the StartStopActivity is called from the widget
         * the work will be stopped and the dialog will show up
         */
        Bundle bundle = getIntent().getExtras();
        isStartedByWidget = 0;
        if(bundle != null)
        {
            isStartedByWidget = bundle.getInt("widget",0);
        }

        if(isStartedByWidget == 1)
        {
            stopWork();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    /**
     * This function will be called when there is an existing startDate in the
     * SharedPreferences. The main purpose of this function is to start the timer
     * from the startDate in the Sharedpreferences and to display the startdate
     */
    public void loadRunningWork()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        long startDate = sharedPrefs.getLong("startDateInMillis", 0);
        worktime = new Worktime(sharedPrefs.getString("unfinishedWork", ""));

        Long duration = System.currentTimeMillis() - startDate;
        seconds = (int) (duration / 1000);
        minutes = seconds / 60;
        seconds = seconds % 60;
        hours = minutes / 60;
        minutes = minutes % 60;

        runningWork = true;
        mtbDropdown.setText(worktime.getIssueKey());
        startTimer(new Date(startDate));

        tvStartTime.setText("Timer started at: " + sdf.format(startDate));
        btnTimer.setText("Stop Timer");
    }

    /**
     * This function will be called after the user successfully pressed the workTimer button
     * Its main purpose is to start a work (Inserts the unfinished Worktime into the database
     * and to start the Timer which will be displayed on the main screen
     */
    public void startWork()
    {
        //TODO: Test Worktime:
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        tvStartTime.setText("Timer started at: "+sdf.format(System.currentTimeMillis()));

        Date date = new Date();
        date.setTime(System.currentTimeMillis());

        sharedPrefs.edit().putLong("startDateInMillis", date.getTime()).apply();

        String testIssueKey = mtbDropdown.getText().toString().equals("Select a project") ? "No project selected" : mtbDropdown.getText().toString(); //<-- Test project in order to add a project -> Later it will get a Project from the DropDown
        worktime.setIssueKey(testIssueKey);

        if(mtbDropdown.getText().toString().equals("Select a project"))
        {
            mtbDropdown.setText("No project selected");
        }

        worktime.setStartDate(date);
        worktime.setEndDateString("");

        sharedPrefs.edit().putString("unfinishedWork", worktime.toString()).apply(); //Reda Object of Worktime
        webservice.saveWorktimeInDatabase(worktime, 3); //Saves the running Worktime

        startTimer(date);

        btnTimer.setText("Stop Timer");

        /*send broadcast to widget to initiate an update */

        Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        Bundle bundle = new Bundle();
        bundle.putString("startorstop", "start");
        intent.putExtras(bundle);
        getApplicationContext().sendBroadcast(intent);
    }

    /**
     * This function will be called when an existing Worktime gets loaded or a Worktime
     * gets started. The main purpose of this function is to start the timer on a given time
     * @param startDate Date Object of the given StartDate
     */
    public void startTimer(Date startDate)
    {
        mtbDropdown.setEnabled(false);
        mtbDropdown.getBackground().setAlpha(69);
        workTimer = new Timer();

        //Sets an TimerTask for the Timer.
        //The advantage of using a Timer with a TimerTask is, that you can set a delay for the start
        // and also set an period for the thread which is the time the Thread has to wait between
        // each new run. The period could also seen as equal to Thread.sleep()
        workTimer.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                //The runOnUiThread is an type of Thread which does everything in a "worker thread", but sends the result to the main thread.
                //This must be used in case the thread  wants to changed something on the UI.
                //In this case, the thread will change an TextView.
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //Checks the hours, minutes and seconds, if one of these is smaller than 10, a left-handed zero will be added.
                        if(hours < 10 && minutes < 10 && seconds < 10)
                            tvTimer.setText("0"+hours+":0"+minutes+":0"+seconds);
                        if(hours < 10 && minutes < 10 && seconds > 10)
                            tvTimer.setText("0"+hours+":0"+minutes+":"+seconds);
                        if(hours < 10 && minutes > 10 && seconds > 10)
                            tvTimer.setText("0"+hours+":"+minutes+":"+seconds);
                        if(hours > 10 && minutes > 10 && seconds > 10)
                            tvTimer.setText(hours+":"+minutes+":"+seconds);
                        if(hours > 10 && minutes < 10 && seconds < 10)
                            tvTimer.setText(hours+":0"+minutes+":0"+seconds);
                        if(hours > 10 && minutes > 10 && seconds < 10)
                            tvTimer.setText(hours+":"+minutes+":0"+seconds);
                        if(hours > 10 && minutes < 10 && seconds > 10)
                            tvTimer.setText(hours+":0"+minutes+":"+seconds);
                        if(hours < 10 && minutes > 10 && seconds < 10)
                            tvTimer.setText("0"+hours+":"+minutes+":0"+seconds);

                        //Converts the seconds into minutes and the minutes into hours
                        seconds++;
                        if(seconds == 60)
                        {
                            minutes++;
                            seconds = 0;
                        }
                        if(minutes == 60)
                        {
                            hours++;
                            minutes = 0;
                        }
                    }});
            }
        }, 1000, 1000);

        if(sharedPrefs.getBoolean("notificationAfter",true))
        {
            try
            {
                scheduleStopNotification(startDate);
            }
            catch (ParseException e)
            {
                Log.e(TAG, "Unexpected Parse Exception");
            }
        }
    }

    public void stopWork()
    {
        worktime = new Worktime(sharedPrefs.getString("unfinishedWork", "")); //Converts String to Worktime Object
        worktime.setEndDate(new Date(System.currentTimeMillis()));
        long difference = worktime.getEndDate().getTime() - worktime.getStartDate().getTime();

        if((difference/60000L) > 0)
        {
                showStopDialog();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Start and end time must be one minute apart", Toast.LENGTH_LONG).show();
        }
    }

    public void showPopupMenu(View view)
    {
        final PopupMenu popup = new PopupMenu(this, view);
        final List<SubMenu> submenuList = new LinkedList<>();
        // Inflate the menu from xml
        popup.inflate(R.menu.project_issue_menu);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                List<Issue> issueList = ListingProvider.getInstance().getIssuesOfProject(item.getTitle().toString());
                SubMenu currentSubMenu = null;

                if(!item.hasSubMenu())
                {
                    mtbDropdown.setText(item.getTitle());
                }
                else
                {
                    for(SubMenu subMenu : submenuList)
                    {
                        if(subMenu.getItem().getTitle().equals(item.getTitle()))
                        {
                            subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            currentSubMenu = subMenu;
                        }
                    }

                    for (Issue issue : issueList)
                    {
                        currentSubMenu.add(issue.getKey());
                    }
                }
                return true;
            }
        });

        List<Project> projectList = ListingProvider.getInstance().getProjects();

        for (Project p : projectList)
        {
            SubMenu helpMenu = popup.getMenu().addSubMenu(p.getProjectKey());
            helpMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            submenuList.add(helpMenu);
        }

        popup.show();
    }

    public void showStopDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View stopDialogView = inflater.inflate(R.layout.stop_worktime_dialog, null);

        builder.setView(stopDialogView);

        final Dialog stopDialog = builder.create();
        final Toolbar tbDialog = stopDialogView.findViewById(R.id.tb_top);
        final EditText etDescription = stopDialogView.findViewById(R.id.et_description);
        final MaterialButton mbConfirm = stopDialogView.findViewById(R.id.mb_confirm);
        final MaterialButton mbCancel = stopDialogView.findViewById(R.id.mb_cancel);

        tbDialog.setTitle("Save Worktime");
        etDescription.setSingleLine();

        stopDialog.show();

        mbConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                workTimer.cancel();
                notificationTimer.cancel();
                btnTimer.setText("Start Timer");

                if(isStartedByWidget == 1)
                {
                    String str = "";
                    if (!worktime.getIssueKey().equals("No project selected"))
                    {
                        str = webservice.loadStandardIssue().toString();
                        worktime.setIssueKey(str);
                    }
                }

                worktime.setBookingtext(etDescription.getText().toString().equals("") ? "Default Bookingtext: Please update Bookingtext" : etDescription.getText().toString());
                webservice.stopWorktime(worktime);
                webservice.saveWorktimeInDatabase(worktime, 3);
                webservice.listAllWorktimes();
                System.out.println(worktimelistAdapter.getUnfilteredWorktimeList());
                //webservice.listAllWorktimes();
                Log.e(TAG, "Stop:" + worktime.toString());
                sharedPrefs.edit().remove("startDateInMillis").apply();
                sharedPrefs.edit().remove("unfinishedWork").apply();
                hours = 0;
                minutes = 0;
                seconds = 0;
                runningWork = false;

                if(!webservice.loadUsername().equals("Guest") && !webservice.loadStandardIssue().isEmpty() && !webservice.loadStandardIssue().equals("Select issue"))
                {
                    mtbDropdown.setEnabled(true);
                    mtbDropdown.setText(webservice.loadStandardIssue());
                    mtbDropdown.getBackground().setAlpha(255);
                }
                else
                {
                    mtbDropdown.setEnabled(false);
                    mtbDropdown.setText("Select a project");
                    mtbDropdown.getBackground().setAlpha(69);
                }

                stopDialog.dismiss();

                /*send broadcast to widget to initiate an update */
                Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                Bundle bundle = new Bundle();
                bundle.putString("startorstop", "stop");
                intent.putExtras(bundle);
                getApplicationContext().sendBroadcast(intent);
            }
        });

        mbCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                stopDialog.dismiss();
            }
        });
    }

    public void scheduleStopNotification(final Date startDate) throws ParseException
    {
        final Date notificationDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date date = sdf.parse(sharedPrefs.getString("notificationAfterStatus", "08:15"));

        notificationDate.setHours(startDate.getHours() + date.getHours());
        notificationDate.setMinutes(startDate.getMinutes() + date.getMinutes());

        Long duration = notificationDate.getTime() - startDate.getTime();

        final int minutes = (int) (duration / 60000 % 60);
        final int hours = (int) (duration / 3600000);

        notificationTimer = new Timer();
        notificationTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                webservice.sendNotification("Warning",String.format("You already worked %02d:%02d", hours,minutes));
            }
        },notificationDate);
    }

    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
        }
        else if (id == R.id.nav_worktime)
        {
            startActivity(new Intent(StartStopActivity.this, WorktimeActivity.class).putExtra("callingActivity", TAG));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_projects)
        {
            startActivity(new Intent(StartStopActivity.this, ProjectActivity.class).putExtra("callingActivity", TAG));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_statistics)
        {
            startActivity(new Intent(StartStopActivity.this, StatisticActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_export)
        {
            startActivity(new Intent(StartStopActivity.this, ExportActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_settings)
        {
            startActivity(new Intent(StartStopActivity.this, SettingsActivity.class).putExtra("callingActivity", TAG));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        if(intent.getStringExtra("MethodName") == null)
        {

        }
        else if(intent.getStringExtra("MethodName").equals("Stop"))
        {
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(0);
            stopWork();
        }
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success)
    {
        if(success)
        {
            //unsyncedWorktimes.clear();
            List<Worktime> syncedWorktimes = ListingProvider.getInstance().getWorktimesDateSpecific();

            for (Worktime w : syncedWorktimes)
            {
                if(!unsyncedWorktimes.contains(w))
                {
                    unsyncedWorktimes.add(w);
                }
            }

            webservice.syncDatabaseWithWebservice(unsyncedWorktimes);
        }
    }

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    /**This method is used to notify the Activity that the List of Worktimes is now up-to-date
     *
     * @param worktimes List of Worktimes in the database
     * @param usage
     */
    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage)
    {
        if(usage == 0)
        {
            //FIXME: Always throws Exception (null pointer reference on virtual method), without below line, everything works as expected so far, should work now (if below line not needed)
            //worktimelistAdapter.setUnfilteredWorktimeList(worktimes);
        }

        if(usage == 1 && worktimes.size() > 0) //Will be true when the usage is 1 and the list contains at least one item
        {
            if(!webservice.loadUsername().equals("Guest"))
            {
                unsyncedWorktimes = new LinkedList<>(worktimes);
                List<Date> modifiedDays = new LinkedList<>();

                for(Worktime worktime : worktimes)
                {
                    if(!modifiedDays.contains(worktime.getStartDate()))
                    {
                        modifiedDays.add(worktime.getStartDate());
                    }
                }

                Collections.sort(modifiedDays);

                webservice.listWorktimesOfDay(((LinkedList<Date>) modifiedDays).getFirst(), ((LinkedList<Date>) modifiedDays).getLast());
            }
            else
            {
                worktimelistAdapter.setUnfilteredWorktimeList(worktimes);
            }
        }

        if(usage == 3)
        {
            worktimelistAdapter.setUnfilteredWorktimeList(worktimes);
        }
    }

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success)
    {
        int usage = 1;
        if(isStartedByWidget == 1)
        {
            usage = 3;
        }

        webservice.loadWorktimesFromDatabase(usage);
    }

    @Override
    public void postingWorktimeCompleted(boolean success) {}
}
