package com.ssi.zeiterfassungsapp.layouts;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.StatisticsTabAdapter;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is used to export all issues of the current user.
 *
 * @author Heiß & Schweiger
 * @since 2019/07/08
 */
public class ExportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncDelegate
{
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private Webservice webservice;
    private TextInputEditText etEmail;
    private TextInputEditText etStartDate;
    private TextInputEditText etEndDate;
    private TextInputEditText etMessage;
    private ListingProvider listingProvider;
    private WorktimelistAdapter adapter;
    private LinkedList<Worktime> unsyncedWorktimes;
    private StatisticsTabAdapter myPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.export_menu);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        adapter = WorktimelistAdapter.getInstance();
        listingProvider = ListingProvider.getInstance();
        webservice.listAllWorktimes();

        webservice.loadWorktimesFromDatabase(1);    //usage 0 oder 1 ??
        unsyncedWorktimes = (LinkedList) adapter.getUnfilteredWorktimeList();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Export");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if (!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        } else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                startActivity(new Intent(ExportActivity.this, LoginActivity.class));
                finish();
            }
        });

        etEmail = findViewById(R.id.et_email);
        etStartDate = findViewById(R.id.et_startDate);
        etEndDate = findViewById(R.id.et_endDate);
        etMessage = findViewById(R.id.et_message);
        MaterialButton btnSend = findViewById(R.id.btn_send);

        etStartDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openDatePicker(etStartDate);
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openDatePicker(etEndDate);
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!webservice.loadUsername().equals("Guest"))
                {
                    onExportTime();
                } else
                {
                    Toast.makeText(getApplicationContext(), "Exporting is not allowed for Guest.\nPlease log in", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Checks the InputsFields and creates and Sends a Email.
     * Only Works with a @ssi-schaefer.com mail.
     */
    public void onExportTime()
    {
        if (etEmail.getText().toString().equals(""))
        {
            etEmail.setError("E-Mail should not be empty");
        }
        else if (!etEmail.getText().toString().matches("^.+\\..+@ssi-schaefer.com$"))
        {
            etEmail.setError("Incorrect E-Mail");
        }
        else
        {
            String email = etEmail.getText().toString().trim();
            String startDateStr = etStartDate.getText().toString();
            String endDateStr = etEndDate.getText().toString(); //"15.02.2020";/
            String message = etMessage.getText().toString();

            createCSVFile(startDateStr, endDateStr);

            //Send Mail
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_GRANT_PREFIX_URI_PERMISSION);
            emailIntent.setType("message/rfc822");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Worktimes from " + startDateStr + " to " + endDateStr);
            emailIntent.putExtra(Intent.EXTRA_TEXT, message);

            File csvFile = new File(getFilesDir(), "worktimes.csv");
            Uri testURI = FileProvider.getUriForFile(getApplicationContext(), "com.ssi.zeiterfassungsapp.provider", csvFile);

            emailIntent.putExtra(Intent.EXTRA_STREAM, testURI);

            try
            {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
            catch (android.content.ActivityNotFoundException ex)
            {
                Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Creates a 'worktimes.csv' file containing all worktimes in startDate to endDate
     * @param startDateStr StartDate of the worktimes
     * @param endDateStr   EndDate of the worktimes
     */
    public void createCSVFile(String startDateStr, String endDateStr)
    {
        listingProvider = ListingProvider.getInstance();
        System.out.println(startDateStr + " ; " + endDateStr);

        Date startDate = null;
        Date endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        try
        {
            startDate = sdf.parse(startDateStr);
            endDate = sdf.parse(endDateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        ArrayList<Worktime> workTimes = listingProvider.filterWorktimes("All Projects", startDate.getTime(), endDate.getTime(), true);

        try
        {
            File csvFile = new File(getFilesDir(), "worktimes.csv");
            // CSV header
            // email;projectID;starttime;stoptime;accomplishedwork/comment
            PrintWriter writer = new PrintWriter(csvFile);

            writer.append("email;");
            writer.append("projectID;");
            writer.append("starttime;");
            writer.append("stoptime;");
            writer.append("accomplishedwork/comment");
            writer.append("\n");

            for (Worktime worktime : workTimes)
            {
                writer.append(webservice.loadUsername() + ";");
                writer.append(worktime.getIssueKey() + ";");
                writer.append(worktime.getStartDateString() + ";");
                writer.append(worktime.getEndDateString() + ";");
                writer.append(worktime.getBookingtext() + "\n");
            }

            writer.flush();
            writer.close();

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * This method opens a date picker dialog for the user to select a specific date in time.
     * @param etTime - TextInputEditText field to display the selected time
     */
    public void openDatePicker(final TextInputEditText etTime)
    {
        final Calendar c = Calendar.getInstance();

        final DatePickerDialog dpdDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                etTime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        dpdDate.show();
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, StartStopActivity.class));
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            startActivity(new Intent(ExportActivity.this, StartStopActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_worktime)
        {
            startActivity(new Intent(ExportActivity.this, WorktimeActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_projects)
        {
            startActivity(new Intent(ExportActivity.this, ProjectActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_statistics)
        {
            startActivity(new Intent(ExportActivity.this, StatisticActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_export)
        {
        }
        else if (id == R.id.nav_settings)
        {
            startActivity(new Intent(ExportActivity.this, SettingsActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage)
    {
        adapter.setUnfilteredWorktimeList(worktimes);
        webservice.listAllWorktimes();
        System.out.println(listingProvider.getWorktimes());
        listingProvider.addLocalWorktimes(worktimes);

        if (usage == 0)
        {
            adapter.setUnfilteredWorktimeList(worktimes);
            webservice.listAllWorktimes();

        }

        if (usage == 1 && worktimes.size() > 0) //Will be true when the usage is 1 and the list contains at least one item
        {
            if (!webservice.loadUsername().equals("Guest"))
            {
                unsyncedWorktimes = new LinkedList<>(worktimes);
                List<Date> modifiedDays = new LinkedList<>();

                for (Worktime worktime : worktimes)
                {
                    if (!modifiedDays.contains(worktime.getStartDate()))
                    {
                        modifiedDays.add(worktime.getStartDate());
                    }
                }

                Collections.sort(modifiedDays);

                webservice.listWorktimesOfDay(((LinkedList<Date>) modifiedDays).getFirst(), ((LinkedList<Date>) modifiedDays).getLast());
            }
            else
            {
                adapter.setUnfilteredWorktimeList(worktimes);
            }
        }
    }

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}
}
