package com.ssi.zeiterfassungsapp.layouts;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author Matthias Pöschl & Bernhard Heiß
 * @since 2019/07/19
 * A simple {@link Fragment} subclass.
 */
public class StatisticsYearly extends Fragment
{
    private ArrayList<Worktime> workTime = new ArrayList<>();
    private ListingProvider listingProvider;
    private LinkedList<Float> hoursofMonths = new LinkedList<>();
    private HashMap<String, Float> hoursPerProject = new HashMap<>();
    private boolean synced = false;

    public StatisticsYearly()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics_yearly, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Toolbar tbBarChart = view.findViewById(R.id.tb_BarChart);
        Toolbar tbPieChart = view.findViewById(R.id.tb_pieChart);

        tbBarChart.setTitle("Monthly Working Hours");
        tbPieChart.setTitle("Hours by Project");

        listingProvider = ListingProvider.getInstance();
        Webservice webservice = Webservice.getInstance();
        synced = !webservice.isGuest();

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));                                             //Hardcode timezone
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        cal.set(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.MONTH, Calendar.JANUARY);

        long startofYear = cal.getTimeInMillis();
        Calendar cal2 = (Calendar) cal.clone();
        cal2.add(Calendar.YEAR, 1);
        long endofYear = cal2.getTimeInMillis();

        workTime = listingProvider.filterWorktimes("All Projects", startofYear, endofYear, synced);

        hoursofMonths(cal);
        hoursPerProject();

        initializeBarChart(view);
        initializePieChart(view);
    }

    /**
     * Get the working hours of the months
     *
     * @param currMonth
     */
    public void hoursofMonths(Calendar currMonth)
    {
        currMonth.set(Calendar.HOUR_OF_DAY, 0);
        currMonth.set(Calendar.MINUTE, 0);

        Calendar nextMonth = (Calendar) currMonth.clone();
        nextMonth.add(Calendar.MONTH, 1);

        for (int i = 1; i <= 12; i++)  //12 months
        {
            float sumofMonth = 0;

            ArrayList<Worktime> singleMonth = listingProvider.filterWorktimes("All Projects", currMonth.getTimeInMillis(), nextMonth.getTimeInMillis(), true);

            for (Worktime obj : singleMonth)
            {
                float diff = Math.abs(obj.getEndDate().getTime() - obj.getStartDate().getTime());
                sumofMonth += diff;
            }

            hoursofMonths.add(sumofMonth);  //add single Month to list
            nextMonth.add(Calendar.MONTH, 1);
            currMonth.add(Calendar.MONTH, 1);
        }
    }

    public void hoursPerProject()
    {
        ArrayList<Project> projectsList = new ArrayList<>();

        for (Worktime worktime : workTime)
        {
            String projectkey = worktime.getIssueKey().split("-")[0];
            projectsList.add(new Project(projectkey, projectkey));
        }

        for (Project project : projectsList)
        {
            ArrayList<Worktime> projWorkTimeList = listingProvider.getWorktimesOfProject(project.getProjectKey().toUpperCase(), workTime.toArray(new Worktime[workTime.size()]));

            float sumofProject = 0;
            for (Worktime obj : projWorkTimeList)
            {
                float diff_TMP = Math.abs(obj.getEndDate().getTime() - obj.getStartDate().getTime());
                float diff = ((diff_TMP / (1000 * 60 * 60)));
                sumofProject += diff;
            }

            hoursPerProject.put(project.getProjectName(), sumofProject);
        }

    }

    public void initializeBarChart(View view)
    {
        BarChart barChart = view.findViewById(R.id.barChart);
        List<BarEntry> entries = new ArrayList<>();

        int xAxis = 1;
        for (float yAxis : hoursofMonths)
        {
            entries.add(new BarEntry(xAxis++, yAxis / (1000 * 60 * 60)));
        }

        BarDataSet dataSet = new BarDataSet(entries, ""); // add entries to dataset

        dataSet.setColor(Color.GRAY);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueFormatter(new ChartFormatter());
        dataSet.setValueTextSize(5);

        BarData barData = new BarData(dataSet);

        barChart.setTouchEnabled(false);
        barChart.getXAxis().setAxisMaximum(entries.size() + 1);
        barChart.getXAxis().setAxisMinimum(0);
        barChart.getAxisLeft().setAxisMaximum(340);
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisRight().setEnabled(false);
        barChart.getAxisLeft().setGranularity(50);
        barChart.getXAxis().setGranularity(1);
        barChart.getXAxis().setLabelCount(12);
        barChart.getAxisLeft().setLabelCount(20);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setValueFormatter(new XAxisFormatter());
        barChart.getXAxis().setTextSize(8);
        barChart.getLegend().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.setData(barData);
        barChart.invalidate(); // refresh
    }

    public void initializePieChart(View view)
    {
        PieChart pieChart = view.findViewById(R.id.pieChart);
        List<PieEntry> entries = new ArrayList<>();

        for (Map.Entry<String, Float> entry : hoursPerProject.entrySet())
        {
            entries.add(new PieEntry(entry.getValue(), entry.getKey()));
        }

        int[] colorArray = new int[]{Color.RED, Color.LTGRAY, Color.GREEN, Color.CYAN, Color.CYAN, Color.MAGENTA};
        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setColors(colorArray);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueFormatter(new ChartFormatter());
        dataSet.setValueTextSize(10);

        PieData pieData = new PieData(dataSet);

        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawEntryLabels(true);
        pieChart.setCenterTextSize(12);
        pieChart.setCenterTextRadiusPercent(50);
        pieChart.setHoleRadius(45);
        pieChart.setTransparentCircleRadius(52);
        pieChart.setTransparentCircleAlpha(50);
        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private class XAxisFormatter implements IAxisValueFormatter
    {
        @Override
        public String getFormattedValue(float value, AxisBase axis)
        {
            if (value == 13f)
            {
                return "";
            }

            return "" + (int) value;
        }
    }

    private class ChartFormatter implements IValueFormatter
    {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler)
        {
            int hours = (int) value;
            int minutes = (int) ((value % 1) * 60);
            return String.format("%02dh %02dm", hours, minutes);
        }
    }
}
