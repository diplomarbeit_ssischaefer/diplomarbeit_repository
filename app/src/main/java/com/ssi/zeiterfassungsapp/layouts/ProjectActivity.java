package com.ssi.zeiterfassungsapp.layouts;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.ProjectListAdapter;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import petrov.kristiyan.colorpicker.ColorPicker;

/**
 * This class is used to display all projects of the current user.
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class ProjectActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncDelegate, ProjectListAdapter.ItemClickListener
{
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private Webservice webservice;
    private RecyclerView.Adapter adapter;
    private Gson gson = new Gson();
    private HashMap<Integer, Integer> projectColors = new HashMap<>();
    private FloatingActionButton fabColor;
    private boolean showColor = false;
    private ArrayList<Project> projectList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_list);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Projects");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        fabColor = findViewById(R.id.fab_color);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if(!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        }
        else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
            fabColor.hide();
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                startActivity(new Intent(ProjectActivity.this, LoginActivity.class));
                finish();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this);
        adapter = new ProjectListAdapter(getApplicationContext());

        loadSharedPreferences();

        projectList = ListingProvider.getInstance().getProjects();

        addProjectList(projectList);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_issues);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(llm);
        ((ProjectListAdapter) adapter).setClickListener(this);

        final SwipeRefreshLayout refreshLayout = findViewById(R.id.srl_projects);

        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        refreshLayout.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorPrimaryDark));

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener()
                {
                    @Override
                    public void onRefresh()
                    {
                        (new Handler()).postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                webservice.loadIssues();
                                refreshLayout.setRefreshing(false);
                            }
                        }, 2000);
                    }
                }
        );
    }

    @Override
    public void onItemClick(View view, final int position)
    {
        if(view.getId() == R.id.ll_projecttask)
        {
            Intent intent = new Intent(ProjectActivity.this, IssueActivity.class);

            intent.putExtra("PROJECTCOLOR", projectColors.get(position));
            intent.putExtra("PROJECTKEY", projectList.get(position).getProjectKey());

            startActivity(intent);
            overridePendingTransition(R.layout.animation_from_right, R.layout.animation_to_left);
            finish();
        }
        else
        {
            if(showColor)
            {
                final ColorPicker colorPicker = new ColorPicker(ProjectActivity.this);

                colorPicker.setTitle("Choose a color for this project");
                colorPicker.getPositiveButton().setTextColor(Color.BLACK);
                colorPicker.show();

                colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener()
                {
                    @Override
                    public void onChooseColor(int pos, int color)
                    {
                        ((ProjectListAdapter) adapter).setColor(color, position);

                        projectColors.put(position, color);

                        String projectColorsString = gson.toJson(projectColors);
                        webservice.storeProjectColor(projectColorsString);
                    }

                    @Override
                    public void onCancel()
                    {
                        colorPicker.dismissDialog();
                    }
                });
            }
        }
    }

    /**
     * This method adds a list of projects to the adapter to make them visible for the user.
     *
     * @param projectList - List of projects to add
     */
    public void addProjectList(ArrayList<Project> projectList)
    {
        for (Project project: projectList)
        {
            ((ProjectListAdapter) adapter).addProject(project);
        }
    }

    /**
     * This method loads the options the user entered in the previous session of the application
     * for the project menu.
     */
    public void loadSharedPreferences()
    {
        String projectColorsString = webservice.loadProjectColor();
        Type type = new TypeToken<HashMap<Integer, Integer>>(){}.getType();

        projectColors = gson.fromJson(projectColorsString, type);

        if(projectColorsString.equals(""))
        {
            projectColors = new HashMap<>();
            projectColors.put(0,0);
        }
    }

    /**
     * This method makes the colors used on the projects visible or invisible
     */
    public void showColors(View view)
    {
        if(!webservice.loadUsername().equals("Guest"))
        {
            if(showColor)
            {
                for(Map.Entry<Integer, Integer> entry : projectColors.entrySet())
                {
                    int pos = entry.getKey();

                    ((ProjectListAdapter) adapter).setColor(Color.BLACK, pos);
                }

                fabColor.setImageResource(R.drawable.ic_colors);
            }
            else
            {
                for(Map.Entry<Integer, Integer> entry : projectColors.entrySet())
                {
                    int pos = entry.getKey();
                    int color = entry.getValue();

                    ((ProjectListAdapter) adapter).setColor(color, pos);
                }

                fabColor.setImageResource(R.drawable.ic_clear_color);
            }

            showColor = !showColor;
        }
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, StartStopActivity.class));
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            startActivity(new Intent(ProjectActivity.this, StartStopActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_worktime)
        {
            startActivity(new Intent(ProjectActivity.this, WorktimeActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_projects)
        {
        }
        else if (id == R.id.nav_statistics)
        {
            startActivity(new Intent(ProjectActivity.this, StatisticActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_export)
        {
            startActivity(new Intent(ProjectActivity.this, ExportActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_settings)
        {
            startActivity(new Intent(ProjectActivity.this, SettingsActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}
}
