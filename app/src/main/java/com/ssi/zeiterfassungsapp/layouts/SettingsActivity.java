package com.ssi.zeiterfassungsapp.layouts;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is used for adjustable settings preferred by the user.
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class SettingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncDelegate
{
    private SharedPreferences sharedPreferences;
    protected DrawerLayout drawer;
    protected NavigationView navigationView;
    protected Toolbar toolbar;
    private Webservice webservice;
    private TextView notificationAfter;
    private Switch switchLogin;
    private Switch switchNotifications;
    private TextView notificationAfterText;
    private boolean showNotification;
    private String notificationTime;
    private Switch switchStandardIssue;
    private TextView standardIssueName;
    private boolean enableStandardIssue;
    private MaterialButton mbIssueDropdown;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        webservice = Webservice.getInstance(getApplicationContext());
        webservice.setListener(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Settings");
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvFirstname = headerLayout.findViewById(R.id.tv_firstname);
        TextView tvLastname = headerLayout.findViewById(R.id.tv_lastname);

        if(!webservice.loadUsername().equals("Guest"))
        {
            String username = webservice.loadUsername().split("@")[0];

            tvFirstname.setText(username.split("\\.")[0].substring(0, 1).toUpperCase() + username.split("\\.")[0].substring(1));
            tvLastname.setText(username.split("\\.")[1].substring(0, 1).toUpperCase() + username.split("\\.")[1].substring(1));
        }
        else
        {
            tvFirstname.setText("");
            tvLastname.setText("Guest");
        }

        Button signOut = findViewById(R.id.btn_sign_out);

        signOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                webservice.logout();
                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                finish();
            }
        });

        notificationAfter = findViewById(R.id.tv_notificationTime);
        switchLogin = findViewById(R.id.switch_autologin);
        switchNotifications = findViewById(R.id.switch_showNotifications);
        notificationAfterText = findViewById(R.id.tv_notificationAfter);
        switchStandardIssue = findViewById(R.id.switch_standardIssue);
        standardIssueName = findViewById(R.id.tv_standardIssueName);
        mbIssueDropdown = findViewById(R.id.btn_dropdown);
        TextView autoLoginText = findViewById(R.id.tv_autoLogin);
        TextView standardIssueText = findViewById(R.id.tv_standardIssueText);

        sharedPreferences = getSharedPreferences("com.ssi.zeiterfassungsapp", MODE_PRIVATE);

        loadSharedPreferences(sharedPreferences);

        if(webservice.loadUsername().equals("Guest"))
        {
            autoLoginText.setTextColor(Color.LTGRAY);
            switchLogin.setEnabled(false);
            switchLogin.setClickable(false);
            switchLogin.setChecked(false);
        }
        else
        {
            autoLoginText.setTextColor(Color.parseColor("#B5000000"));
            switchLogin.setEnabled(true);
            switchLogin.setClickable(true);

            switchLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    webservice.storeAutoLogin(isChecked);
                    switchLogin.setChecked(isChecked);
                }
            });
        }

        switchNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    notificationAfterText.setTextColor(Color.parseColor("#B5000000"));
                    notificationAfter.setTextColor(Color.parseColor("#B5000000"));

                    checkNotificationsOn();
                }
                else
                {
                    notificationAfterText.setTextColor(Color.LTGRAY);
                    notificationAfter.setTextColor(Color.LTGRAY);
                }

                showNotification = isChecked;
                notificationAfter.setClickable(isChecked);
                sharedPreferences.edit().putBoolean("notificationStatus", isChecked).apply();
            }
        });

        if(webservice.loadUsername().equals("Guest"))
        {
            standardIssueText.setTextColor(Color.LTGRAY);
            switchStandardIssue.setEnabled(false);
            switchStandardIssue.setClickable(false);
            switchStandardIssue.setChecked(false);
        }
        else
        {
            final long startDate = sharedPreferences.getLong("startDateInMillis", 0);

            standardIssueText.setTextColor(Color.parseColor("#B5000000"));

            switchStandardIssue.setClickable(true);

            if(startDate == 0 && !webservice.loadStandardIssue().equals("Select issue"))
            {
                standardIssueName.setTextColor(Color.parseColor("#B5000000"));
                mbIssueDropdown.getBackground().setAlpha(255);
                mbIssueDropdown.setEnabled(true);
                mbIssueDropdown.setClickable(true);
                switchStandardIssue.setChecked(true);
            }

            mbIssueDropdown.setText(webservice.loadStandardIssue());

            switchStandardIssue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    if(startDate != 0)
                    {
                        Toast.makeText(getApplicationContext(), "To change settings for standard issue stop running worktime.", Toast.LENGTH_LONG).show();
                        switchStandardIssue.setChecked(!switchStandardIssue.isChecked());
                    }
                    else
                    {
                        if(isChecked)
                        {
                            standardIssueName.setTextColor(Color.parseColor("#B5000000"));
                            mbIssueDropdown.getBackground().setAlpha(255);
                        }
                        else
                        {
                            standardIssueName.setTextColor(Color.LTGRAY);
                            mbIssueDropdown.getBackground().setAlpha(69);
                            mbIssueDropdown.setText("Select issue");
                            webservice.storeStandardIssue(mbIssueDropdown.getText().toString());
                        }

                        mbIssueDropdown.setEnabled(isChecked);
                        mbIssueDropdown.setClickable(isChecked);
                        enableStandardIssue = isChecked;
                        sharedPreferences.edit().putBoolean("enableStandardProject", isChecked).apply();
                    }
                }
            });
        }

        mbIssueDropdown.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showPopupMenu(view);
            }
        });
    }

    /**
     * This method loads the settings the user saved in the previous session of the application
     * for the settings menu.
     *
     * @param sharedPreferences - Preferences stored on the user device
     */
    public void loadSharedPreferences(SharedPreferences sharedPreferences)
    {
        switchNotifications.setChecked(false);
        switchStandardIssue.setChecked(false);
        mbIssueDropdown.getBackground().setAlpha(69);
        mbIssueDropdown.setEnabled(false);

        if(sharedPreferences.getBoolean("notificationStatus", showNotification))
        {
            notificationAfter.setClickable(true);
            switchNotifications.setChecked(true);
            checkNotificationsOn();
            notificationAfter.setText(sharedPreferences.getString("notificationAfterStatus", notificationTime));
            notificationAfterText.setTextColor(Color.parseColor("#B5000000"));
            notificationAfter.setTextColor(Color.parseColor("#B5000000"));
        }
        else if(sharedPreferences.getBoolean("enableStandardProject", enableStandardIssue) && !webservice.loadUsername().equals("Guest"))
        {
            mbIssueDropdown.setClickable(true);
            switchStandardIssue.setChecked(true);
            standardIssueName.setTextColor(Color.parseColor("#B5000000"));
            mbIssueDropdown.getBackground().setAlpha(255);
            mbIssueDropdown.setEnabled(true);
            mbIssueDropdown.setText(webservice.loadStandardIssue());
        }

        switchLogin.setChecked(webservice.loadAutoLogin());
    }

    /**
     * This method sets an click listener event on the notification setting, as long as it is
     * checked as on. The click listener opens a date picker dialog for the user
     * to select a specific time, when the notification shall be received.
     */
    public void checkNotificationsOn()
    {
        notificationAfter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final Calendar c = Calendar.getInstance();

                final TimePickerDialog tpdTime = new TimePickerDialog(SettingsActivity.this, new TimePickerDialog.OnTimeSetListener()
                {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int min)
                    {
                        notificationTime = String.format("%02d:%02d", hourOfDay, min);
                        notificationAfter.setText(notificationTime);
                        sharedPreferences.edit().putString("notificationAfterStatus", notificationTime).apply();
                    }
                },c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),true);

                tpdTime.show();
            }
        });
    }

    /**
     * This method opens a popup menu of all the issues available for the user.
     * Selecting one will result in setting it as the default issue for tracking time.
     */
    public void showPopupMenu(View view)
    {
        final PopupMenu popup = new PopupMenu(this, view);
        final List<SubMenu> submenuList = new LinkedList<>();
        popup.inflate(R.menu.project_issue_menu);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                List<Issue> issueList = ListingProvider.getInstance().getIssuesOfProject(item.getTitle().toString());
                SubMenu currentSubMenu = null;

                if(!item.hasSubMenu())
                {
                    mbIssueDropdown.setText(item.getTitle());
                    webservice.storeStandardIssue(mbIssueDropdown.getText().toString());
                }
                else
                {
                    for(SubMenu subMenu : submenuList)
                    {
                        if(subMenu.getItem().getTitle().equals(item.getTitle()))
                        {
                            subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            currentSubMenu = subMenu;
                        }
                    }

                    for (Issue issue : issueList)
                    {
                        currentSubMenu.add(issue.getKey());
                    }
                }

                return true;
            }
        });

        List<Project> projectList = ListingProvider.getInstance().getProjects();

        for (Project p : projectList)
        {
            SubMenu helpMenu = popup.getMenu().addSubMenu(p.getProjectKey());

            helpMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            submenuList.add(helpMenu);
        }

        popup.show();
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(this, StartStopActivity.class));
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            startActivity(new Intent(SettingsActivity.this, StartStopActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_worktime)
        {
            startActivity(new Intent(SettingsActivity.this, WorktimeActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_projects)
        {
            startActivity(new Intent(SettingsActivity.this, ProjectActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_statistics)
        {
            startActivity(new Intent(SettingsActivity.this, StatisticActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_export)
        {
            startActivity(new Intent(SettingsActivity.this, ExportActivity.class));
            overridePendingTransition(R.layout.animation_fade_into, R.layout.animation_fade_out);
            finish();
        }
        else if (id == R.id.nav_settings)
        {
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success) {}
}
