package com.ssi.zeiterfassungsapp.layouts;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.button.MaterialButton;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.util.NetworkUtil;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Matthias Pöschl
 * @since 2019/07/23
 * Class that handles the eveything that is used for the editDialog
 */
public class EditDialog implements AsyncDelegate
{
    private WorktimelistAdapter adapter;
    private Date changedFrom;
    private Date changedTo;
    private String changedProjectKey;
    private String changedDescription;
    private Webservice webservice;
    private Dialog editDialog;
    private Context aContext;
    private boolean OK;

    public EditDialog(Context applicationContext)
    {
        OK = false;
        this.adapter = WorktimelistAdapter.getInstance();
        this.webservice = Webservice.getInstance(applicationContext);
        webservice.setListener(this);
        aContext = applicationContext;
    }

    public View showEditDialog(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View editDialogView = inflater.inflate(R.layout.edit_worktime_dialog, null);
        builder.setView(editDialogView);

        editDialog = builder.create();

        Toolbar tbDialog = editDialogView.findViewById(R.id.tb_top);
        tbDialog.setTitle("Edit Worktime");

        WindowManager.LayoutParams params = editDialog.getWindow().getAttributes();
        params.y = 128;
        editDialog.getWindow().setAttributes(params);
        editDialog.getWindow().setGravity(Gravity.TOP);

        editDialog.show();

        final EditText etDescription = editDialogView.findViewById(R.id.et_description);

        if(etDescription.toString().equals(""))
        {
            etDescription.setText("Default Bookingtext: Please update Bookingtext");
        }

        return editDialogView;
    }

    public void initializeEditDialog(final Context context, final Context applicationContext, final int position, final View editDialogView)
    {
        changedFrom = adapter.getItem(position).getStartDate();
        changedTo = adapter.getItem(position).getEndDate();
        Date newFrom = new Date();
        Date newTo = new Date();

        changedDescription = adapter.getItem(position).getBookingtext();
        changedProjectKey = adapter.getItem(position).getIssueKey();

        final EditText etStarttime = editDialogView.findViewById(R.id.et_startDate);
        final EditText etEndtime = editDialogView.findViewById(R.id.et_endDate);
        final EditText etDescription = editDialogView.findViewById(R.id.et_description);
        final MaterialButton mbDropdown = editDialogView.findViewById(R.id.btn_dropdown);

        etDescription.setText(changedDescription);

        if(!webservice.loadUsername().equals("Guest"))
        {
            mbDropdown.getBackground().setAlpha(255);

            mbDropdown.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    showPopupMenu(context, view, mbDropdown);
                }
            });
        }
        else
        {
            mbDropdown.getBackground().setAlpha(69);
        }


        final MaterialButton mtbConfirm = editDialogView.findViewById(R.id.mb_confirm);

        mtbConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date newFrom = null;
                Date newTo = null;
                changedFrom = adapter.getItem(position).getStartDate();
                changedTo = adapter.getItem(position).getEndDate();
                changedDescription = adapter.getItem(position).getBookingtext();
                changedProjectKey = adapter.getItem(position).getIssueKey();

                if(changedFrom.getTime() > changedTo.getTime())
                {
                    Toast.makeText(applicationContext,"The starttime can not be set later than the endtime", Toast.LENGTH_LONG).show();
                }
                else  if((TimeUnit.MILLISECONDS.toHours(changedTo.getTime() - changedFrom.getTime())) > 12)
                {
                    Toast.makeText(applicationContext, "It is not allowed to work more than 12 hours", Toast.LENGTH_LONG).show();
                }
                else if(mbDropdown.getText().equals("Select a project") && !webservice.loadUsername().equals("Guest"))
                {
                    Toast.makeText(applicationContext, "Please select a project first!", Toast.LENGTH_LONG).show();
                }
                else
                {
                    MaterialButton mbDropdown = editDialogView.findViewById(R.id.btn_dropdown);
                    Worktime oldWorktime = adapter.getItem(position);
                    EditText etDescription = editDialogView.findViewById(R.id.et_description);

                    try {
                        newFrom = dateFormat.parse(etStarttime.getText().toString());
                        newTo = dateFormat.parse(etEndtime.getText().toString());
                    } catch (ParseException e) {
                        Log.e("EditDialog", e.toString());
                    }
                    //FIXME: Start Date and End Date from Dialog should be used instead of old Worktime (it does not change the time)
                    adapter.getItem(position).setBookingtext(etDescription.getText().toString().equals("") ? "Default Bookingtext: Please update Bookingtext" : etDescription.getText().toString());
                    adapter.getItem(position).setIssueKey(mbDropdown.getText().toString());
                    Worktime newWorktime = new Worktime(adapter.getItem(position).getIssueKey(), adapter.getItem(position).getStartDate(), adapter.getItem(position).getEndDate(), adapter.getItem(position).getBookingtext());

                    adapter.notifyDataSetChanged();

                    if(oldWorktime.isSynced() && NetworkUtil.isConnected(context) && !webservice.loadUsername().equals("Guest"))
                    {
                        webservice.changeSyncedWorktime(oldWorktime, newWorktime);
                    }
                    else
                    {
                        webservice.editWorktime(oldWorktime, newWorktime);
                    }
                    //webservice.editWorktime(oldWorktime, newWorktime);
                    adapter.notifyDataSetChanged();
                    OK = true;
                    editDialog.dismiss();
                }
            }
        });

        final MaterialButton mbCancel = editDialogView.findViewById(R.id.mb_cancel);

        mbCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OK = false;
                editDialog.dismiss();
            }
        });

        final Calendar c = Calendar.getInstance();

        c.setTime(changedFrom);

        final TimePickerDialog tpdStarttime = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener()
        {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes)
            {
                etStarttime.setText(String.format("%s, %d:%02d", etStarttime.getText().toString(), hourOfDay, minutes));
                adapter.getItem(position).getStartDate().setHours(hourOfDay);
                adapter.getItem(position).getStartDate().setMinutes(minutes);
            }
        }, adapter.getItem(position).getStartDate().getHours(), adapter.getItem(position).getStartDate().getMinutes(), true);

        final DatePickerDialog dpdStarttime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etStarttime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                adapter.getItem(position).getStartDate().setYear(year-1900);
                adapter.getItem(position).getStartDate().setMonth(month);
                adapter.getItem(position).getStartDate().setDate(day);

                //changedFrom = calendar.getTime();
                tpdStarttime.show();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));



        final TimePickerDialog tpdEndtime = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener()
        {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes)
            {
                etEndtime.setText(String.format("%s, %d:%02d", etEndtime.getText().toString(), hourOfDay, minutes));
                adapter.getItem(position).getEndDate().setHours(hourOfDay);
                adapter.getItem(position).getEndDate().setMinutes(minutes);
            }
        }, adapter.getItem(position).getEndDate().getHours(), adapter.getItem(position).getEndDate().getMinutes(),true);

        c.setTime(changedTo);

        final DatePickerDialog dpdEndtime = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day)
            {
                Calendar calendar = Calendar.getInstance();
                etEndtime.setText(String.format("%02d.%02d.%d", day, month + 1, year));
                calendar.set(year, month, day);
                adapter.getItem(position).getEndDate().setYear(year-1900); //Because Gregorianian Cal starts with 1900 but returns exact number of year
                adapter.getItem(position).getEndDate().setMonth(month);
                adapter.getItem(position).getEndDate().setDate(day);
                //changedTo = calendar.getTime();
                tpdEndtime.show();
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        newTo = changedTo;

        etStarttime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdStarttime.show();
            }
        });

        etEndtime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dpdEndtime.show();
            }
        });

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(adapter.getItem(position).getStartDate());
        changedFrom = calendar.getTime();

        etStarttime.setText(String.format("%02d.%02d.%d, %d:%02d"
                , calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.MONTH) + 1
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)));

        //changedFrom.setYear(Calendar.YEAR);
        //changedFrom.setMonth(Calendar.MONTH);
        //changedFrom.setDate(Calendar.DATE);
        //changedFrom.setHours(Calendar.HOUR_OF_DAY);
        //changedFrom.setMinutes(Calendar.MINUTE);
        calendar.setTime(adapter.getItem(position).getEndDate());
        changedTo = calendar.getTime();

        etEndtime.setText(String.format("%02d.%02d.%d, %d:%02d"
                , calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.MONTH) + 1
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)));

        //changedTo.setDate(Calendar.DATE);
        //changedTo.setHours(Calendar.HOUR_OF_DAY);
        //changedTo.setMinutes(Calendar.MINUTE);
        mbDropdown.setText(adapter.getItem(position).getIssueKey());
    }

    public void showPopupMenu(Context context,View view,final MaterialButton mbDropdown)
    {
        final PopupMenu popup = new PopupMenu(context, view);
        final List<SubMenu> submenuList = new LinkedList<>();
        // Inflate the menu from xml
        popup.inflate(R.menu.project_issue_menu);
        // Setup menu item selection

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                List<Issue> issueList = ListingProvider.getInstance().getIssuesOfProject(item.getTitle().toString());
                SubMenu currentSubMenu = null;

                if(!item.hasSubMenu())
                {
                    mbDropdown.setText(item.getTitle());
                }
                else
                {
                    for(SubMenu subMenu : submenuList)
                    {
                        if(subMenu.getItem().getTitle().equals(item.getTitle()))
                        {
                            subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            currentSubMenu = subMenu;
                        }
                    }
                    for (Issue issue : issueList)
                    {
                        currentSubMenu.add(issue.getKey());
                    }
                }

                return true;
            }
        });

        List<Project> projectList = ListingProvider.getInstance().getProjects();

        for (Project p : projectList)
        {
            SubMenu helpMenu = popup.getMenu().addSubMenu(p.getProjectKey());
            helpMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            submenuList.add(helpMenu);
        }

        popup.show();
    }

    public boolean isOK()
    {
        return OK;
    }

    @Override
    public void loginComplete(boolean success) {}

    @Override
    public void validationComplete(boolean success) {}

    @Override
    public void issueLoadingComplete(boolean success) {}

    @Override
    public void worktimeLoadingComplete(boolean success)
    {
        adapter.setUnfilteredWorktimeList(ListingProvider.getInstance().getWorktimes());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void worktimeDayLoadingComplete(boolean success) {}

    @Override
    public void authenticationJiraComplete(boolean success) {}

    @Override
    public void deauthenticationJiraComplete(boolean success) {}

    @Override
    public void notificationSent(boolean success) {}

    @Override
    public void updateWorktimeList(List<Worktime> worktimes, int usage) {}

    @Override
    public void deleteWorktimesComplete(boolean success) {}

    @Override
    public void stopWorktimeComplete(boolean success) {}

    @Override
    public void postingWorktimeCompleted(boolean success)
    {
        webservice.listAllWorktimes();
    }
}
