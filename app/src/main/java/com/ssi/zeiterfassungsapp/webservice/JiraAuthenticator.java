package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.JiraInterceptors.Methods;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-17
 */
public class JiraAuthenticator implements Callback
{
    private static final String TAG = "JiraAuthenticator";
    private Context mContext;
    private AsyncDelegate delegate;

    public JiraAuthenticator(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        if(response.code() != 401 && response.code() != 403) //errors with authentication
        {
            if(Methods.isCookieSet(mContext))
            {
                delegate.authenticationJiraComplete(true);
            }
        }
        else //error message received
        {
            Log.e(TAG,response.code()+"");
            delegate.authenticationJiraComplete(false);
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        delegate.authenticationJiraComplete(false);
    }
}
