package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.beans.Responses.WorktimeResponse;
import com.ssi.zeiterfassungsapp.beans.WorktimeRetrieved;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-15
 */
public class TimestampChanger implements Callback
{
    private static final String TAG = "TimestampChanger";
    private Context mContext;
    private Worktime oldWorktime;
    private Worktime newWorktime;

    public TimestampChanger(Context mContext)
    {
        this.mContext = mContext;
    }

    public void setOldWorktime(Worktime oldWorktime)
    {
        this.oldWorktime = oldWorktime;
    }

    public void setNewWorktime(Worktime newWorktime)
    {
        this.newWorktime = newWorktime;
    }

    /**
     * Method replaces the old worktime (set via setter) with the new worktime (set via setter) in the provided List which is then returned
     * @param worktimesOnline List in which the old worktime should be replaced with the new one
     * @return updated List (new worktime instead of old one) or null if the oldWorktime wasn't found in the list
     */
    public ArrayList<Worktime> getChangedWorktimeList(ArrayList<Worktime> worktimesOnline)
    {
        ArrayList<Worktime> updatedWorktimesOfDay = null;

        try
        {
            if (worktimesOnline.remove(oldWorktime))
            {
                updatedWorktimesOfDay = new ArrayList<>(worktimesOnline);
                updatedWorktimesOfDay.add(newWorktime);
            }
        }
        catch(NullPointerException npe)
        {
            Log.e(TAG,npe.getMessage());
        }
        finally
        {
            return updatedWorktimesOfDay;
        }
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        Type type = new TypeToken<WorktimeResponse>(){}.getType(); //storing the type of response
        WorktimeResponse worktimeResponse = null; //the response object to hold the response
        Gson gson = new Gson();

        if(response.code() == 200) //If the Code is equal to 200 it is a valid response
        {
            worktimeResponse = (WorktimeResponse) response.body(); //transforms the response into a GetResponse
            ArrayList<Worktime> worktimesOnline = new ArrayList<>();

            for(WorktimeRetrieved worktime : worktimeResponse.getResult())
            {
                worktimesOnline.add(worktime.transformToWorktime());
            }

            try
            {
                Webservice.getInstance().postWorktime(new LinkedList<Worktime>(getChangedWorktimeList(worktimesOnline)));
            }
            catch(NullPointerException npe)
            {
                Log.e(TAG,"No changes of synced worktimes ");
            }

            Log.e(TAG,"here we are");
        }
        else
        {
            try
            {
                worktimeResponse = gson.fromJson(response.errorBody().string(), type); //transforms the response
                Toast.makeText(mContext,"Editing and synchronizing worktime failed - try again later!",Toast.LENGTH_LONG); //notify the user of failed completion
            }
            catch(IOException e)
            {
                Log.e(TAG,"Error: " + e.getMessage());
            }
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        Log.e(TAG,t.getMessage());
    }
}
