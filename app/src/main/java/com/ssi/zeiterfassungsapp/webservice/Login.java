package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.LoginResponse;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.EncryptionManager;
import java.io.IOException;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**Class which logs user in and provides Token for further requests
 *
 * @author Schwarzkogler
 * @since 2019/07/08
 */
/**This class simply provides a Login for a User
 *
 * @author Schwarzkogler
 * @since 2019/07/08
 */
public class Login implements Callback
{
    public static final String TAG = "Login"; //TAG is useful for Debugging
    private Context mContext;
    private EncryptionManager encryptionManager;
    private Gson gson;
    private AsyncDelegate delegate;

    public Login(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
        gson = new Gson();
        encryptionManager = new EncryptionManager(mContext);
    }

    /**This method simply stores the credentials in order to use the token for further actions during app usage
     *
     * @param loginResponse Login Object with Token
     */
    public void storeToken(LoginResponse loginResponse)
    {
        try
        {
            encryptionManager.encryptToken(loginResponse.getToken()); //Encrypts the Token with AES
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }
    }

    /**This method simply loads the Token from SharedPreferences (if existing)
     *
     * @return decrypted Token
     */
    public String loadToken()
    {
        try
        {
            return encryptionManager.decryptToken(); //Decrypts the Token and returns the value
        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }

    public void storeUser(String username)
    {
        encryptionManager.storeUsernameInPreferences(username);
    }

    public void storeAutoLogin(boolean autoLogin)
    {
        encryptionManager.storeAutoLoginInPreferences(autoLogin);
    }

    public boolean loadAutoLogin()
    {
        return encryptionManager.loadAutoLoginFromPreferences();
    }

    public String loadUsername()
    {
        return encryptionManager.loadUsernameFromPreferences();
    }

    public void storeRememberMe(boolean rememberMe)
    {
        encryptionManager.storeRememberMeInPreferences(rememberMe);
    }

    public boolean loadRememberMe()
    {
        return encryptionManager.loadRememberMeFromPreferences();
    }

    public void storeProjectColor(String projectColorsString)
    {
        encryptionManager.storeProjectColorPreferences(projectColorsString);
    }

    public String loadProjectColor()
    {
        return encryptionManager.loadProjectColorPreferences();
    }

    public void storeStandardIssue(String standardIssueString)
    {
        encryptionManager.storeStandardIssuePreferences(standardIssueString);
    }

    public String loadStandardIssue()
    {
        return encryptionManager.loadStandardIssuePreferences();
    }

    public void logout()
    {
        encryptionManager.removeTokenFromPreferences();
        encryptionManager.removeUserFromPreferences();
        encryptionManager.removeKeysFromPreferences("com.ssi.zeiterfassungsapp.token.iv", "com.ssi.zeiterfassungsapp.token.key");

        Toast.makeText(mContext, "Logout successful", Toast.LENGTH_LONG).show();
    }

    /**This method simply evaluates the response of the Callable
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call call, Response response)
    {
        //TODO: Formatting and beautifying of Code Style
        Type type = new TypeToken<LoginResponse>(){}.getType(); //Creates instance of the Type (LoginResponse)
        LoginResponse loginResponse = null;

        if(response.code() < 400) //If response code is over 400 -> Error occured
        {
            loginResponse = (LoginResponse) response.body(); //Converts response to LoginResponse
            storeToken(loginResponse); //Stores Token encrypted

            Toast.makeText(mContext, "Login successful", Toast.LENGTH_LONG).show(); //Displays User information

            delegate.loginComplete(true); //Notifies MainActivity that the thread completed the task successfully without errors
        }
        else
        {
            try
            {
                loginResponse = gson.fromJson(response.errorBody().string(), type); //Converts the response to LoginResponse

                Toast.makeText(mContext, "Login failed - Please try again", Toast.LENGTH_LONG).show();

                delegate.loginComplete(false);
            }
            catch (IOException e)
            {
                Log.e(TAG, "Error: " + e.getMessage());
            }
        }

        loginResponse.setCode(response.code());
    }

    /**This method simply evaluates the Error if an error occurs
     *
     * @param call Call variable
     * @param t Throwable variable
     */
    @Override
    public void onFailure(Call call, Throwable t)
    {
        Log.e(TAG, "Error: " + t.getMessage());
    }
}
