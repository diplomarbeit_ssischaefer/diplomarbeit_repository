package com.ssi.zeiterfassungsapp.webservice;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.WorktimeResponse;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import java.io.IOException;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-15
 */
public class WorktimeLister implements Callback
{
    private static final String TAG = "WorktimeLister";
    private Context mContext;
    private AsyncDelegate delegate;

    public WorktimeLister(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        Type type = new TypeToken<WorktimeResponse>(){}.getType(); //storing the type of response
        WorktimeResponse worktimeResponse = null; //the response object to hold the response
        Gson gson = new Gson();

        if(response.code() == 200) //If the Code is equal to 200 it is a valid response
        {
            worktimeResponse = (WorktimeResponse) response.body(); //transforms the response into a GetResponse
            ListingProvider.getInstance().setWorktimes(worktimeResponse.getResult()); //calls the method to fill the list with Worktimes

            Toast.makeText(mContext,"Gathering worktimes was successful!",Toast.LENGTH_LONG); //notify user of successful completion

            delegate.worktimeLoadingComplete(true); //notify Activity of successful completion
        }
        else
        {
            try
            {
                worktimeResponse = gson.fromJson(response.errorBody().string(), type); //transforms the response

                Toast.makeText(mContext,"Gathering worktimes failed - please try again later!",Toast.LENGTH_LONG); //notify the user of failed completion
            }
            catch(IOException e)
            {
                Log.e(TAG,"Error: " + e.getMessage());
            }
            finally
            {
                delegate.worktimeLoadingComplete(false); //notify Activity of failed completion
            }
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        Toast.makeText(mContext,"Gathering worktimes failed - please try again later!",Toast.LENGTH_LONG); //notify user of failed completion

        delegate.worktimeLoadingComplete(false); //notify Activity of failed completion
    }
}
