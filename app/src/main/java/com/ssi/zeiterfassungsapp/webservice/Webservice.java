package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.ShareCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.ssi.zeiterfassungsapp.adapters.WorktimelistAdapter;
import com.ssi.zeiterfassungsapp.beans.Notification;
import com.ssi.zeiterfassungsapp.beans.NotificationModel;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.database.DatabaseClient;
import com.ssi.zeiterfassungsapp.database.DatabaseReader;
import com.ssi.zeiterfassungsapp.database.DatabaseRemover;
import com.ssi.zeiterfassungsapp.database.DatabaseSaver;
import com.ssi.zeiterfassungsapp.database.DatabaseUpdater;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.JiraInterceptors.AddCookiesInterceptor;
import com.ssi.zeiterfassungsapp.util.JiraInterceptors.BasicAuthInterceptor;
import com.ssi.zeiterfassungsapp.util.JiraInterceptors.ReceivedCookiesInterceptor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**This class is used to serve as a Single Point of Connection for all JIRA operations
 *
 * @author Klaminger & Schwarzkogler
 * @since 2019/07/08
 */
public class Webservice
{
    private static final String TAG = "Webservice";
    private static final SimpleDateFormat dateformatRequestWorktimes = new SimpleDateFormat("yyyyMMdd");
    private boolean guest = false;
    //Instance Variables of Request Classes
    private Login loginTask;
    private TokenValidator tokenValidatorTask;
    private WorktimeImporter worktimeImporter;
    private IssueLoader issueLoadingTask;
    private WorktimeLister worktimeListingTask;
    private WorktimeDayLister worktimeDayListingTask;
    private TimestampChanger editWorktimeTask;
    private WorktimeDeleter deleteWorktimesTask;
    private JiraAuthenticator authenticationJiraTask;
    private JiraDeauthenticator deauthenticationJiraTask;
    private Notifier notifierTask;
    //Instance Variables of Booleans which indicates a State
    public static boolean userIsConnected = false;
    public static boolean tokenIsValid = false;
    //Instance Variables of other Objects
    private AsyncDelegate listener;
    private Context mContext;
    private Gson gson;
    //Instance Variables for RetroFit
    public static final String BASE_URL_WORKTIME = "https://worktime.wamas.com/api/";
    public static final String BASE_URL_FIREBASE = "https://fcm.googleapis.com/fcm/";
    public static final String BASE_URL_JIRAPROJECTS = "https://jira.ssi-schaefer.com/rest/";
    private Retrofit retrofitForWorktime;
    private Retrofit retrofitForFirebase;
    private Retrofit retrofitForJiraProjects;
    private JiraApiService worktimeService;
    private JiraApiService firebaseService;
    private JiraApiService projectsService;
    //Instance Variables for Database
    private DatabaseSaver databaseSaver;
    private DatabaseReader databaseReader;
    private DatabaseRemover databaseRemover;
    private DatabaseUpdater databaseUpdater;
    private static Webservice instance = null;
    private WorktimelistAdapter adapter;

    public static Webservice getInstance(Context mContext)
    {
        if(instance == null)
        {
            instance = new Webservice(mContext);
        }
        return instance;
    }

    public static Webservice getInstance()
    {
        if(instance == null)
        {
            instance = new Webservice();
        }
        return instance;
    }

    private Webservice(Context mContext)
    {
        this.mContext = mContext;
    }

    private Webservice()
    {

    }

    /**This method simply logs a user in the System in. in order to notify the Application
     * that the Login failed/succeeded, the loginTask sends a parameter to the MainActivity
     * with AsyncDelegate and in MainActivity this information will be used to proceed
     *
     * @param username Username of the User
     * @param password Password of the User
     */
    public void login(String username,  String password, boolean rememberMe)
    {
        //adapter = WorktimelistAdapter.getInstance();
        Call call = worktimeService.loginWithUser(username, password); //Creates a call instance of loginWithUser()
        call.enqueue(loginTask); //Starts a Background task with Retrofit
        storeUsername(username);
        loginTask.storeRememberMe(rememberMe);
        authenticateJira(username, password);
    }

    public String loadUsername()
    {
        return loginTask.loadUsername();
    }

    public void storeUsername(String username)
    {
        loginTask.storeUser(username);
    }

    public boolean loadRememberMe()
    {
        return loginTask.loadRememberMe();
    }

    public void logout()
    {
        loginTask.logout();
        deauthenticateJira();
        //adapter.setUnfilteredWorktimeList(new LinkedList<Worktime>());
    }

    /**This method simply loads an existing Token from SharedPreferences
     *
     * @return String of the Token
     */
    public String loadToken()
    {
        if(loginTask.loadToken() != null) //Checks if token exists
        {
            return loginTask.loadToken(); //returns token
        }
        else
        {
            return null;//throw(new RuntimeException("No Token found in SharedPreferences: User never logged in - Please make initial Login")); //Throws Exception with more information
        }
    }

    /**This method simply checks the validity of the Token (if Stored)
     *
     */
    public void checkToken()
    {
        Call call = worktimeService.checkToken(loginTask.loadToken()); //Creates a call instance of checkToken()
        call.enqueue(tokenValidatorTask); //Starts background task
    }

    /**
     * This method simply imports the Worktime into the Webservice using HTTPS POST Requests
     */
    public void postWorktime(LinkedList<Worktime> worktimes)
    {
        try
        {
            Call call = worktimeService.postWorktime(loadToken(), loadUsername(), worktimeImporter.getPreparedDataForRequest(worktimes)); //creates the Request with given parameters (Currently running with only one worktime
            call.enqueue(worktimeImporter); //Starts the asynchroun taks in the background
        }
        catch(Exception e)
        {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    /**This method simply inserts a new Worktime if no connection found
     *
     * @param worktime Worktime Object
     */
    public void saveWorktimeInDatabase(Worktime worktime, int usage)
    {
        databaseSaver = new DatabaseSaver(mContext, listener, usage);
        databaseSaver.execute(worktime);
    }

    /**This method simply closes the connection of the Database
     *
     */
    public void closeDatabase()
    {
        DatabaseClient.getInstance(mContext).closeDatabase();
    }

    /**This method simply loads all Worktime Objects form the Database
     *
     */
    public void loadWorktimesFromDatabase(int usage)
    {
        if(usage != 0 && usage != 1 && usage != 3)
        {
            Log.e(TAG, "Usage for DatabaseReader invalid");
        }
        else
        {
            databaseReader = new DatabaseReader(mContext, listener, usage);
            databaseReader.execute();
        }
    }

    /**This method simply deletes all entities in the Database
     *
     */
    public void deleteAllWorktimesFromDatabase()
    {
        databaseRemover = new DatabaseRemover(mContext, listener);
        databaseRemover.execute();
    }

    /**This method simply deletes a Worktime from the database
     *
     * @param worktime
     */
    public void deleteWorktime(Worktime worktime)
    {
        databaseRemover = new DatabaseRemover(mContext, listener);
        databaseRemover.execute(worktime);
    }

    /**This method simply edits a already existing Worktime in order to Stop the Worktime
     *
     * @param worktime Object of worktime with enddate
     */
    public void stopWorktime(Worktime worktime)
    {
        databaseUpdater = new DatabaseUpdater(mContext, listener);
        databaseUpdater.execute(worktime);
    }

    /**This method simply replaces the old Worktime in the Database with the new Worktime
     *
     * @param oldWorktime Worktime Object of old (edited Worktime)
     * @param newWorktike Worktime Object with new Values
     */
    public void editWorktime(Worktime oldWorktime, Worktime newWorktike)
    {
        databaseUpdater = new DatabaseUpdater(mContext, listener);
        databaseUpdater.execute(oldWorktime, newWorktike);
    }

    /**
     * This method loads the all Worktimes into a List in ListingProvider
     * TODO check token (in Activity)
     */
    public void listAllWorktimes()
    {
        HashMap<String,String> params = new HashMap<>();

        params.put("tokenid",loadToken());

        Call call = worktimeService.requestWorktimes(params);
        call.enqueue(worktimeListingTask);
    }

    /**
     * This method loads all Worktimes between startdate and enddate into a List in ListingProvider
     * @param startdate Date, first day from which to retrieve Worktimes
     * @param enddate Date, last day from which to retrieve Worktimes
     */
    public void listWorktimesOfDay(Date startdate, Date enddate)
    {
        HashMap<String,String> params = new HashMap<>();

        params.put("tokenid",loadToken());
        params.put("datefrom",dateformatRequestWorktimes.format(startdate));
        params.put("dateto",dateformatRequestWorktimes.format(enddate));

        Call call = worktimeService.requestWorktimes(params);
        call.enqueue(worktimeDayListingTask);
    }

    /**
     * This method removes all Worktimes of a Day
     * @param date Date, specifying the day from which to remove all worktimes
     */
    public void removeWorktimesOfDay(Date date)
    {
        SimpleDateFormat dateformatRemoveWorktime = new SimpleDateFormat("dd.MM.yyyy");

        Call call = worktimeService.removeWorktimesOfDay(loadToken(),dateformatRemoveWorktime.format(date),loadUsername());
        call.enqueue(deleteWorktimesTask);
    }

    /**
     * This method allows for synced worktimes (from API) to be changed/edited
     * @param oldWorktime worktime Object before change (gets replaced by newWorktime)
     * @param newWorktime worktime Object after change (replaces oldWorktime)
     */
    public void changeSyncedWorktime(Worktime oldWorktime, Worktime newWorktime)
    {
        editWorktimeTask.setOldWorktime(oldWorktime);
        editWorktimeTask.setNewWorktime(newWorktime);

        HashMap<String,String> params = new HashMap<>();

        params.put("tokenid",loadToken());
        params.put("datefrom",dateformatRequestWorktimes.format(oldWorktime.getStartDate()));
        params.put("dateto",dateformatRequestWorktimes.format(newWorktime.getStartDate()));

        Call call = worktimeService.requestWorktimes(params);
        call.enqueue(editWorktimeTask);
    }

    /**
     * Authenticate with (Login) Jira -> check with AsyncDelegate.authenticateJiraComplete() if the process is finished!
     */
    public void authenticateJira(String username, String password)
    {
        //implementing OkHttpClient for Cookie-relevant Session with Jira REST API
        updateJiraSessionClients(getOkHttpClient(new ReceivedCookiesInterceptor(mContext), new BasicAuthInterceptor(username.trim(),password.trim())));

        Call call = projectsService.authenticateJira(username.trim(),password.trim());
        call.enqueue(authenticationJiraTask);
    }

    /**
     * Load issues (and corresponding projects) from Jira
     * only issues assigned to the current user
     */
    public void loadIssues()
    {
        updateJiraSessionClients(getOkHttpClient(new AddCookiesInterceptor(mContext)));

        Call call = projectsService.requestIssuesJira();
        call.enqueue(issueLoadingTask);
    }

    /**
     * Deauthenticate with (Logout) Jira -> check with AsyncDelegate.deauthenticateJiraComplete() if the process is finished!
     */
    public void deauthenticateJira()
    {
        updateJiraSessionClients(getOkHttpClient(new AddCookiesInterceptor(mContext)));

        Call call = projectsService.logoutJira();
        call.enqueue(deauthenticationJiraTask);
    }

    public void sendNotification(String title, String body)
    {
        NotificationModel notificationModel = new NotificationModel(body,title);
        final Notification notification = new Notification();

        notification.setNotificationModel(notificationModel);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task)
                    {
                        if (!task.isSuccessful())
                        {
                            return;
                        }
                        // Get new Instance ID token
                        notification.setToken(task.getResult().getToken());
                    }
                });

        Call call = firebaseService.createNotification(notification);
        call.enqueue(notifierTask);
    }

    public void storeAutoLogin(boolean autoLogin)
    {
        loginTask.storeAutoLogin(autoLogin);
    }

    public boolean loadAutoLogin()
    {
        return loginTask.loadAutoLogin();
    }

    public void storeProjectColor(String projectColorsString)
    {
        loginTask.storeProjectColor(projectColorsString);
    }

    public String loadProjectColor()
    {
        return loginTask.loadProjectColor();
    }

    public void storeStandardIssue(String standardIssueString)
    {
        loginTask.storeStandardIssue(standardIssueString);
    }

    public String loadStandardIssue()
    {
        return loginTask.loadStandardIssue();
    }

    public void syncDatabaseWithWebservice(List<Worktime> unsyncedAndSyncedWorktimes)
    {
        if(!loadUsername().equals("Guest"))
        {
            Log.e(TAG, "SYNC STARTED");

            postWorktime((LinkedList<Worktime>) unsyncedAndSyncedWorktimes);
            deleteAllWorktimesFromDatabase();
        }
        else
        {
            Toast.makeText(mContext, "Sync with Webservice not available as Guest", Toast.LENGTH_LONG).show();
        }
    }

    /**This method simply creates an instance of a RetroFitClient in order to use retroFit
     *
     * @return A retorfit client instance
     */
    public Retrofit getRetroFitClient(String url)
    {
        return new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build(); //Creates a retrofit instance
    }

    /**
     * get the OkHttpClient as requested with the correct interceptors
     * @param interceptors
     * @return OkHttpClient built
     */
    private OkHttpClient getOkHttpClient(Interceptor... interceptors)
    {
        //implementing OkHttpClient for Cookie-relevant Session with Jira REST API
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60 * 5, TimeUnit.SECONDS)
                .readTimeout(60 * 5, TimeUnit.SECONDS)
                .writeTimeout(60 * 5, TimeUnit.SECONDS);

        for (Interceptor interceptor : interceptors)
        {
            okHttpClient.interceptors().add(interceptor);
        }

        return okHttpClient.build();
    }

    /**
     * Set retrofitForJiraProjects according to needs
     * @param okHttpClient
     */
    private void updateJiraSessionClients(OkHttpClient okHttpClient)
    {
        retrofitForJiraProjects = new Retrofit.Builder().baseUrl(BASE_URL_JIRAPROJECTS).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        projectsService = retrofitForJiraProjects.create(JiraApiService.class);
    }

    /**This class is used to initialize the Webservice because every new Activity the listener must change
     *
     * @param listener Instance of AsyncDelegate
     */
    public void setListener(AsyncDelegate listener)
    {
        this.listener = listener;
        Log.e("setListener", "changed activity - " +mContext.getPackageName());
        gson = new Gson();
        retrofitForWorktime = getRetroFitClient(BASE_URL_WORKTIME);
        retrofitForFirebase = getRetroFitClient(BASE_URL_FIREBASE);
        worktimeService = retrofitForWorktime.create(JiraApiService.class);
        firebaseService = retrofitForFirebase.create(JiraApiService.class);

        loginTask = new Login(mContext, listener);
        tokenValidatorTask = new TokenValidator(mContext, listener);
        worktimeImporter = new WorktimeImporter(mContext, listener);
        issueLoadingTask = new IssueLoader(mContext,listener);
        worktimeListingTask = new WorktimeLister(mContext,listener);
        worktimeDayListingTask = new WorktimeDayLister(listener);
        editWorktimeTask = new TimestampChanger(mContext);
        deleteWorktimesTask = new WorktimeDeleter(listener);
        authenticationJiraTask = new JiraAuthenticator(mContext,listener);
        deauthenticationJiraTask = new JiraDeauthenticator(mContext,listener);
        notifierTask = new Notifier(mContext,listener);

        updateJiraSessionClients(getOkHttpClient(new AddCookiesInterceptor(mContext),new ReceivedCookiesInterceptor(mContext)));
    }

    public boolean isGuest()
    {
        return guest;
    }

    public void setGuest(boolean guest)
    {
        guest = guest;
    }

    public Context getmContext() {
        return mContext;
    }

    public AsyncDelegate getListener() {
        return listener;
    }
}
