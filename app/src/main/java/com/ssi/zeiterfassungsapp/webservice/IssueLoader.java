package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.IssueResponse;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import java.lang.reflect.Type;
import retrofit2.*;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-08
 */
public class IssueLoader implements Callback
{
    private static final String TAG = "IssueLoader";
    private AsyncDelegate delegate;
    private Context mContext;

    public IssueLoader(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        Type type = new TypeToken<IssueResponse>(){}.getType(); //storing the type of response
        IssueResponse issueResponse; //the response object to hold the response
        Gson gson = new Gson();

        if(response.code() < 400) //If the Code is above 400 it is an error response
        {
            issueResponse = (IssueResponse)response.body(); //transforms the response into an IssueResponse
            ListingProvider.getInstance().setIssues(issueResponse.getIssues()); //calls the method to fill the lists with Issues
            Toast.makeText(mContext,"Gathering issues was successful!",Toast.LENGTH_LONG); //notify user of successful completion
            delegate.issueLoadingComplete(true); //notify Activity of successful completion
        }
        else
        {
            Log.e(TAG,response.code()+"");
            Toast.makeText(mContext, "Gathering issues failed - please try again later!", Toast.LENGTH_LONG); //notify the user of failed completion
            delegate.issueLoadingComplete(false); //notify Activity of failed completion
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        Toast.makeText(mContext,"Gathering issues failed - please try again later!",Toast.LENGTH_LONG); //notify user of failed completion
        delegate.issueLoadingComplete(false); //notify Activity of failed completion
    }
}
