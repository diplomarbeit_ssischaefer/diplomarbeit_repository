package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * This class functions as an adapter for the WorktimeList and it configures and sets all the
 * This class catches the response and the failure
 * @author Matthias Pöschl
 * @since  2019/07/09
 */
public class Notifier implements Callback
{
    private static String TAG = "Notifier";
    private Context mContext;
    private AsyncDelegate delegate;

    public Notifier(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        Log.e(TAG,"Notification sent");
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        Log.e(TAG,"Registration key is invalid");
    }
}
