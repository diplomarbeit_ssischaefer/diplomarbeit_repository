package com.ssi.zeiterfassungsapp.webservice;

import com.ssi.zeiterfassungsapp.beans.Responses.IssueResponse;
import com.ssi.zeiterfassungsapp.beans.Responses.LoginResponse;
import com.ssi.zeiterfassungsapp.beans.Notification;
import com.ssi.zeiterfassungsapp.beans.Responses.PostWorkTimeResponse;
import com.ssi.zeiterfassungsapp.beans.Responses.ValidationResponse;
import com.ssi.zeiterfassungsapp.beans.Responses.WorktimeResponse;
import java.util.Map;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**This Interface is used to provide User-Defined Requests
 *
 * @author Klaminger & Schwarzkogler
 * @since 2019/07/10
 */
public interface JiraApiService
{
    /**This Request is used for Login
     *
     * @param username String of Username
     * @param password String of password
     * @return Object of Type LoginResponse
     */
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> loginWithUser(@Field("username") String username, @Field("password") String password);

    /**This Request is used to validate an existing Token
     *
     * @param token String of Token
     * @return Object of Type ValidationResponse
     */
    @FormUrlEncoded
    @POST("checkToken")
    Call<ValidationResponse> checkToken(@Field("tokenid") String token);

    /**This Request is used to pass worktime to the Webservice in order to import it
     *
     * @param token String of token
     * @param bookuser String of username
     * @param input Formatted String of all Bookings (Worktimes)
     * @return Object of Type PostWorkTimeResponse
     */
    @FormUrlEncoded
    @POST("postWorktime")
    Call<PostWorkTimeResponse> postWorktime(@Field("tokenid") String token, @Field("bookuser") String bookuser, @Field("input") String input);

    /**
     * This Request is used for gathering worktimes
     * @param params may contain
     *      tokenid     - required
     *      datefrom    - optional - if empty or not set: first of actual month in format YYYYMMDD
     *      dateto      - optional - if empty or not set: last of actual month in format YYYYMMDD
     *      bookinguser - optional - only bookings from one user
     * @return Object of Type WorktimeResponse
     */
    @GET("getWorktime")
    Call<WorktimeResponse> requestWorktimes(@QueryMap Map<String,String> params);

    /**
     * This Request is used to delete the worktimes of a user of a specific day (all worktimes of the day are removed)
     * @param tokenid  - required
     * @param date     - of which all worktimes will be removed
     * @param bookuser - username
     * @return
     */
    @DELETE("deleteWorktime")
    Call<Object> removeWorktimesOfDay(@Query("tokenid") String tokenid, @Query("date") String date, @Query("bookuser") String bookuser);

    /**
     *TODO Javadoc Comment!!
     *
     */
    @Headers({"Authorization: key=AAAATcoV-Ec:APA91bF_SEJVSG6LSn21URFGOqBX5VkkSqf6mZc8ASt6ebdSQtNqRg51_vk9JVKJyJGdlY7kEU-GHAW_HXRbWHRmobREXivf_h5kF8zSyl4YKio2sFvS3TAb3v6_isMfRnRmI2x0NpQk",
            "Content-Type:application/json"})
    @POST("send")
    Call<ResponseBody> createNotification(@Body Notification notification);

    /**
     * This Request is used to authenticate the User in Jira to request Projects
     * @param username String of Username
     * @param password String of Password
     * @return ResponseBody
     */
    @FormUrlEncoded
    @POST("auth/1/session")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> authenticateJira(@Field("username") String username, @Field("password") String password);

    /**
     * This Request is used to log User out of Jira
     * @return ResponseBody Object
     */
    @DELETE("auth/1/session")
    Call<ResponseBody> logoutJira();

    /**
     * Request Issues of the current user via Jira
     * @return IssueResponse
     */
    @GET("api/2/search?jql=assignee=currentuser()&issuetype=sub-task&fields=key,id,issuetype,project,subtasks,summary,status,customfield_13176")
    Call<IssueResponse> requestIssuesJira();
}
