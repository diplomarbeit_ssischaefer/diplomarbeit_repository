package com.ssi.zeiterfassungsapp.webservice;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.WorktimeResponse;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import java.io.IOException;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-24
 */
public class WorktimeDayLister implements Callback
{
    private static final String TAG = "WorktimeDayLister";
    private AsyncDelegate delegate;

    public WorktimeDayLister(AsyncDelegate delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        Type type = new TypeToken<WorktimeResponse>(){}.getType(); //storing the type of response
        WorktimeResponse worktimeResponse; //the response object to hold the response
        Gson gson = new Gson();

        if(response.code() == 200) //If the Code is equal to 200 it is a valid response
        {
            worktimeResponse = (WorktimeResponse) response.body(); //transforms the response into a GetResponse
            ListingProvider.getInstance().setWorktimesDateSpecific(worktimeResponse.getResult()); //calls the method to fill the list with Worktimes
            delegate.worktimeDayLoadingComplete(true); //notify Activity of successful completion
        }
        else
        {
            try
            {
                worktimeResponse = gson.fromJson(response.errorBody().string(), type); //transforms the response
            }
            catch(IOException e)
            {
                Log.e(TAG,"Error: " + e.getMessage());
            }
            finally
            {
                delegate.worktimeDayLoadingComplete(false); //notify Activity of failed completion
            }
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        delegate.worktimeDayLoadingComplete(false); //notify Activity of failed completion
    }
}
