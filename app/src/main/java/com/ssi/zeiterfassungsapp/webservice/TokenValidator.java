package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.ValidationResponse;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import java.io.IOException;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**This class is used to validate the Token stored in SharedPreferences
 *
 * @author Schwarzkogler
 * @since 2019/07/09
 */
public class TokenValidator implements Callback
{
    private static final String TAG = "TokenValidator";
    private Context mContext;
    private Gson gson;
    private AsyncDelegate delegate;

    public TokenValidator(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        gson = new Gson();
        this.delegate = delegate;
    }

    /**This method simply evaluates the response of the Callable
     *
     * @param call Call variable
     * @param response response variable in order to evaluate it
     */
    @Override
    public void onResponse(Call call, Response response)
    {
        ValidationResponse validationResponse = null;
        Type type = new TypeToken<ValidationResponse>(){}.getType(); //Creates an instance of the Object Type ValidationResponse

        if(response.code() < 400) //If response code is over 400 -> Error
        {
            validationResponse = (ValidationResponse) response.body();

            if(validationResponse.getValid().equals("no"))
            {
                Toast.makeText(mContext, "Token not found or expired - Please login", Toast.LENGTH_LONG).show();

                delegate.validationComplete(false); //Notifies MainActivity that the task has failed
            }
            else
            {
                Toast.makeText(mContext, "Token valid", Toast.LENGTH_LONG).show();

                delegate.validationComplete(true); //Notifies MainActivity that the task is successfully completed
            }
        }
        else
        {
            try
            {
                validationResponse = gson.fromJson(response.errorBody().string(), type);

                Toast.makeText(mContext, "Token not found or expired - Please login", Toast.LENGTH_LONG).show();

                delegate.validationComplete(false);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**This method simply evaluates the Error if an error occurs
     *
     * @param call Call variable
     * @param t Throwable variable
     */
    @Override
    public void onFailure(Call call, Throwable t)
    {
        Toast.makeText(mContext, "Failed to connect - Please use WIFI or Mobile Data or login as Guest", Toast.LENGTH_LONG).show();

        delegate.validationComplete(false);

        Log.e(TAG, "Error: " + t.getMessage());
    }
}
