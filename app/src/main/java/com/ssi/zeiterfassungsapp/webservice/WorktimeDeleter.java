package com.ssi.zeiterfassungsapp.webservice;

import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-25
 */
public class WorktimeDeleter implements Callback
{
    private static final String TAG = "WorktimeDeleter";
    private AsyncDelegate delegate;

    public WorktimeDeleter(AsyncDelegate delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        if(response.code() == 200) //If the Code is equal to 200 it is a valid response
        {
            delegate.deleteWorktimesComplete(true); //notify Activity of successful completion
        }
        else
        {
            delegate.worktimeLoadingComplete(false); //notify Activity of failed completion
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        delegate.deleteWorktimesComplete(false);
    }
}
