package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ssi.zeiterfassungsapp.beans.Responses.PostWorkTimeResponse;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.PostWorktimeDeserializer;
import java.lang.reflect.Type;
import java.util.LinkedList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**This class is used to evaluate the Response of PostWorktime
 *
 * @author Schwarzkogler
 * @since 2019/07/12
 */
public class WorktimeImporter implements Callback
{
    private static final String TAG = "WorktimeImporter";
    private Context mContext;
    private AsyncDelegate delegate;
    private Gson gson;

    public WorktimeImporter(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
        gson = new GsonBuilder()
                .setLenient()
                .registerTypeAdapter(PostWorkTimeResponse.class, new PostWorktimeDeserializer()) //Sets the custom deserializer: Because the default Deserializer doesn't do his work like we want...
                .setDateFormat("dd.MM.yyyy HH:mm").create(); //Sets the date format if neseccary
    }

    /**This method simply gets called when the application receives an Response of PostWorktime
     *
     * @param call Call Object
     * @param response Response Object of Request
     */
    @Override
    public void onResponse(Call call, Response response)
    {
        try
        {
            Type type = new TypeToken<PostWorkTimeResponse>(){}.getType(); //Creates instance of the Type (PostWorkTimeResponse)
            PostWorkTimeResponse workTimeResponse;

            if(response.code() == 200) //When the response code is 200 (Successfully)
            {
                String jsonObject = gson.toJson(response.body()); //Creates a String of the JSON Object
                workTimeResponse = gson.fromJson(jsonObject, type); //Creates a PostWorktimeResponse Object of the Response
                delegate.postingWorktimeCompleted(true);
            }
            else //When the Response code not 200 (Failed)
            {
                workTimeResponse = gson.fromJson(response.errorBody().string(), type); //Converts the response to LoginResponse
                delegate.postingWorktimeCompleted(false);
            }

            //Log.e(TAG, workTimeResponse.toString());
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error onResponse: " + e.getMessage() + " " + e.getCause());
            delegate.postingWorktimeCompleted(false);
        }
    }

    /**This method will be called when an error occured during Request
     *
     * @param call Call Object
     * @param t Throwable Object of the Exception
     */
    @Override
    public void onFailure(Call call, Throwable t)
    {
        Log.e(TAG, "Error onFailure: " + t.getMessage() + " " + t.getCause());

        delegate.postingWorktimeCompleted(false);
    }

    /**This method simply creates a String which prepares the List of Worktimes to a String
     *
     * @param worktimes List of all Worktimes
     * @return Formatted String
     */
    public String getPreparedDataForRequest(LinkedList<Worktime> worktimes)
    {
        String preparedList = "{"; //Start of the JSON Syntax

        for (int i = 0; i < worktimes.size(); i++) //Iterates through all Worktimes
        {
            System.out.println(i);

            if(i == worktimes.size()-1) //If Worktime is the last in the list, then the Worktime String adds a closed Bracket
            {
                preparedList += "\"" + i + "\":" + worktimes.get(i).toRequest() + "}";
            }
            else
            {
                preparedList += "\"" + i + "\":" + worktimes.get(i).toRequest() + ", "; //The worktimes will add a comma to seperate the Worktime Objects
            }
        }

        System.out.println(preparedList);

        return preparedList;
    }
}