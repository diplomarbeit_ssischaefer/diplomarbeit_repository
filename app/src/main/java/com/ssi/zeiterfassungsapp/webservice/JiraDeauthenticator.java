package com.ssi.zeiterfassungsapp.webservice;

import android.content.Context;
import android.util.Log;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import com.ssi.zeiterfassungsapp.util.JiraInterceptors.Methods;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-18
 */
public class JiraDeauthenticator implements Callback
{
    private static final String TAG = "JiraDeauthenticator";
    private Context mContext;
    private AsyncDelegate delegate;

    public JiraDeauthenticator(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    public void onResponse(Call call, Response response)
    {
        if(response.code() == 204 || response.code() == 401)
        {
            Methods.removeCookies(mContext);
            delegate.deauthenticationJiraComplete(true);
        }
        else
        {
            delegate.deauthenticationJiraComplete(false);
        }
    }

    @Override
    public void onFailure(Call call, Throwable t)
    {
        delegate.deauthenticationJiraComplete(false);
        Log.e(TAG,t.getMessage());
    }
}
