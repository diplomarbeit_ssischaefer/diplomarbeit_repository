package com.ssi.zeiterfassungsapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**This Class is the Util Class for determining the Network State
 *
 * @author Schwarzkogler
 * @since 2019/07/08
 */
public class NetworkUtil
{
    public static final String TAG = "NetworkUtil";

    /** Method which returns specific Network State in order to allow/deny Network operations
     *
     * @param mContext
     * @return True or False depending on Network State
     */
    public static boolean isConnected(Context mContext)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null)
        {
            Log.d(TAG, networkInfo.getTypeName());
            switch(networkInfo.getType())
            {
                case ConnectivityManager.TYPE_MOBILE: return true;
                case ConnectivityManager.TYPE_WIFI: return true;
                default: return false;
            }
        }

        return false;
    }
}
