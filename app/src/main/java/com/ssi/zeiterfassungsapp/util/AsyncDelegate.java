package com.ssi.zeiterfassungsapp.util;

import com.ssi.zeiterfassungsapp.beans.Worktime;
import java.util.List;

/**This interface is used to notify Activities about the current state of a Background Task
 *
 * @author Klaminger & Schwarzkogler
 * @since 2019/07/08
 */
public interface AsyncDelegate
{
    /**This method simply notifies an Activity if Login is completed
     *
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void loginComplete(boolean success);

    /**This method simply notifies an Activity if Token Validation is completed
     *
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void validationComplete(boolean success);

    /**
     * This method simply notifies an Activity if Gathering Issues is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void issueLoadingComplete(boolean success);

    /**
     * This method simply notifies an Activity if Gathering Worktimes is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void worktimeLoadingComplete(boolean success);

    /**
     * This method simply notifies an Activity if Gathering Worktimes of a Day is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void worktimeDayLoadingComplete(boolean success);

    /**
     * This method simply notifies an Activity if Authentication in Jira is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void authenticationJiraComplete(boolean success);

    /**
     * This method simply notifies an Activity if Deauthentication in Jira is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void deauthenticationJiraComplete(boolean success);

    void notificationSent(boolean success);

    /**
     * This method simply returns a Worktime List of the current database state
     * @param worktimes List of Worktimes in the database
     * @param usage
     */
    void updateWorktimeList(List<Worktime> worktimes, int usage);

    /**
     * This method simply notifies an Activity if Deletion of Worktimes is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void deleteWorktimesComplete(boolean success);

    void stopWorktimeComplete(boolean success);

    /**
     * This method simply notifies an Activity if Posting Worktimes is completed
     * @param success State of Completion (True -> Successful; False -> Failed)
     */
    void postingWorktimeCompleted(boolean success);
    //TODO: Add further Notifier below!
}
