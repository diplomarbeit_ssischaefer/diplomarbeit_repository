package com.ssi.zeiterfassungsapp.util;

import android.util.Log;

import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.beans.WorktimeRetrieved;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-17
 */
public class ListingProvider
{
    private TreeSet<Project> projects;
    private ArrayList<Issue> issues;
    private ArrayList<Worktime> worktimes;
    private TreeSet<Worktime> worktimesDateSpecific;
    private static ListingProvider instance;

    private ListingProvider()
    {
        projects = new TreeSet<>();
        issues = new ArrayList<>();
        worktimes = new ArrayList<>();
        worktimesDateSpecific = new TreeSet<>();
    }

    public static ListingProvider getInstance()
    {
        return instance == null ? instance = new ListingProvider() : instance;
    }

    //Everything to do with Projects
    public ArrayList<Project> getProjects()
    {
        return new ArrayList<>(projects);
    }

    //Everything to do with Issues
    /**
     * Get all issues regardless of any filter
     * @return ArrayList<Issue> containing all available Issues
     */
    public ArrayList<Issue> getIssues()
    {
        return issues;
    }

    /**
     * Method adds Issues to the complete Issuelist and also adds the projects that contain relevant issues to the list
     * @param issues
     */
    public void setIssues(ArrayList<Issue> issues)
    {
        String currentUser = Webservice.getInstance().loadUsername().split("@")[0].replace('.',' ');
        StringBuilder strb = new StringBuilder(currentUser);
        strb = new StringBuilder(strb.substring(strb.indexOf(" ")) + " " + strb.substring(0,strb.indexOf(" ")));
        currentUser = strb.toString();
        clearProjectsAndIssues();
        for (Issue issue: issues)
        {
            System.out.println(issue);
            //Issuetype-ID = 8 -> Sub-Task -> time tracking only allowed for this type of issue!!
            if(Integer.parseInt(issue.getFields().getIssuetype().getId()) == 8
            && issue.getFields().isTimeLoggingActivated()) //checks if current user is timelogging allowed
            {
                this.issues.add(issue);
            }
        }

        for (Issue issue: this.issues)
        {
            projects.add(issue.getFields().getProject().transformToProject());
        }
        Collections.sort(this.issues);
    }

    /**
     * Get all Issues of a specified project
     * @param projectkey String specifying the project (UPPERCASE)
     * @return ArrayList<Issue> containing all available Issues of the project
     */
    public ArrayList<Issue> getIssuesOfProject(String projectkey)
    {
        ArrayList<Issue> filteredIssues = new ArrayList<>();

        for (Issue issue: issues)
        {
            if(issue.getFields().getProject().getKey().equals(projectkey))
            {
                filteredIssues.add(issue);
            }
        }
        Collections.sort(filteredIssues);
        return filteredIssues;
    }

    /**
     * clear and reset the lists of Issues and Projects so they can be filled with new ones again
     */
    public void clearProjectsAndIssues()
    {
        issues.clear();
        projects.clear();
    }
    /**
     * clear and reset the lists of worktimes so they can be filled with new ones again
     */
    public void clearWorktimes()
    {
        worktimes.clear();
    }

    //Everything to do with Worktimes
    /**
     * Get all worktimes (as set and added)
     * @return ArrayList<Worktime> containing all set and added worktimes
     */
    public ArrayList<Worktime> getWorktimes()
    {
        return worktimes;
    }

    /**
     * Initial setting of worktimes
     * If logged in as guest: use for setting local worktimes - if logged in as user: use for setting API requested worktimes
     * @param worktimes ArrayList<Worktime> containing Worktimes
     */
    public void setWorktimes(ArrayList<WorktimeRetrieved> worktimes)
    {
        this.worktimes.clear();

        for (WorktimeRetrieved worktime_r : worktimes)
        {
            this.worktimes.add(worktime_r.transformToWorktime());
        }
    }

    public ArrayList<Worktime> getWorktimesDateSpecific()
    {
        return new ArrayList<>(worktimesDateSpecific);
    }

    /**
     * Set Worktimes that are requested of a specific date or period of time
     * @param worktimesDateSpecific List of worktimes to be added
     */
    public void setWorktimesDateSpecific(ArrayList<WorktimeRetrieved> worktimesDateSpecific)
    {
        this.worktimesDateSpecific.clear();

        if(worktimesDateSpecific != null && worktimesDateSpecific.size() > 0)
        {
            for (WorktimeRetrieved worktime_r : worktimesDateSpecific)
            {
                this.worktimesDateSpecific.add(worktime_r.transformToWorktime());
            }
        }
    }

    /**
     * If the local database worktimes need to be accessed together with the requested ones
     * @param localWorktimes ArrayList<Worktime> containing local Worktimes
     */
    public void addLocalWorktimes(List<Worktime> localWorktimes)
    {
        if(worktimes == null || worktimes.size() == 0)
        {
            worktimes = new ArrayList<>(localWorktimes);
        }
        else
        {
            for (Worktime worktime : localWorktimes)
            {
                if(!worktimes.contains(worktime))
                {
                    worktimes.add(worktime);
                }
            }
        }
    }

    /**
     * This method sorts all Worktimes according to the following criteria:
     * -unsynced worktimes before synced worktimes
     * -each category (unsynced/synced) ordered by startdate
     * This method should be used for the reset-filter button in the UI
     * @return List of sorted worktimes
     */
    public ArrayList<Worktime> sortWorktimesBasicRefresh()
    {
        ArrayList<Worktime> filteredWorktimes = new ArrayList<>();

        long day_in_ms = 1000*60*60*24;
        Date today = new Date(System.currentTimeMillis()); //today
        Date daysago = new Date(System.currentTimeMillis() - (3* day_in_ms)); //3 days ago
        for (Worktime worktime: worktimes)
        {
            if(worktime.isSynced() && worktime.getStartDate().after(daysago) && worktime.getStartDate().before(today)
                    || !worktime.isSynced())
            {
                filteredWorktimes.add(worktime);
            }
        }
        Comparator<Worktime> wtCompi = new Comparator<Worktime>()
        {
            @Override
            public int compare(Worktime wt1, Worktime wt2)
            {
                int compval =  wt1.isSynced() && !wt2.isSynced()? 1 : !wt1.isSynced() && wt2.isSynced() ? -1 : 0;
                if(compval != 0)
                {
                    return compval;
                }
                compval = wt2.getEndDate().compareTo(wt1.getEndDate());
                return compval;
            }
        };
        Collections.sort(filteredWorktimes,wtCompi);
        return filteredWorktimes;
    }

    /**
     * Filter for all relevant information
     * @param projectkey String of projectkey
     * @param startdatemillis long milliseconds of startdate
     * @param enddatemillis long milliseconds of enddate
     * @param synced boolean is worktime synced
     * @return ArrayList with all required Worktimes
     */
    public ArrayList<Worktime> filterWorktimes(String projectkey, long startdatemillis, long enddatemillis, boolean synced)
    {
        ArrayList<Worktime> filteredWorktimes = new ArrayList<>();
        if(projectkey.contains("-"))
        {
            projectkey = projectkey.split("-")[0];
        }
        for (Worktime worktime: worktimes)
        {
            boolean enddatecheck = false;
            if(worktime.getEndDate() == null || worktime.getEndDate().before(new Date(enddatemillis)))
            {
                enddatecheck = true;
            }
            if (synced == worktime.isSynced() &&
                    worktime.getStartDate().after(new Date(startdatemillis)) && enddatecheck)
            {
                if (projectkey.equalsIgnoreCase("All Projects"))
                {
                    filteredWorktimes.add(worktime);
                }
                else if (worktime.getIssueKey().contains(projectkey))
                {
                    filteredWorktimes.add(worktime);
                }
            }
        }
         return filteredWorktimes;
    }

    /**
     * Addeded back 18.10.2019
     * ToDo: Test For Errors
     * get Worktimes of a specific project
     * @param projectkey identifies the project (UPPERCASE)
     * @param worktimearray optional: if an already filtered worktime list needs to be filtered further, it can be passed as array here
     * @return ArrayList<Worktime> containing all filtered worktimes
     */
    public ArrayList<Worktime> getWorktimesOfProject(String projectkey, Worktime... worktimearray)
    {
        ArrayList<Worktime> allWorktimesAvailable = null;
        if(worktimearray != null && worktimearray.length >= 0)
        {
            allWorktimesAvailable = new ArrayList<>(Arrays.asList(worktimearray));
        }
        else
        {
            allWorktimesAvailable = new ArrayList<>(worktimes);
        }
        if(projectkey.equalsIgnoreCase("All projects"))
        {
            return allWorktimesAvailable;
        }
        //Filter for PROJECT
        if(projectkey.contains("-"))
        {
            projectkey = projectkey.split("-")[0];
        }
        ArrayList<Worktime> filteredWorktimes = new ArrayList<>();
        for(Worktime worktime : allWorktimesAvailable)
        {
            //Filter for PROJECT
            if(worktime.getIssueKey().contains(projectkey))
            {
                filteredWorktimes.add(worktime);
            }
            //Filter for ISSUE
            /*
            if(worktime.getIssueKey().equals(projectkey))
            {
                filteredWorktimes.add(worktime);
            }
            */
        }
        return filteredWorktimes;
    }
}
