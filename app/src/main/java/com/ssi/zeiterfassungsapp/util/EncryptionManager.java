package com.ssi.zeiterfassungsapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class EncryptionManager
{
    private static final String TAG = "EncryptionManager";
    private static Context mContext;
    private static final String SHARED_PREFS = "com.ssi.zeiterfassungsapp";
    private static SharedPreferences sharedPref;

    public EncryptionManager(Context mContext)
    {
        this.mContext = mContext;
        sharedPref = mContext.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public void encryptToken(String token) throws Exception
    {
        storeEncryptedTokenInPreferences(AESUtils.encrypt(token));
    }

    public String decryptToken() throws Exception
    {
        String encryptedToken = sharedPref.getString(SHARED_PREFS + ".token.value", null);

        return encryptedToken != null ? AESUtils.decrypt(encryptedToken) : null;
    }

    public static void storeEncryptedTokenInPreferences(String encryptedToken)
    {
        sharedPref.edit().putString(SHARED_PREFS + ".token.value", encryptedToken).apply();
    }

    public static void storeKeyInPreferences(String key, byte[] value)
    {
        for (int i = 0; i < value.length; i++)
        {
            sharedPref.edit().putString(key + "_" + i, new String(value[i] + "")).apply();
        }
    }

    public void storeUsernameInPreferences(String username)
    {
        sharedPref.edit().putString(SHARED_PREFS + ".bookuser", username).apply();
    }

    public String loadUsernameFromPreferences()
    {
        String username = sharedPref.getString(SHARED_PREFS + ".bookuser", new String());

        return username;
    }

    public void storeProjectColorPreferences(String projectColorsString)
    {
        sharedPref.edit().putString(SHARED_PREFS + ".projectColors", projectColorsString).apply();
    }

    public String loadProjectColorPreferences()
    {
        String projectColorsString = sharedPref.getString(SHARED_PREFS + ".projectColors", new String());

        return projectColorsString;
    }

    public void storeStandardIssuePreferences(String standardIssueString)
    {
        sharedPref.edit().putString(SHARED_PREFS + ".standardIssue", standardIssueString).apply();
    }

    public String loadStandardIssuePreferences()
    {
        String standardIssueString = sharedPref.getString(SHARED_PREFS + ".standardIssue", new String());

        return standardIssueString;
    }

    public void removeUserFromPreferences()
    {
        if(!loadRememberMeFromPreferences())
        {
            sharedPref.edit().remove(SHARED_PREFS + ".bookuser").apply();
        }
    }

    public void storeAutoLoginInPreferences(boolean autoLogin)
    {
        sharedPref.edit().putBoolean(SHARED_PREFS + ".autoLogin", autoLogin).apply();
    }

    public boolean loadAutoLoginFromPreferences()
    {
        return sharedPref.getBoolean(SHARED_PREFS + ".autoLogin", false);
    }

    public void storeRememberMeInPreferences(boolean rememberMe)
    {
        sharedPref.edit().putBoolean(SHARED_PREFS + ".rememberMe", rememberMe).apply();
    }

    public boolean loadRememberMeFromPreferences()
    {
        return sharedPref.getBoolean(SHARED_PREFS + ".rememberMe", false);
    }

    public static byte[] readKeyFromPreferences(String key) throws NullPointerException
    {
        byte[] value = new byte[16];

        for (int i = 0; i < 16; i++)
        {
            value[i] = Byte.parseByte(sharedPref.getString(key + "_" + i, null));
        }

        Log.w(TAG, "SharedPreferences: " + value);
        
        return value;
    }

    public void removeKeysFromPreferences(String... keys)
    {
        for (String key : keys)
        {
            for (int i = 0; i < 16; i++)
            {
                sharedPref.edit().remove(key + "_" + i).apply();
            }
        }
    }

    public void removeTokenFromPreferences()
    {
        sharedPref.edit().remove(SHARED_PREFS + ".token.value").apply();
    }
}
