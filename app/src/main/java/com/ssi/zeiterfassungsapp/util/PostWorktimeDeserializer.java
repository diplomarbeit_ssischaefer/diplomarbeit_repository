package com.ssi.zeiterfassungsapp.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.ssi.zeiterfassungsapp.beans.Responses.PostWorkTimeResponse;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import java.lang.reflect.Type;

/**This class serves as a Custom JSON Dezerializer
 * @author Schwarzkogler
 * @since 2019/07/16
 */
public class PostWorktimeDeserializer implements JsonDeserializer<PostWorkTimeResponse>
{
    private static final String TAG = "PostWorktimeDeserializer";

    /**This method simply deserializes the JSON Response String in order to map the String to PostWorkTimeResponse
     *
     * @param json JsonElement of the Response
     * @param typeOfT
     * @param context
     * @return PostWorktimeResponse Object
     * @throws JsonParseException
     */
    @Override
    public PostWorkTimeResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
    {
        JsonParser parser = new JsonParser();
        PostWorkTimeResponse workTimeResponse = new PostWorkTimeResponse();

        JsonObject jsonObject = parser.parse(String.valueOf(json)).getAsJsonObject(); //Parses the JsonElement Response to a JsonObject in order to access the JSON Data

        //Start of setting the values of the response to the Object
        workTimeResponse.setStatus(jsonObject.get("status").getAsString());

        if(workTimeResponse.getStatus().equals("Ok")) //Returns True if the Response is successfully
        {
            Worktime worktimes[] = new Worktime[jsonObject.get("booking").getAsJsonArray().size()]; //Initializes a Array
            workTimeResponse.setUser(jsonObject.get("user").getAsString());
            workTimeResponse.setOrigin(jsonObject.get("origin").getAsString());
            workTimeResponse.setTarget(jsonObject.get("target").getAsString());
            workTimeResponse.setBookuser(jsonObject.get("bookuser").getAsString());

            for (int i = 0; i < worktimes.length; i++) //Iterates through all Bookings and adds every booking to the Array
            {
                JsonElement element = jsonObject.get("booking").getAsJsonArray().get(i);
                Worktime w = new Worktime();
                w.setIssueKey(element.getAsJsonObject().get("ISSUEKEY").getAsString());
                w.setBookingtext(element.getAsJsonObject().get("BOOKINGTEXT").getAsString());
                w.setStartDateString(element.getAsJsonObject().get("STARTDATE").getAsString());
                w.setEndDateString(element.getAsJsonObject().get("ENDDATE").getAsString());
                worktimes[i] = w;
            }

            workTimeResponse.setWorktime(worktimes);
        }
        else //If the Response failed
        {
            workTimeResponse.setMsg(jsonObject.get("msg").getAsString());
        }

        return workTimeResponse; //Returns a mapped PostWorktimeResponse
    }
}
