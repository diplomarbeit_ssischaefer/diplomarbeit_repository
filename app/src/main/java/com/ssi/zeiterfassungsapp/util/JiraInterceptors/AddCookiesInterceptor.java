package com.ssi.zeiterfassungsapp.util.JiraInterceptors;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.util.HashSet;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-17
 */
public class AddCookiesInterceptor implements Interceptor
{
    private Context mContext;

    public AddCookiesInterceptor(Context mContext)
    {
        this.mContext = mContext;
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request.Builder builder = chain.request().newBuilder();
        HashSet<String> preferences = Methods.getCookies(mContext);

        for (String cookie : preferences)
        {
            builder.addHeader("Cookie",cookie);
        }

        return chain.proceed(builder.build());
    }
}
