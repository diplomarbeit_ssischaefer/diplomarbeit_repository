package com.ssi.zeiterfassungsapp.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateDeserializer implements JsonDeserializer<Date>
{
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
    {
        String date = json.getAsString();

        System.out.println(date);

        SimpleDateFormat sdt = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);

        try
        {
            return sdt.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
