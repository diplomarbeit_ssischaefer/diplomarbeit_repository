package com.ssi.zeiterfassungsapp.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.layouts.StartStopActivity;

/**
 * @author Matthias Pöschl
 * @since 2019/07/16
 * Service class for the notifications
 */
public class NotificationMessagingService extends FirebaseMessagingService
{
    //TODO: add extra receiver for notifications without stop button
    private static final String SHARED_PREFS = "com.ssi.zeiterfassungsapp";
    public static final String NOTIFICATION_REPLY = "NotificationReply";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);

        Log.d("msg", "onMessageReceived: " + remoteMessage.getData().get("message"));

        Intent intent = new Intent(this, StartStopActivity.class);
        intent.putExtra("MethodName", "Stop");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        String channelId = "Default";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setSmallIcon(R.mipmap.ssi_logo)
                .setContentText(remoteMessage.getData().get("body")).setAutoCancel(true)
                .addAction(R.mipmap.ssi_logo,"Stop",pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }

        builder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        manager.notify(0, builder.build());
    }
}