package com.ssi.zeiterfassungsapp.util.JiraInterceptors;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashSet;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-17
 */
public class Methods
{
    private static final String SHARED_PREFS = "com.ssi.zeiterfassungsapp";

    /**
     * Retrieve cookies from shared preferences to add to header of request
     * @param mContext
     * @return Set of Cookies
     */
    public static HashSet<String> getCookies(Context mContext)
    {
        SharedPreferences myPreferences = mContext.getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);

        return (HashSet<String>) myPreferences.getStringSet("cookies",new HashSet<String>());
    }

    /**
     * Store cookies from header of response to share preferences
     * @param mContext
     * @param cookies
     * @return true if storing was successful, false if not
     */
    public static boolean setCookies(Context mContext, HashSet<String> cookies)
    {
        SharedPreferences myPreferences = mContext.getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();

        return editor.putStringSet("cookies",cookies).commit();
    }

    /**
     * Remove cookies after closing the app/deauthorising
     * @param mContext
     * @return true if removal was successful, false if not
     */
    public static boolean removeCookies(Context mContext)
    {
        SharedPreferences myPreferences = mContext.getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();

        return editor.remove("cookies").commit();
    }

    public static boolean isCookieSet(Context mContext)
    {
        SharedPreferences myPreferences = mContext.getSharedPreferences(SHARED_PREFS,Context.MODE_PRIVATE);

        return myPreferences.contains("cookies");
    }
}
