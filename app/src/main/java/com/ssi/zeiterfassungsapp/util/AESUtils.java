package com.ssi.zeiterfassungsapp.util;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils
{
    private static byte[] generate()
    {
        SecureRandom secureRandom = new SecureRandom();
        byte[] values = new byte[16];

        secureRandom.nextBytes(values);

        return values;
    }

    public static String encrypt(String cleartext) throws Exception
    {
        EncryptionManager.storeKeyInPreferences("com.ssi.zeiterfassungsapp.token.iv", generate()); //Stores the iv to SharedPreferences
        EncryptionManager.storeKeyInPreferences("com.ssi.zeiterfassungsapp.token.key", generate()); //Stores the keyValue to SharedPreferences

        byte[] rawKey = getRawKey();
        byte[] result = encrypt(rawKey, cleartext.getBytes());

        return toHex(result);
    }

    public static String decrypt(String encrypted) throws Exception
    {
        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(enc);

        return new String(result);
    }

    private static byte[] getRawKey()
    {
        SecretKey keySpec = new SecretKeySpec(EncryptionManager.readKeyFromPreferences("com.ssi.zeiterfassungsapp.token.key"), "AES/GCM/NOPADDING");
        byte[] raw = keySpec.getEncoded();

        return raw;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception
    {
        SecretKey keySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/GCM/NOPADDING");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(EncryptionManager.readKeyFromPreferences("com.ssi.zeiterfassungsapp.token.iv")));
        byte[] encrypted = cipher.doFinal(clear);

        return encrypted;
    }

    private static byte[] decrypt(byte[] encrypted) throws Exception
    {
        SecretKey keySpec = new SecretKeySpec(EncryptionManager.readKeyFromPreferences("com.ssi.zeiterfassungsapp.token.key"), "AES");
        Cipher cipher = Cipher.getInstance("AES/GCM/NOPADDING");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(EncryptionManager.readKeyFromPreferences("com.ssi.zeiterfassungsapp.token.iv")));
        byte[] decrypted = cipher.doFinal(encrypted);

        return decrypted;
    }

    private static byte[] toByte(String hexString)
    {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];

        for (int i = 0; i < len; i++)
        {
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
        }

        return result;
    }

    private static String toHex(byte[] buf)
    {
        if (buf == null)
        {
            return "";
        }

        StringBuffer result = new StringBuffer(2 * buf.length);

        for (int i = 0; i < buf.length; i++)
        {
            appendHex(result, buf[i]);
        }

        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b)
    {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }
}