package com.ssi.zeiterfassungsapp.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.widget.Toast;

/** Will be triggered every network change
 *
 * @author Schwarzkogler
 * @since 2019/07/08
 */
public class NetworkReceiver extends BroadcastReceiver
{
    private static final String TAG = "NetworkReceiver";

    /**Constructor for Class, which registers the Receiver to particular activity
     *
     * @param activity
     */
    public NetworkReceiver(Activity activity)
    {
        activity.registerReceiver(this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public NetworkReceiver()
    {

    }

    /**Method will be called every network change
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(!NetworkUtil.isConnected(context))
        {
            Toast.makeText(context, "No connection available", Toast.LENGTH_SHORT).show();
        }
    }
}
