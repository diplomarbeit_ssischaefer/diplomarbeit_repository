package com.ssi.zeiterfassungsapp.database;

import android.content.Context;
import android.os.AsyncTask;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import java.util.LinkedList;
import java.util.List;

/**This class simply reads the entities from the database
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
public class DatabaseReader extends AsyncTask<Void,Void, List<Worktime>>
{
    private Context mContext;
    private AsyncDelegate delegate;
    private static final String TAG = "DatabaseReader";
    private int usage;  // 0...Only for reading all the entries
                        // 1...For syncing the database with the webservice

    public DatabaseReader(Context mContext, AsyncDelegate delegate, int usage)
    {
        this.mContext = mContext;
        this.delegate = delegate;
        this.usage = usage;
    }

    @Override
    protected List<Worktime> doInBackground(Void... voids)
    {
        List<Worktime> tempWorktimes = DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().getAll();

        return tempWorktimes;
    }

    @Override
    protected void onPostExecute(List<Worktime> worktimes)
    {
        super.onPostExecute(worktimes);

        List<Worktime> finishedWorktimes = new LinkedList<>();

        for (Worktime worktime : worktimes)
        {
            if(worktime.getEndDate() != null)
            {
                worktime.setSynced(false);
                finishedWorktimes.add(worktime);
            }
        }

        delegate.updateWorktimeList(finishedWorktimes, usage);
    }
}
