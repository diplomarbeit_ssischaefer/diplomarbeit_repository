package com.ssi.zeiterfassungsapp.database;

import android.content.Context;
import android.os.AsyncTask;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import java.util.List;

/**This class works as a Background Thread for Updating either running Worktimes or other Worktimes
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
public class DatabaseUpdater extends AsyncTask<Worktime,Void, List<Worktime>>
{
    private Context mContext;
    private AsyncDelegate delegate;

    public DatabaseUpdater(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    protected List<Worktime> doInBackground(Worktime... worktimes)
    {
        if(worktimes.length == 1) //If Only one Worktime Object -> Worktime stopped and will be stored as new object
        {
            //FIXME: The Constraint will get thrown because apparently, the EndDate is NULL... This error gets trhown while pressing the Stop Button
            String endDate = worktimes[0].getEndDateString();
            String bookingText = worktimes[0].getBookingtext();

            DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().updateOnGoingWorktime(endDate, bookingText);
        }
        else if(worktimes.length == 2)
        {
            DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().deleteWorktime(worktimes[0].getStartDateString(), worktimes[0].getEndDateString()); //Deletes old Worktime
            DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().insertWorktime(worktimes[1]); //replaces old Worktime with new Worktime
        }

        List<Worktime> list = DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().getAll();

        return list;
    }

    @Override
    protected void onPostExecute(List<Worktime> list)
    {
        //delegate.updateWorktimeList(list, usage);
        delegate.stopWorktimeComplete(true);
    }
}
