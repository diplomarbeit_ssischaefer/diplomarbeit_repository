package com.ssi.zeiterfassungsapp.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.work.Logger;

import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import java.util.List;

public class DatabaseSaver extends AsyncTask<Worktime,Void, List<Worktime>>
{
    private Context mContext;
    private AsyncDelegate delegate;
    private int usage;

    public DatabaseSaver(Context mContext, AsyncDelegate delegate, int usage)
    {
        this.mContext = mContext;
        this.delegate = delegate;
        this.usage = usage;
    }

    @Override
    protected List<Worktime> doInBackground(Worktime... worktimes)
    {
        DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().insertWorktime(worktimes[0]);

        List<Worktime> list = DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().getAll();

        return list;
    }

    @Override
    protected void onPostExecute(List<Worktime> list)
    {
        /*
         * a NullPointer gets thrown when the WidgetActivity starts a worktime,
         * catching it allows normal operation
         */
        //List<Worktime> after = DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().getAll();
        //Log.e("AfterAfter", after.size()+"");
        try
        {
            Log.e("DBSaver_onPostExecute", list.size() + "");
            if(usage != 3)
            {
                delegate.updateWorktimeList(list, 0);
            }
            else
            {
                delegate.updateWorktimeList(list, usage);
            }

        }
        catch (NullPointerException ex)
        {
            Log.v("NullPointerException","Due an error in webservice and/or widget a NullPointer was thrown in DatabaseSaver.java." +
                      "normal app procedure should still work.\n"+
                      "Why was this thrown?\n"+
                      "In WidgetActivity.startWork() a call to webservice.saveWorktimeInDatabase(..) will still save the worktime but will throw this exception a after a few seconds of its execution.");
        }

    }
}
