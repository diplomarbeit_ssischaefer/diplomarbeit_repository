package com.ssi.zeiterfassungsapp.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.ssi.zeiterfassungsapp.beans.Worktime;

/**This abstract class works as a Databse Object. It maintaines the basic configuration
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
@Database(entities = {Worktime.class}, version = 3, exportSchema = false)
public abstract class WorktimeDatabase extends RoomDatabase
{
    public abstract WorktimeDao worktimeDao();
}
