package com.ssi.zeiterfassungsapp.database;

import android.content.Context;
import android.os.AsyncTask;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.AsyncDelegate;
import java.util.List;

/**This class works as a Background Thread for Removing either all Entries or just a specific one
 * from the local Room Database
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
public class DatabaseRemover extends AsyncTask<Worktime,Void, List<Worktime>>
{
    private Context mContext;
    private AsyncDelegate delegate;

    public DatabaseRemover(Context mContext, AsyncDelegate delegate)
    {
        this.mContext = mContext;
        this.delegate = delegate;
    }

    @Override
    protected List<Worktime> doInBackground(Worktime... worktimes)
    {
        if(worktimes.length == 0) //When the worktimes length is zero, this means that no specific Worktime should be deleted and therefore it will delete all entries
        {
            DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().deleteAll();
        }
        else
        {
            for (Worktime w : worktimes)
            {
                DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().deleteWorktime(w.getStartDateString(), w.getEndDateString());
            }
        }

        List<Worktime> list = DatabaseClient.getInstance(mContext).getWorktimeDatabase().worktimeDao().getAll();

        return list;
    }

    @Override
    protected void onPostExecute(List<Worktime> worktimes)
    {
        delegate.updateWorktimeList(worktimes, 0);
    }
}
