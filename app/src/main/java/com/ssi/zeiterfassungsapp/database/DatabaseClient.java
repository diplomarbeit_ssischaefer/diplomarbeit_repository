package com.ssi.zeiterfassungsapp.database;

import android.content.Context;
import androidx.room.Room;

/**This class is a simple DatabaseClient as a Singleton Class in order to use always the same
 * Database Instance during the whole App Lifetime
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
public class DatabaseClient
{
    private Context mContext;
    private static DatabaseClient instance;
    private WorktimeDatabase worktimeDatabase;

    public static DatabaseClient getInstance(Context mContext)
    {
        if(instance == null)
        {
            instance = new DatabaseClient(mContext);
        }
        return instance;
    }

    private DatabaseClient(Context mContext)
    {
        this.mContext = mContext;
        worktimeDatabase = Room.databaseBuilder(mContext, WorktimeDatabase.class, "worktime_database").build(); //Create the database
    }

    /**This method simply returns the Database Instance of the WorktimeDatabase
     *
     * @return WorktimeDatabase Object Instance
     */
    public WorktimeDatabase getWorktimeDatabase()
    {
        return worktimeDatabase;
    }

    /**This method simply closes the Database Connection
     *
     */
    public void closeDatabase()
    {
        worktimeDatabase.close();
    }
}
