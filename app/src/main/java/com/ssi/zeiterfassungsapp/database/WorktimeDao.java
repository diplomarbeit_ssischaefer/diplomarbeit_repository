package com.ssi.zeiterfassungsapp.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import java.util.List;

/**This Interface is used to work as a Database Access Object. It helps to query and insert data into the local Room Database
 *
 * @author Schwarzkogler
 * @since 2019/07/17
 */
@Dao
public interface WorktimeDao
{
    @Query("SELECT * FROM Worktime")
    List<Worktime> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertWorktime(Worktime worktime);

    @Query("DELETE FROM Worktime")
    void deleteAll();

    @Query("SELECT * FROM Worktime WHERE end_date = ''")
    Worktime getRunningWorktime();

    @Query("UPDATE Worktime SET end_date = :endDate, booking_text = :text WHERE end_date = ''")
    void updateOnGoingWorktime(String endDate, String text);

    @Query("DELETE FROM Worktime WHERE start_date = :startDate AND end_date = :endDate")
    void deleteWorktime(String startDate, String endDate);
}
