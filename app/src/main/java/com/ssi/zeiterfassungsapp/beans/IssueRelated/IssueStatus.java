package com.ssi.zeiterfassungsapp.beans.IssueRelated;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-23
 */
public class IssueStatus
{
    private String name;
    private String id;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}
