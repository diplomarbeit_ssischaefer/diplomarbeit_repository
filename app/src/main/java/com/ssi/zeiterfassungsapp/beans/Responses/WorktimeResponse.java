package com.ssi.zeiterfassungsapp.beans.Responses;

import com.ssi.zeiterfassungsapp.beans.WorktimeRetrieved;
import java.util.ArrayList;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-09
 */
public class WorktimeResponse
{
    private String status;
    private String user;
    private ArrayList<WorktimeRetrieved> result;
    private int code;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public ArrayList<WorktimeRetrieved> getResult()
    {
        return result;
    }

    public void setResult(ArrayList<WorktimeRetrieved> result)
    {
        this.result = result;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "GetResponse{" +
                "status='" + status + '\'' +
                ", user='" + user + '\'' +
                ", result=" + result;
    }
}
