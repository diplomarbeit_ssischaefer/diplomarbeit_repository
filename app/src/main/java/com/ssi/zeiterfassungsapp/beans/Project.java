package com.ssi.zeiterfassungsapp.beans;

import android.widget.ImageView;
import com.ssi.zeiterfassungsapp.R;

public class Project implements Comparable<Project>
{
    private ImageView projectImage;
    private String projectName;
    private String projectKey;
    private int projectImageID;
    private int color;

    public Project(ImageView projectImage, String projectName, String projectKey)
    {
        this.projectImageID = R.drawable.ic_projectfolder;
        this.projectImage = projectImage;
        this.projectName = projectName;
        this.projectKey = projectKey;
    }

    public Project(String projectName, String projectKey)
    {
        this.projectName = projectName;
        this.projectKey = projectKey;
    }

    public ImageView getProjectImage()
    {
        return projectImage;
    }

    public void setProjectImage(ImageView projectImage)
    {
        this.projectImage = projectImage;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getProjectKey()
    {
        return projectKey;
    }

    public void setProjectKey(String projectKey)
    {
        this.projectKey = projectKey;
    }

    public int getProjectImageID()
    {
        return projectImageID;
    }

    public void setProjectImageID(int projectImageID)
    {
        this.projectImageID = projectImageID;
    }

    public int getColor()
    {
        return color;
    }

    public void setColor(int color)
    {
        this.color = color;
    }

    @Override
    public int compareTo(Project project)
    {
        return this.projectKey.compareTo(project.projectKey);
    }
}
