package com.ssi.zeiterfassungsapp.beans;

import android.util.Log;
import com.google.gson.annotations.SerializedName;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-23
 */
public class WorktimeRetrieved
{
    @SerializedName("ISSUEKEY")
    private String issuekey;
    @SerializedName("STARTDATET")
    private String startdatet;
    @SerializedName("ENDDATET")
    private String enddatet;
    @SerializedName("BOOKINGTEXT")
    private String bookingtext;

    private Date startdate;
    private Date enddate;
    private final SimpleDateFormat dateformat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    private static final String TAG = "WorktimeRetrieved";

    public String getIssuekey()
    {
        return issuekey;
    }

    public void setIssuekey(String issuekey)
    {
        this.issuekey = issuekey;
    }

    public String getStartdatet()
    {
        return startdatet;
    }

    public void setStartdatet(String startdatet)
    {
        this.startdatet = startdatet;

        try
        {
            this.startdate = dateformat.parse(startdatet);
        }
        catch (ParseException e)
        {
            Log.e(TAG,e.getMessage() + " startdate");
        }
    }

    public String getEnddatet()
    {
        return enddatet;
    }

    public void setEnddatet(String enddatet)
    {
        this.enddatet = enddatet;

        try
        {
            this.enddate = dateformat.parse(enddatet);
        }
        catch (ParseException e)
        {
            Log.e(TAG,e.getMessage() + " enddate");
        }
    }

    public Date getStartdate()
    {
        return startdate;
    }

    public Date getEnddate()
    {
        return enddate;
    }

    public String getBookingtext()
    {
        return bookingtext;
    }

    public void setBookingtext(String bookingtext)
    {
        this.bookingtext = bookingtext;
    }

    /**
     * create a Worktime Object with the data of this WorktimeRetrieved Object
     * @return Worktime Object
     */
    public Worktime transformToWorktime()
    {
        if(startdate == null)
        {
            setStartdatet(startdatet);
        }
        if(enddate == null)
        {
            setEnddatet(enddatet);
        }

        Worktime worktime = new Worktime();

        worktime.setIssueKey(issuekey);
        worktime.setStartDate(startdate);
        worktime.setEndDate(enddate);
        worktime.setBookingtext(bookingtext);
        worktime.setSynced(true);

        return worktime;
    }
}