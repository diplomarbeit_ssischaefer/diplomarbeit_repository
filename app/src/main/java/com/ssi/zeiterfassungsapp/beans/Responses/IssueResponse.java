package com.ssi.zeiterfassungsapp.beans.Responses;

import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import java.util.ArrayList;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-23
 */
public class IssueResponse
{
    private ArrayList<Issue> issues;

    public ArrayList<Issue> getIssues()
    {
        return issues;
    }

    public void setIssues(ArrayList<Issue> issues)
    {
        this.issues = issues;
    }
}
