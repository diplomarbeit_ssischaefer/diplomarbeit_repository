package com.ssi.zeiterfassungsapp.beans.Responses;

import com.google.gson.annotations.SerializedName;

public class ValidationResponse
{
    private String status;
    private String token;
    private String valid;
    private String tokencreated;
    @SerializedName("expires in min")
    private String expires;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getValid()
    {
        return valid;
    }

    public void setValid(String valid)
    {
        this.valid = valid;
    }

    public String getTokencreated()
    {
        return tokencreated;
    }

    public void setTokencreated(String tokencreated)
    {
        this.tokencreated = tokencreated;
    }

    public String getExpires()
    {
        return expires;
    }

    public void setExpires(String expires)
    {
        this.expires = expires;
    }

    @Override
    public String toString()
    {
        return "ValidationResponse{" +
                "status='" + status + '\'' +
                ", token='" + token + '\'' +
                ", valid='" + valid + '\'' +
                ", tokencreated='" + tokencreated + '\'' +
                ", expires='" + expires + '\'' +
                '}';
    }
}
