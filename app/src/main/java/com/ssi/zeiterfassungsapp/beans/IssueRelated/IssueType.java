package com.ssi.zeiterfassungsapp.beans.IssueRelated;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-23
 */
public class IssueType
{
    private String id;
    private String description;
    private String name;
    private boolean subtask;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isSubtask()
    {
        return subtask;
    }

    public void setSubtask(boolean subtask)
    {
        this.subtask = subtask;
    }
}
