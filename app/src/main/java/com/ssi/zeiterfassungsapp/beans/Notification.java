package com.ssi.zeiterfassungsapp.beans;

import com.google.gson.annotations.SerializedName;

/**
 * @author Matthias Pöschl
 * @since 2019/07/16
 * Beans class for notification
 */
public class Notification
{
    @SerializedName("to")
    private String token;
    @SerializedName("data")
    private NotificationModel notificationModel;

    public NotificationModel getNotificationModel()
    {
        return notificationModel;
    }

    public void setNotificationModel(NotificationModel notificationModel)
    {
        this.notificationModel = notificationModel;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}