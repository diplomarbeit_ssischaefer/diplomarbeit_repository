package com.ssi.zeiterfassungsapp.beans;

import java.util.Objects;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-17
 */
public class JiraProject implements Comparable<JiraProject>
{
    private String self;
    private String key;
    private String name;

    public String getSelf()
    {
        return self;
    }

    public void setSelf(String self)
    {
        this.self = self;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "JiraProject{" +
                "self='" + self + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public Project transformToProject()
    {
        return new Project(name,key);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JiraProject that = (JiraProject) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(key);
    }

    @Override
    public int compareTo(JiraProject jiraProject)
    {
        return jiraProject.getKey().compareTo(this.getKey());
    }
}
