package com.ssi.zeiterfassungsapp.beans.Responses;

public class LoginResponse
{
    private String status;
    private String token;
    private String msg;
    private int code;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "LoginResponse{" +
                "status='" + status + '\'' +
                ", token='" + token + '\'' +
                ", msg='" + msg + '\'' +
                ", code=" + code +
                '}';
    }
}
