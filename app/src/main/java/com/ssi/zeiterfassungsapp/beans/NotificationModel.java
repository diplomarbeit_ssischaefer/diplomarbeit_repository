package com.ssi.zeiterfassungsapp.beans;

/**
 * @author Matthias Pöschl
 * @since 2019/07/17
 * Beans for the title and body of the notifications
 */
public class NotificationModel
{
    private String body;
    private String title;

    public NotificationModel(String body, String title)
    {
        this.body = body;
        this.title = title;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}