package com.ssi.zeiterfassungsapp.beans.IssueRelated;

import java.util.Comparator;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-08
 */
public class Issue implements Comparable<Issue>
{
    private String key;
    private IssueFields fields;
    private int color;

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public IssueFields getFields()
    {
        return fields;
    }

    public void setFields(IssueFields fields)
    {
        this.fields = fields;
    }

    public int getColor()
    {
        return color;
    }

    public void setColor(int color)
    {
        this.color = color;
    }

    @Override
    public int compareTo(Issue issue) {
        return this.getKey().compareTo(issue.getKey());
    }
}
