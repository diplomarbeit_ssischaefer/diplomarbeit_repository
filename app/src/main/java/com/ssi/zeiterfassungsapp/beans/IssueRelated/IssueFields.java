package com.ssi.zeiterfassungsapp.beans.IssueRelated;

import com.ssi.zeiterfassungsapp.beans.JiraProject;

/**
 * @author Kerstin Klaminger
 * @since 2019-07-23
 */
public class IssueFields
{
    private Issue[] subtasks;
    private JiraProject project;
    private String summary;
    private IssueStatus status;
    private IssueType issuetype;
    private String customfield_13176; //TimeLogging activated

    public Issue[] getSubtasks()
    {
        return subtasks;
    }

    public void setSubtasks(Issue[] subtasks)
    {
        this.subtasks = subtasks;
    }

    public JiraProject getProject()
    {
        return project;
    }

    public void setProject(JiraProject project)
    {
        this.project = project;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status)
    {
        this.status = status;
    }

    public IssueType getIssuetype()
    {
        return issuetype;
    }

    public void setIssuetype(IssueType issuetype)
    {
        this.issuetype = issuetype;
    }

    public String getCustomfield_13176()
    {
        return customfield_13176;
    }

    public void setCustomfield_13176(String customfield_13176)
    {
        this.customfield_13176 = customfield_13176;
    }

    public boolean isTimeLoggingActivated()
    {
        return this.customfield_13176 != null && !this.customfield_13176.isEmpty();
    }
}