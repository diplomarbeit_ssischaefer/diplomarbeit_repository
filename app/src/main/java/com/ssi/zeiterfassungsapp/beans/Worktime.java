package com.ssi.zeiterfassungsapp.beans;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

@Entity(tableName = "Worktime", primaryKeys = {"start_date", "end_date"})
public class Worktime implements Comparable<Worktime>
{
    @Ignore
    private static final String TAG = "Worktime";

    @SerializedName("ISSUEKEY")//Mandatory for Webservice
    private String issueKey;

    @SerializedName("STARTDATE") //Mandatory for Webservice
    @ColumnInfo(name = "start_date")
    @NonNull
    private String startDateString;

    @Expose
    @Ignore
    private Date startDate;

    @SerializedName("ENDDATE") //Mandatory for Webservice
    @ColumnInfo(name = "end_date")
    @NonNull
    private String endDateString;

    @Expose
    @Ignore
    private Date endDate;

    @SerializedName("BOOKINGTEXT") //Optional for Webservice
    @ColumnInfo(name = "booking_text")
    private String bookingtext;

    @Expose
    @Ignore
    private transient SimpleDateFormat df;

    @Expose
    @Ignore
    private transient boolean synced;

    @Ignore
    public Worktime(Date startDate, Date endDate, String issueKey)
    {
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY); //Date format in App
        this.startDate = startDate;
        this.startDateString = df.format(startDate);
        this.endDate = endDate;
        this.endDateString = df.format(endDate);
        this.issueKey = issueKey;
    }

    /**This Constructor is used if a Worktime will be recorded manually
     *
     * @param issueKey String of IssueKey
     * @param startDate Date of Startdate
     * @param endDate Date of Enddate
     * @param bookingtext String of Bookingtext
     */
    @Ignore
    public Worktime(String issueKey, Date startDate, Date endDate, String bookingtext)
    {
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
        this.issueKey = issueKey;
        this.startDate = startDate;
        this.startDateString = df.format(startDate);
        this.endDate = endDate;
        this.endDateString = df.format(endDate);
        this.bookingtext = bookingtext;
    }

    /**This Constructor is used if a Worktime will be recorded manually
     *
     * @param issueKey String of IssueKey
     * @param startDateString String of Startdate
     * @param endDateString String of Enddate
     * @param bookingtext String of Bookingtext
     */
    public Worktime(String issueKey, String startDateString, String endDateString, String bookingtext)
    {
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
        this.issueKey = issueKey;
        this.startDateString = startDateString;
        this.endDateString = endDateString;

        try
        {
            this.startDate = df.parse(startDateString);
            this.endDate = df.parse(endDateString);
        }
        catch (ParseException e)
        {
            Log.e(TAG, e.getMessage() + " Constructor");
        }

        this.bookingtext = bookingtext;
    }

    @Ignore
    public Worktime(String formattedString)
    {
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
        String parameters = formattedString.substring(9, formattedString.length()-1);
        System.out.println(parameters);
        String issueKey = parameters.split(", ")[2].split("=")[1];
        String bookingText;

        try
        {
            bookingText = parameters.split(", ")[3].split("=")[1] != null
                    ? "" : parameters.split(", ")[3].split("=")[1];
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            bookingText = "";
        }

        String startDate = parameters.split(", ")[0].split("=")[1];
        String endDate;

        try
        {
            endDate = parameters.split(", ")[1].split("=")[1].equals("")
                    ? "null" : parameters.split(", ")[1].split("=")[1];
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            endDate = "null";
        }

        this.issueKey = issueKey;
        this.bookingtext = bookingText;
        this.startDateString = startDate;
        this.endDateString = endDate;

        try
        {
            this.startDate = df.parse(startDateString);
            this.endDate = df.parse(endDateString);
        }
        catch (ParseException e)
        {
            Log.e(TAG, e.getMessage() + " Worktime");
        }
    }

    /**
     * Constructor for Deserializing JSON Strings
     */
    @Ignore
    public Worktime()
    {
        df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
    }

    /**Setter Method which sets the Date and the Date String for JSON Purpose
     *
     * @param startDateString Date String
     */
    public void setStartDateString(String startDateString)
    {
        this.startDateString = startDateString;

        try
        {
            this.startDate = df.parse(startDateString);
        }
        catch (ParseException e)
        {
            Log.e(TAG, e.getMessage() + " setStartDateString");
        }
    }

    /**Setter Method which sets the Date and the Date String for JSON Purpose
     *
     * @param endDateString Date String
     */
    public void setEndDateString(String endDateString)
    {
        this.endDateString = endDateString;

        try
        {
            this.endDate = df.parse(endDateString);
        }
        catch (ParseException e)
        {
            Log.e(TAG, e.getMessage() + " setEndDateString");
        }
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public String getStartDateString()
    {
        return startDateString;
    }

    /**Setter Method which sets the Date and the Date String for JSON Purpose
     *
     * @param startDate Date Object
     */
    public void setStartDate(Date startDate)
    {
        this.startDateString = df.format(startDate);
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public String getEndDateString()
    {
        return endDateString;
    }

    /**Setter Method which sets the Date and the Date String for JSON Purpose
     *
     * @param endDate Date Object
     */
    public void setEndDate(Date endDate)
    {
        this.endDateString = df.format(endDate);
        this.endDate = endDate;
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public void setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
    }

    public String getBookingtext()
    {
        return bookingtext;
    }

    public void setBookingtext(String bookingtext)
    {
        this.bookingtext = bookingtext;
    }

    public boolean isSynced()
    {
        return synced;
    }

    public void setSynced(boolean synced)
    {
        this.synced = synced;
    }

    @Override
    public String toString()
    {
        String startDateS = "";
        String endDateS = "";

        if(startDate != null)
        {
            startDateS = df.format(startDate);
        }

        if(endDate != null)
        {
            endDateS = df.format(endDate);
        }

        return "Worktime{" +
                "from=" + startDateS +
                ", to=" + endDateS +
                ", projectKey=" + issueKey +
                ", bookingtext=" + bookingtext +
                '}';
    }

    /**This method is used to format the Worktime Object to a String in order to place it inside a Request
     *
     * @return String of formatted Worktime
     */
    public String toRequest()
    {
        return String.format("{" +
                "\"ISSUEKEY\":\"%s\"," +
                "\"STARTDATE\":\"%s\"," +
                "\"ENDDATE\":\"%s\"," +
                "\"BOOKINGTEXT\":\"%s\"" +
                "}", issueKey, startDateString, endDateString, bookingtext);
    }

    @Override
    public int compareTo(Worktime worktime)
    {
        return worktime.getEndDate().compareTo(this.getEndDate());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Worktime worktime = (Worktime) o;

        return startDateString.equals(worktime.startDateString);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(startDateString);
    }
}
