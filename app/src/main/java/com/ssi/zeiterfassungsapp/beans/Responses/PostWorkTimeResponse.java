package com.ssi.zeiterfassungsapp.beans.Responses;

import com.google.gson.annotations.SerializedName;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import java.util.Arrays;

/**This class is used to work as a Wrapper Class of PostWorktime
 *
 * @author Schwarzkogler
 * @since 2019/07/11
 */
public class PostWorkTimeResponse
{
    @SerializedName("status")
    private String status;
    @SerializedName("user")
    private String user;
    @SerializedName("origin")
    private String origin;
    @SerializedName("target")
    private String target;
    @SerializedName("bookuser")
    private String bookuser;
    @SerializedName("booking")
    private Worktime[] worktimes;
    @SerializedName("msg")
    private String msg;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getOrigin()
    {
        return origin;
    }

    public Worktime[] getWorktime()
    {
        return worktimes;
    }

    public void setWorktime(Worktime[] worktimes)
    {
        this.worktimes = worktimes;
    }

    public void setOrigin(String origin)
    {
        this.origin = origin;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getBookuser()
    {
        return bookuser;
    }

    public void setBookuser(String bookuser)
    {
        this.bookuser = bookuser;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    @Override
    public String toString()
    {
        return "PostWorkTimeResponse{" +
                "status='" + status + '\'' +
                ", user='" + user + '\'' +
                ", origin='" + origin + '\'' +
                ", target='" + target + '\'' +
                ", bookuser='" + bookuser + '\'' +
                ", booking=" + Arrays.toString(worktimes) +
                ", msg='" + msg + '\'' +
                '}';
    }
}
