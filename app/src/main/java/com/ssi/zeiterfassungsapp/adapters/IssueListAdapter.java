package com.ssi.zeiterfassungsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.IssueRelated.Issue;
import java.util.ArrayList;

/**
 * This class is used as an adapter for all issues of a project.
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class IssueListAdapter extends RecyclerView.Adapter<IssueListAdapter.ViewHolder>
{
    private ArrayList<Issue> issueList = new ArrayList<>();
    private Context context;

    public IssueListAdapter(Context context)
    {
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public CardView cvCard;
        public TextView issueKey;
        public TextView issueSummary;
        public TextView issueStatus;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.cvCard = itemView.findViewById(R.id.cv_issue);
            this.issueKey = itemView.findViewById(R.id.tv_issuekey);
            this.issueSummary = itemView.findViewById(R.id.tv_summary);
            this.issueStatus = itemView.findViewById(R.id.tv_status);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.issue_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition)
    {
        if(issueList.get(listPosition).getColor() != 0)
        {
            holder.cvCard.setCardBackgroundColor(issueList.get(listPosition).getColor());
            holder.cvCard.getBackground().setAlpha(70);
        }

        holder.issueKey.setText(issueList.get(listPosition).getKey());
        holder.issueSummary.setText(issueList.get(listPosition).getFields().getSummary());
        holder.issueStatus.setText(issueList.get(listPosition).getFields().getStatus().getName());
    }

    @Override
    public int getItemCount()
    {
        return issueList.size();
    }

    public Issue getItem(int id)
    {
        return issueList.get(id);
    }

    /**
     * This method adds a issue to the list of issues.
     *
     * @param issue - issue to add
     */
    public void addIssue(Issue issue)
    {
        issueList.add(issue);
        notifyDataSetChanged();
    }

    /**
     * This method sets the color for the issues of the project that has been selected.
     *
     * @param color - integer of the project's color
     */
    public void setColor(int color)
    {
        for (Issue issue : issueList)
        {
            issue.setColor(color);
        }
    }
}
