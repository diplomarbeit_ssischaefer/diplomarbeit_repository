package com.ssi.zeiterfassungsapp.adapters;

import android.content.Context;
import android.graphics.ColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Project;
import com.ssi.zeiterfassungsapp.webservice.Webservice;
import java.util.ArrayList;

/**
 * This class is used as an adapter for all projects of the user.
 *
 * @author Schweiger
 * @since 2019/07/08
 */
public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ViewHolder>
{
    private ArrayList<Project> projectList = new ArrayList<>();
    private ItemClickListener projectClickListener;
    private Context context;

    public ProjectListAdapter(Context context)
    {
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView projectImage;
        public TextView projectName;
        public TextView projectKey;
        public LinearLayout projectTask;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.projectImage = itemView.findViewById(R.id.iv_project);
            this.projectName = itemView.findViewById(R.id.tv_issuekey);
            this.projectKey = itemView.findViewById(R.id.tv_summary);
            this.projectTask = itemView.findViewById(R.id.ll_projecttask);

            projectImage.setOnClickListener(this);
            projectTask.setOnClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            projectClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition)
    {
        ImageView image = projectList.get(listPosition).getProjectImage();
        ColorFilter cf = image.getColorFilter();

        holder.projectImage.setColorFilter(cf);
        holder.projectImage.setImageResource(projectList.get(listPosition).getProjectImageID());
        holder.projectName.setText(projectList.get(listPosition).getProjectName());
        holder.projectKey.setText(projectList.get(listPosition).getProjectKey());
    }

    @Override
    public int getItemCount()
    {
        return projectList.size();
    }

    public Project getItem(int id)
    {
        return projectList.get(id);
    }

    /**
     * This method adds a project to the list of projects.
     *
     * @param project - project to add
     */
    public void addProject(Project project)
    {
        Webservice webservice = Webservice.getInstance();

        if(!webservice.loadUsername().equals("Guest"))
        {
            project.setProjectImage(new ImageView(context));
            project.setProjectImageID(R.drawable.ic_projectfolder);
            project.getProjectImage().setImageResource(R.drawable.ic_projectfolder);
            projectList.add(project);
            notifyDataSetChanged();
        }
    }

    /**
     * This method sets a color for a specific project that has been selected by the user.
     *
     * @param color - integer of the color selected
     * @param pos - position of the project selected
     */
    public void setColor(int color, int pos)
    {
        projectList.get(pos).getProjectImage().setColorFilter(color);
        projectList.get(pos).setColor(color);
        notifyDataSetChanged();
    }

    public void setClickListener(ProjectListAdapter.ItemClickListener ItemClickListener)
    {
        this.projectClickListener = ItemClickListener;
    }

    public interface ItemClickListener
    {
        void onItemClick(View view, int position);
    }
}
