package com.ssi.zeiterfassungsapp.adapters;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.ssi.zeiterfassungsapp.R;
import com.ssi.zeiterfassungsapp.beans.Worktime;
import com.ssi.zeiterfassungsapp.util.ListingProvider;
import com.ssi.zeiterfassungsapp.webservice.Webservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class functions as an adapter for the WorktimeList and it configures and sets all the
 * neccessary things for the filteredWorktimeList
 * @author Matthias Pöschl
 * @since  2019/07/09
 */
public class WorktimelistAdapter extends RecyclerView.Adapter<WorktimelistAdapter.ViewHolder>
{
    private List<Worktime> filteredWorktimeList;
    private List<Worktime> unfilteredWorktimeList;
    private LayoutInflater inflater;
    private ItemClickListener worktimeClickListener;
    private static WorktimelistAdapter worktimeListAdapterInstance;
    private ListingProvider listingProvider;

    public void setInflater(LayoutInflater inflater)
    {
        this.inflater = inflater;
    }

    public void setUnfilteredWorktimeList(List<Worktime> unfilteredWorktimeList)
    {
        this.unfilteredWorktimeList = unfilteredWorktimeList;
        if(listingProvider != null)
        {
            listingProvider.addLocalWorktimes(this.unfilteredWorktimeList);
        }

        Collections.sort(this.unfilteredWorktimeList); //sorted by startdate (per default)

        //filter(0,Long.MAX_VALUE,"All Projects",false);
    }

    public List<Worktime> getUnfilteredWorktimeList()
    {
        return unfilteredWorktimeList;
    }

    public static WorktimelistAdapter getInstance()
    {
        if(worktimeListAdapterInstance == null)
        {
            worktimeListAdapterInstance = new WorktimelistAdapter();
        }

        return worktimeListAdapterInstance;
    }

    private WorktimelistAdapter()
    {
        filteredWorktimeList = new LinkedList<>();
    }

    // This inflates the worktime_list_item.animation when it is needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.worktime_list_item, parent, false);

        return new ViewHolder(view);
    }

    // This binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Worktime worktime = filteredWorktimeList.get(position);
        String fromTo = String.format("%s %20d:%02d - %d:%02d","From - To:", worktime.getStartDate().getHours(), worktime.getStartDate().getMinutes(),
                worktime.getEndDate().getHours(), worktime.getEndDate().getMinutes());

        holder.tvFromTo.setText(fromTo);

        if(worktime.isSynced())
        {
            holder.cbSynced.setChecked(true);
            holder.cbSynced.setAlpha(1);
        }
        else
        {
            holder.cbSynced.setAlpha(0);
        }

        Long duration = worktime.getEndDate().getTime() - (worktime.getStartDate().getTime());
        int seconds = (int) (duration / 1000);
        int minutes = seconds / 60;
        seconds = seconds % 60;
        int hours = minutes / 60;
        minutes = minutes % 60;

        String formatDuration = String.format("%s %21d:%02d:%02d","Duration:", hours, minutes, seconds);
        holder.tvDuration.setText(formatDuration);

        SimpleDateFormat dayFormatter = new SimpleDateFormat("EEEE, dd.MM.yyyy");
        String formattedDate = dayFormatter.format(worktime.getStartDate());
        holder.tvDate.setText(formattedDate);

        holder.tvProjectid.setText(worktime.getIssueKey());

        if(worktime.getIssueKey().equals("No project selected"))
        {
            holder.tvProjectid.setTextColor(Color.RED);
            holder.tvProjectid.setTypeface(holder.tvProjectid.getTypeface(), Typeface.ITALIC);
        }
        else
        {
            holder.tvProjectid.setTextColor(Color.BLACK);
            holder.tvProjectid.setTypeface(null, Typeface.NORMAL);
        }
    }

    // Returns the total number of rows
    @Override
    public int getItemCount()
    {
        return filteredWorktimeList.size();
    }

    // Stores and recycles the views as they are scrolled off the screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView;
        CardView cv;
        TextView tvFromTo;
        TextView tvDuration;
        TextView tvDate;
        TextView tvProjectid;
        CheckBox cbSynced;

        ViewHolder(View itemView)
        {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_date);
            cv = itemView.findViewById(R.id.cv_time);
            tvFromTo = itemView.findViewById(R.id.tv_fromTo);
            tvDuration = itemView.findViewById(R.id.tv_duration);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvProjectid = itemView.findViewById(R.id.tv_projectid);
            cbSynced = itemView.findViewById(R.id.cb_synced);
            itemView.setOnClickListener(this);

            cv.bringToFront();
            tvFromTo.bringToFront();
            tvDuration.bringToFront();
        }

        //Listener for OnClickEvents, which calls the onItemClick method
        @Override
        public void onClick(View view)
        {
            if (worktimeClickListener != null)
            {
                worktimeClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // Method for getting the data at the given position
    public Worktime getItem(int id)
    {
        //TODO null pointer when list is empty
       return filteredWorktimeList.get(id);
    }

    public void addItem(Worktime item)
    {
        unfilteredWorktimeList.add(item);
        resetFilter();
        notifyItemInserted(filteredWorktimeList.size() - 1);
    }

    /**
     * filter for Worktimes in duration, project or synchronisation state
     * @param from milliseconds of startdate to filter from
     * @param to   milliseconds of enddate to filter up to
     * @param projectkey representing the project
     * @param synced if the worktime is synced or not
     */
    public void filter(long from, long to,String projectkey, boolean synced)
    {
        listingProvider = ListingProvider.getInstance();
        filteredWorktimeList.clear();
        filteredWorktimeList.addAll(listingProvider.filterWorktimes(projectkey,from,to,synced));
        Collections.sort(this.filteredWorktimeList); //sorted by startdate (per default)
        notifyDataSetChanged();
    }

    /**
     * filter Worktimes and sort them
     * worktimes from the last 3 days only
     * unsynced worktimes before synced worktimes
     * each category sorted by startdate
     */
    public void resetFilter()
    {
        filteredWorktimeList.clear();
        filteredWorktimeList.addAll(ListingProvider.getInstance().sortWorktimesBasicRefresh());
    }

    // Method that allows for catching the click events
    public void setClickListener(ItemClickListener ItemClickListener)
    {
        this.worktimeClickListener = ItemClickListener;
    }

    // The parent activity will implement this method to respond to click events
    public interface ItemClickListener
    {
        void onItemClick(View view, int position);
    }

    public void reset()
    {
        unfilteredWorktimeList = new ArrayList<>();
        filteredWorktimeList = new ArrayList<>();
        listingProvider.clearProjectsAndIssues();
        listingProvider.clearWorktimes();
    }
}
