package com.ssi.zeiterfassungsapp.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.ssi.zeiterfassungsapp.layouts.StatisticsMonthly;
import com.ssi.zeiterfassungsapp.layouts.StatisticsWeekly;
import com.ssi.zeiterfassungsapp.layouts.StatisticsYearly;

public class StatisticsTabAdapter extends FragmentStatePagerAdapter
{
    public StatisticsTabAdapter(@NonNull FragmentManager fm, int behavior)
    {
        super(fm, behavior);
    }

    public StatisticsTabAdapter(FragmentManager supportFragmentManager)
    {
        super(supportFragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int position)
    {
        switch(position)
        {
            case 0: return new StatisticsWeekly();
            case 1: return new StatisticsMonthly();
            case 2: return new StatisticsYearly();
            default: return null;
        }
    }

    @Override
    public int getCount()
    {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        switch(position)
        {
            case 0: return "Daily";
            case 1: return "Weekly";
            case 2: return "Monthly";
            default: return null;
        }
    }
}
